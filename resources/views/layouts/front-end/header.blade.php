<header class="header header-intro-clearance header-4">
    <div class="header-top">
        <div class="container">
            <div class="header-left">
                <a href="tel:#"><i class="icon-phone"></i>Call: +0123 456 789</a>
            </div><!-- End .header-left -->
            <div class="header-right">
                <ul class="top-menu">
                    <li>
                        <a href="#">Links</a>
                        <ul>
                            <li>
                                <div class="header-dropdown">
                                    <i class="fa fa-money-bill-alt"></i>
                                    <a href="#">USD</a>
                                    <div class="header-menu">
                                        <ul>
                                            <li><a href="#">Eur</a></li>
                                            <li><a href="#">Usd</a></li>
                                        </ul>
                                    </div><!-- End .header-menu -->
                                </div>
                            </li>
                            <li class="nav-item header-dropdown">

                                <a class="nav-link" data-toggle="dropdown" href="">
                                    <i class="fas fa-language"></i> {{ __('lang.choos_language') }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right p-0">
                                    <a class="nav-link" data-toggle="nav-item" href="{{ url('localization/lo') }}">
                                        <i class="flag-icon flag-icon-la"></i> {{ __('lang.laos') }}
                                    </a>
                                    <a class="nav-link" data-toggle="nav-item" href="{{ url('localization/en') }}">
                                        <i class="flag-icon flag-icon-us"></i> {{ __('lang.english') }}
                                    </a>
                                </div>
                            </li>
                            {{-- <li>
                                <div class="header-dropdown">
                                    <a href="#">English</a>
                                    <div class="header-menu">
                                        <ul>
                                            <li><a href="#">English</a></li>
                                            <li><a href="#">French</a></li>
                                            <li><a href="#">Spanish</a></li>
                                        </ul>
                                    </div><!-- End .header-menu -->
                                </div>
                            </li> --}}
                            {{-- <li><a href="#signin-modal" data-toggle="modal">Sign in / Sign up</a></li> --}}
                        </ul>
                    </li>
                </ul><!-- End .top-menu -->
            </div><!-- End .header-right -->
        </div><!-- End .container -->
    </div><!-- End .header-top -->

    <div class="header-middle">
        <div class="container">
            <div class="header-left">
                <button class="mobile-menu-toggler">
                    <span class="sr-only">Toggle mobile menu</span>
                    <i class="icon-bars"></i>
                </button>
                <a href="#" class="logo">
                    <img src="{{ asset('logo/logo.jpg') }}" class="rounded-circle" alt="Molla Logo" width="50"
                        height="50">
                </a>
                <span class="brand-text font-weight-light pl-3 d-none d-lg-block" style="font-size: 18px">
                    {{ __('lang.title') }}</span>
            </div><!-- End .header-left -->

            <div class="header-center">
                <div class="header-search header-search-extended header-search-visible d-none d-lg-block">
                    <a href="#" class="search-toggle" role="button"><i class="icon-search"></i></a>
                    <form action="#" method="get">
                        <div class="header-search-wrapper search-wrapper-wide">
                            <label for="q" class="sr-only">Search</label>
                            <button class="btn btn-primary" type="submit"><i class="icon-search"></i></button>
                            <input type="search" class="form-control" name="q" id="q"
                                placeholder="Search product ..." required>
                        </div><!-- End .header-search-wrapper -->
                    </form>
                </div><!-- End .header-search -->
            </div>

            <div class="header-right">
                <div class="dropdown compare-dropdown">
                    <div class="dropdown-menu dropdown-menu-right">
                        <ul class="compare-products">
                            <li class="compare-product">
                                <a href="#" class="btn-remove" title="Remove Product"><i
                                        class="icon-close"></i></a>
                                <h4 class="compare-product-title"><a href="product.html">Blue Night Dress</a></h4>
                            </li>
                            <li class="compare-product">
                                <a href="#" class="btn-remove" title="Remove Product"><i
                                        class="icon-close"></i></a>
                                <h4 class="compare-product-title"><a href="product.html">White Long Skirt</a></h4>
                            </li>
                        </ul>

                        <div class="compare-actions">
                            <a href="#" class="action-link">Clear All</a>
                            <a href="#" class="btn btn-outline-primary-2"><span>Compare</span><i
                                    class="icon-long-arrow-right"></i></a>
                        </div>
                    </div><!-- End .dropdown-menu -->
                </div><!-- End .compare-dropdown -->

                <div class="wishlist">
                    <a href="wishlist.html" title="Wishlist">
                        <div class="icon">
                            <i class="icon-heart-o"></i>
                            <span class="wishlist-count badge">3</span>
                        </div>
                        <p>Wishlist</p>
                    </a>
                </div><!-- End .compare-dropdown -->

                
                <div class="dropdown cart-dropdown">
                    {{-- <a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static" title="Compare Products" aria-label="Compare Products">
                        <i class="icon-random"></i>
                        <span class="compare-txt">Compare</span>
                    </a> --}}

                  
                    @auth
                    @if (Auth::user()->id != 1)
                    gtdh
                    @endif
                    jyu
                    @else
                    
                        <a href="{{ route('frontend.login') }}" class="dropdown-toggle" role="button"
                            aria-haspopup="true" aria-expanded="false" data-display="static">
                            <div class="icon">
                                <i class="icon-user"></i>
                            </div>
                            <p>{{ __('lang.login') }}</p>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <ul class="compare-products">
                                <li class="compare-product">
                                    <h3 class="compare-product-title"><a href="product.html"><i class="fa fa-user"></i> {{ __('lang.profile') }}</a></h3>
                                </li>
                                <li class="compare-product">
                                    <h3 class="compare-product-title"><a href="product.html"><i class="fa fa-lock-open"></i> {{ __('lang.change_password') }}</a></h3>
                                </li>
                            </ul>
    
                            <div class="compare-actions">
                                {{-- <a href="{{ route('frontend.logout') }}" class="btn btn-outline-primary-2"><span><i class="fa fa-sign-out-alt"></i> {{ __('lang.logout') }}</span></a> --}}
                            </div>
                        </div><!-- End .dropdown-menu -->
                    {{-- @endif --}}
                </div><!-- End .cart-dropdown -->
                @endauth
            </div><!-- End .header-right -->
        </div><!-- End .container -->
    </div><!-- End .header-middle -->

    <div class="header-bottom sticky-header">
        <div class="container">
            <div class="header-right">
                <nav class="main-nav">
                    <ul class="menu sf-arrows">
                        <li class="megamenu-container active">
                            <a href="index.html" class="sf-with" style="font-size: 17px"><i class="fa fa-home"></i>
                                {{ __('lang.home') }}</a>
                        </li>
                        <li class="megamenu-container">
                            <a href="index.html" class="sf-with" style="font-size: 17px"> {{ __('lang.sale') }}</a>
                        </li>
                        <li class="megamenu-container">
                            <a href="index.html" class="sf-with" style="font-size: 17px"> {{ __('lang.news') }}</a>
                        </li>
                        <li class="megamenu-container">
                            <a href="index.html" class="sf-with" style="font-size: 17px"> {{ __('lang.land') }}</a>
                        </li>
                        <li class="megamenu-container">
                            <a href="index.html" class="sf-with" style="font-size: 17px">
                                {{ __('lang.real_estate') }}</a>
                        </li>



                        <li>
                            <a href="elements-list.html" class="sf-with-ul">Elements</a>
                            <ul>
                                <li><a href="elements-products.html">Products</a></li>
                                <li><a href="elements-typography.html">Typography</a></li>

                            </ul>
                        </li>
                    </ul><!-- End .menu -->
                </nav><!-- End .main-nav -->
            </div><!-- End .header-center -->

            <div class="header-right">
                {{-- <i class="la la-lightbulb-o"></i> --}}
                <p>Clearance></p>
                <nav class="main-nav">
                    <ul class="menu sf-arrows">
                        <li class="megamenu-container">
                            <a href="index.html" class="sf-with" style="font-size: 17px">
                                {{ __('lang.contact') }}</a>
                        </li>
                        <li class="megamenu-container">
                            <a href="index.html" class="sf-with" style="font-size: 17px"> {{ __('lang.about') }}</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div><!-- End .container -->
    </div><!-- End .header-bottom -->
</header><!-- End .header -->
