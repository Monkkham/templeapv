<!DOCTYPE html>
<html lang="en">
<!-- molla/index-2.html  22 Nov 2019 09:55:32 GMT -->
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Molla - Bootstrap eCommerce Template</title>
    <meta name="keywords" content="HTML5 Template">
    <meta name="description" content="Molla - Bootstrap eCommerce Template">
    <meta name="author" content="p-themes">
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('front-end/assets/images/icons/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('front-end/assets/images/icons/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('front-end/assets/images/icons/favicon-16x16.png')}}">
    <link rel="manifest" href="{{ asset('front-end/assets/images/icons/site.html')}}">
    <link rel="mask-icon" href="{{ asset('front-end/assets/images/icons/safari-pinned-tab.svg" color="#666666')}}">
    <link rel="shortcut icon" href="{{ asset('front-end/assets/images/icons/favicon.ico')}}">
    <meta name="apple-mobile-web-app-title" content="Molla">
    <meta name="application-name" content="Molla">
    <meta name="msapplication-TileColor" content="#cc9966">
    <meta name="msapplication-config" content="{{ asset('front-end/assets/images/icons/browserconfig.xml')}}">
    <meta name="theme-color" content="#ffffff">
     <!-- Font Awesome -->
     <link rel="stylesheet" href="{{asset('admin/plugins/flag-icon-css/css/flag-icon.min.css')}}">

     <link rel="stylesheet" href="{{asset('admin/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Plugins CSS File -->
    <link rel="stylesheet" href="{{ asset('front-end/assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('front-end/assets/css/plugins/owl-carousel/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('front-end/assets/css/plugins/magnific-popup/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ 'front-end/assets/css/bootstrap.min.css' }}">
    <link rel="stylesheet" href="{{ 'front-end/assets/css/plugins/jquery.countdown.css' }}">
    <!-- Main CSS File -->
    <link rel="stylesheet" href="{{ asset('front-end/assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ 'front-end/assets/css/style.css' }}">
    <link rel="stylesheet" href="{{ 'front-end/assets/css/skins/skin-demo-4.css' }}">
    <link rel="stylesheet" href="{{ 'front-end/assets/css/demos/demo-4.css' }}">
    <style>
        nav svg{ height: 20px; }
        @font-face{
        font-family: Noto Sans Lao Medium;
        src: url('{{asset('fonts/NotoSansLao-Medium.ttf')}}');
        }
        a,p, *{
            font-family: 'Noto Sans Lao Medium';
        }
    </style>
    @livewireStyles
</head>
<body style="font-family: 'Noto Sans Lao Medium'">
    <div class="page-wrapper">
        @include('layouts.front-end.header')
        {{ $slot }}
    </div>
    @include('layouts.front-end.mobile-menu')
    {{-- @include('livewire.frontend.register-content') --}}
        <!-- Plugins JS File -->
        <script src="{{ asset('front-end/assets/js/jquery.min.js')}}"></script>
        <script src="{{ asset('front-end/assets/js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{ asset('front-end/assets/js/jquery.hoverIntent.min.js')}}"></script>
        <script src="{{ asset('front-end/assets/js/jquery.waypoints.min.js')}}"></script>
        <script src="{{ asset('front-end/assets/js/superfish.min.js')}}"></script>
        <script src="{{ asset('front-end/assets/js/owl.carousel.min.js')}}"></script>
        <script src="{{ asset('front-end/assets/js/jquery.magnific-popup.min.js')}}"></script>
        <!-- Main JS File -->
        <script src="{{ asset('front-end/assets/js/main.js')}}"></script>
        @stack('scripts')
        {{-- alert message  --}}
    <script>
        window.livewire.on('alert', param => {
        toastr[param['type']](param['message'],param['type']);
        });
    </script>
@livewireScripts
</body>
</html>