  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-warning elevation-3">
      <!-- Brand Logo -->
      <a href="#" class="brand-link">
          <img src="{{ asset('logo/logo1.jpg') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
              style="opacity: .8">
          <span class="brand-text font-weight-light text-md">ວັດອຳພາວັນ</span>
      </a>
      <!-- Sidebar -->
      <div class="sidebar">
          <!-- Sidebar user panel (optional) -->
          {{-- <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">ຜູ້ຄຸ້ມຄອງລະບົບ</a>
        </div>
      </div> --}}
          <!-- Sidebar Menu -->
          {{-- @if (auth()->user()->roles == 'admin') --}}
          <nav class="mt-2">
              <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                  data-accordion="false">
                  <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                  <li class="nav-item menu-open">
                      <a href="{{ route('dashboard') }}" class="nav-link active">
                          <i class="nav-icon fas fa-tachometer-alt"></i>
                          <p>
                              {{ __('lang.dashboard') }}
                          </p>
                      </a>
                  </li>
                  <li class="dropdown-divider"></li>
                  <li class="nav-item">
                      <a href="{{ route('backend.income') }}" class="nav-link">
                          <i class="nav-icon fas fa-hand-holding-usd text-success"></i>
                          <p>
                              {{ __('lang.income') }}
                          </p>
                      </a>
                  </li>
                  <li class="dropdown-divider"></li>
                  <li class="nav-item">
                      <a href="{{ route('backend.expend') }}" class="nav-link">
                          <i class="nav-icon fas fa-cart-plus text-danger"></i>
                          <p>
                              {{ __('lang.expend') }}
                          </p>
                      </a>
                  </li>
                  <li class="dropdown-divider"></li>
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-chart-line"></i>
                        <p>
                            ລາຍງານ
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('backend.report-income') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p> ລາຍຮັບ</p>
                            </a>
                        </li>
                        <li class="dropdown-divider"></li>
                        <li class="nav-item">
                            <a href="{{ route('backend.report-expend') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p> ລາຍຈ່າຍ</p>
                            </a>
                        </li>
                    </ul>
                    {{-- <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('backend.report_people_donate') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>ເຈົ້າສັດທາໂມທະນາ</p>
                            </a>
                        </li>
                    </ul> --}}
                </li>
                <li class="dropdown-divider"></li>
                  {{-- ===================== pepple donate ======================= --}}
                  <li class="nav-item">
                      <a href="#" class="nav-link">
                          <i class="nav-icon fas fa-user-lock"></i>
                          <p>
                              ສິດທິ ເເລະ ຜູ້ໃຊ້
                              <i class="fas fa-angle-left right"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="{{ route('backend.role') }}" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>ສິດທິເຂົ້າໃຊ້</p>
                              </a>
                          </li>
                      </ul>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="{{ route('backend.users') }}" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>ຜູ້ໃຊ້ງານລະບົບ</p>
                              </a>
                          </li>
                      </ul>
                  </li>
                  <li class="dropdown-divider"></li>
                  @if (auth()->user()->phone == '52310548')
                      <li class="nav-item">
                          <a href="{{ route('backend.salary') }}" class="nav-link">
                              <i class="nav-icon fas fa-money-bill-wave-alt"></i>
                              <p>
                                  ຮັບເງິນເດືອນ
                              </p>
                          </a>
                      </li>
                  <li class="dropdown-divider"></li>
                      <li class="nav-item">
                          <a href="{{ route('backend.general') }}" class="nav-link">
                              <i class="nav-icon fas fa-plus-circle"></i>
                              <p>
                                  {{ __('lang.save_general') }}
                              </p>
                          </a>
                      </li>
                  @endif
                  {{-- ===================== pepple donate ======================= --}}
                  {{-- <li class="nav-item">
                      <a href="#" class="nav-link">
                          <i class="nav-icon fas fa-pray"></i>
                          <p>
                              ໂມທະນາທານ
                              <i class="fas fa-angle-left right"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="{{ route('backend.people_donate') }}" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>ບັນທຶກເຈົ້າສັດທາ</p>
                              </a>
                          </li>
                      </ul>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="{{ route('backend.read_people_donate') }}" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>ອ່ານລາຍຊື່ເຈົ້າສັດທາ</p>
                              </a>
                          </li>
                      </ul>
                  </li> --}}
                  {{-- ===================== monk novice ======================= --}}
                  {{-- <li class="nav-item">
                      <a href="#" class="nav-link">
                          <i class="nav-icon fas fa-users"></i>
                          <p>
                              {{ __('lang.all_monk') }}
                              <i class="fas fa-angle-left right"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="{{ route('backend.monk_novice') }}" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>ພຣະສົງປະຈຸບັນ</p>
                              </a>
                          </li>
                      </ul>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="{{ route('backend.monkhistory') }}" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>ປະຫວັດພຣະສົງ</p>
                              </a>
                          </li>
                      </ul>
                  </li> --}}
                  {{-- ===================== report ======================= --}}
                  </li>
              </ul>
          </nav>
          {{-- @endif --}}
          <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
  </aside>
