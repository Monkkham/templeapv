<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/png" href="{{ asset('logo/logo1.jpg') }}" />
  <link rel="stylesheet" href="{{asset('admin/plugins/flag-icon-css/css/flag-icon.min.css')}}">

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('admin/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ asset('admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('admin/dist/css/adminlte.min.css')}}">
  <!-- Toastr -->
  <link rel="stylesheet" href="{{ asset('admin/plugins/toastr/toastr.min.css')}}">
   <!-- sweetalert2 -->
   <link rel="stylesheet" href="{{asset('admin/plugins/sweetalert2/sweetalert2.css')}}">
   <link rel="stylesheet" href="{{asset('admin/plugins/sweetalert2/sweetalert2.min.css')}}">
   <!-- Select2 -->
   <link rel="stylesheet" href="{{asset('admin/plugins/select2/css/select2.min.css')}}">
   <link rel="stylesheet" href="{{asset('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
  <style>
    nav svg{ height: 20px; }
    @font-face{
    font-family: Noto Sans Lao Medium;
    src: url('{{asset('fonts/NotoSansLao-Medium.ttf')}}');
    }
    body {
        background-image: linear-gradient(to right, rgb(237, 240, 71), rgba(235, 53, 123, 0.4));
 /* background-image: url("https://www.wallpapertip.com/wmimgs/10-108159_yellow-blue-wallpaper-background-full-hd-pt-tiga.jpg"); */
 /* background-color: #cccccc; */
}
</style>
@livewireStyles
</head>
<body class="hold-transition login-page" style="font-family: 'Noto Sans Lao Medium'">







<div class="login-box">
    <marquee scrollamount="12" direction="left" class="text-dark">
        <h1><i class="flag-icon flag-icon-la"></i> {{ __('lang.headding1') }} {{ __('lang.headding2') }}</h1>
    </marquee>
    <!-- /.login-logo -->
    <div class="card card-outline card-primary">
        <div class="card-header text-center">
            <a href="#" class="h2"><i class="fa fa-user-tie"></i> {{ __('lang.welcome_to') }}</a>
        </div>
        <div class="card-body">
            <p class="login-box-msg h5">
                <a href="#" class="brand-link">
                    <img src="{{ asset('logo/logo1.jpg') }}" class="img-circle elevation-2" height="70"> <br>
                    <a href="#" target="_blank" class="h5"><b>ວັດອຳພາວັນ</b></a>
                </a>
            </p>
            <form action="{{route('store')}}" method="post">
            @csrf
                <div class="input-group mb-3">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-phone"></span>
                    </div>
                </div>
                <input type="tel" name="phone" value=""
                 class="form-control @error('phone') is-invalid @enderror"
                    placeholder="{{ __('lang.phone') }}">

            </div>
            @error('phone')
                <span style="color: red" class="error">{{ $message }}</span>
            @enderror
            <div class="input-group mb-3">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-lock"></span>
                    </div>
                </div>
                <input type="password" value="52310548" name="password"
                    class="form-control @error('password') is-invalid @enderror"
                    placeholder="{{ __('lang.password') }}">

            </div>
            @error('password')
                <span style="color: red" class="error">{{ $message }}</span>
            @enderror
            <div class="row">
                <div class="col-8">
              <div class="icheck-primary">
                <input type="checkbox" id="remember" wire:model="remember">
                <label for="remember">
                  ຈຶ່ຈຳຂະນ້ອຍໄວ້
                </label>
              </div>
            </div>
            </div>
            <div class="social-auth-links text-center mt-2 mb-3">
                <button wire:click="login" class="btn btn-block btn-primary">
                    <i class="fa fa-sign-in-alt"></i> {{ __('lang.login') }}
                </button>
                <a href="{{ route('register') }}" class="btn btn-block btn-success">
                        <i class="fas fa-pen-alt"></i> ລົງທະບຽນ
                </a>
                    {{-- <a href="#" class="btn btn-block btn-danger">
                        <i class="fab fa-google-plus mr-2"></i> Sign in Google+
                    </a> --}}
            </div>
            <!-- /.social-auth-links -->
            {{-- </form> --}}
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<!-- /.login-box -->




<!-- jQuery -->
<script src="{{ asset('admin/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('admin/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('admin/dist/js/adminlte.min.js')}}"></script>
<!-- Toastr -->
<script src="{{asset('admin/plugins/toastr/toastr.min.js')}}"></script>
<!-- SweetAlert2 -->
<script src="{{asset('admin/plugins/sweetalert2/sweetalert2.all.js')}}"></script>
<script src="{{asset('admin/plugins/sweetalert2/sweetalert2.all.min.js')}}"></script>
<script src="{{asset('admin/plugins/sweetalert2/sweetalert2.js')}}"></script>
<script src="{{asset('admin/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
@livewireScripts
@include('layouts.backend.script')

@stack('scripts')
</body>
</html>