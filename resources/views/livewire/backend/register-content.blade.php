
{{-- <div class="float-lg-start"><h4>{{ __('lang.headding1') }}</h4></div>
<div class="float-lg-start"><h4>{{ __('lang.headding2') }}</h4></div><br> --}}
{{-- <marquee scrollamount="15" direction="left"> <h1>{{ __('lang.headding1') }} {{ __('lang.headding2') }}</h1> </marquee> --}}
<div class="login-box">
    {{-- <marquee scrollamount="12" direction="left" class="text-dark">
        <h1><i class="flag-icon flag-icon-la"></i> {{ __('lang.headding1') }} {{ __('lang.headding2') }}</h1>
    </marquee> --}}
    <!-- /.login-logo -->
    <div class="card card-outline card-primary">
        <div class="card-header text-left">
            <a href="/"><i class="fas fa-arrow-circle-left"></i> ກັບຄືນ</a>
        </div>
        <div class="card-body">
            <p class="login-box-msg h5">
                {{-- <a href="#" class="brand-link">
                    <img src="{{ asset('logo/logo1.jpg') }}" class="img-circle elevation-2" height="70"> <br>
                    <a href="#" target="_blank" class="h5"><b>ວັດອຳພາວັນ</b></a>
                </a> --}}
                <a href="#" class="h4"><i class="fa fa-user-tie"></i> ລົງທະບຽນນຳໃຊ້ລະບົບ</a>
            </p>
            {{-- <form> --}}
            <div class="input-group mb-3">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-user"></span>
                    </div>
                </div>
                <input type="text" wire:model="name" wire:keydown.enter="login"
                 class="form-control @error('name') is-invalid @enderror"
                    placeholder="{{ __('lang.name') }}">

            </div>
            @error('name')
                <span style="color: red" class="error">{{ $message }}</span>
            @enderror
            <div class="input-group mb-3">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-user"></span>
                    </div>
                </div>
                <input type="text" wire:model="lastname" wire:keydown.enter="login"
                 class="form-control @error('lastname') is-invalid @enderror"
                    placeholder="{{ __('lang.lastname') }}">

            </div>
            @error('lastname')
                <span style="color: red" class="error">{{ $message }}</span>
            @enderror
            <div class="input-group mb-3">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-phone"></span>
                    </div>
                </div>
                <input type="tel" wire:model="phone" wire:keydown.enter="login"
                 class="form-control @error('phone') is-invalid @enderror"
                    placeholder="ເບີໂທ">

            </div>
            @error('phone')
                <span style="color: red" class="error">{{ $message }}</span>
            @enderror
            <div class="input-group mb-3">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-lock"></span>
                    </div>
                </div>
                <input type="password" value="555555" wire:model="password" wire:keydown.enter="login"
                    class="form-control @error('password') is-invalid @enderror"
                    placeholder="{{ __('lang.password') }}">

            </div>
            @error('password')
                <span style="color: red" class="error">{{ $message }}</span>
            @enderror
            <div class="input-group mb-3">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-lock"></span>
                    </div>
                </div>
                <input type="password" value="555555" wire:model="confirm_password" wire:keydown.enter="login"
                    class="form-control @error('confirm_password') is-invalid @enderror"
                    placeholder="ຍືນຍັນລະຫັດຜ່ານ">

            </div>
            @error('confirm_password')
                <span style="color: red" class="error">{{ $message }}</span>
            @enderror
            <div class="row">
            </div>
            <div class="social-auth-links text-center mt-2 mb-3">
                <button wire:click="register" class="btn btn-block btn-success">
                    <i class="fas fa-pen-alt"></i> ລົງທະບຽນ
                </button>
                {{-- <a href="#" class="btn btn-block btn-primary">
                        <i class="fab fa-facebook mr-2"></i> Sign in Facebook
                    </a>
                    <a href="#" class="btn btn-block btn-danger">
                        <i class="fab fa-google-plus mr-2"></i> Sign in Google+
                    </a> --}}
            </div>
            <!-- /.social-auth-links -->
            {{-- </form> --}}
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<!-- /.login-box -->

