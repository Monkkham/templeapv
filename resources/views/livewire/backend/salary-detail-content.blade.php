<div>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3">
                    <h5 class="text-info"><i class="fas fa-user-alt"></i> {{ $this->name }}</h5>
                </div>
                {{-- <div class="col-sm-9">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">ຫນ້າຫລັກ</a></li>
                        <li class="breadcrumb-item active">ບັນທຶກລາຍຊື່ເຈົ້າສັດທາ</li>
                    </ol>
                </div> --}}
            </div>
        </div>
    </section>
    <section wire:ignore class="content">
        <div class="container-fluid">
            <div class="row">
                <!--List users- table table-bordered table-striped -->
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="col-md-6">
                                            {{-- @foreach ($rolepermissions as $items)
                        @if ($items->permissionname->name == 'createUser') --}}
                                            {{-- <a wire:click="create" class="btn btn-primary btn-sm"
                                                href="javascript:void(0)"><i class="fa fa-plus"></i>
                                                {{ __('lang.add') }}</a> --}}
                                            {{-- @endif
                        @endforeach --}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="row">
                                    {{-- <div class="col-md-6">
                                        <input wire:model="search" type="text" class="form-control"
                                            placeholder="{{ __('lang.search') }}">
                                    </div> --}}
                                    {{-- <div class="col-md-6">
                                        <select wire:model="search_people" id="select_data3" class="form-control">
                                          <option value="">ເລືອກເຈົ້າສັດທາ</option>
                                          @foreach ($people_donate as $item)
                                          <option value="{{ $item->id }}">{{ $item->name }}</option>
                                          @endforeach
                                        </select>
                                      </div>  --}}
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr class="text-center bg-light text-bold">
                                            <th>ລຳດັບ</th>
                                            <th>ວ.ດ.ປ ຮັບເງິນ</th>
                                            <th>ສະກຸນເງິນ</th>
                                            <th>ປະເພດເງິນ</th>
                                            <th>ຈັດການ</th>
        
                                        </tr>
                                    </thead>
                                    <tbody>
        
                                        @php $no = 1 @endphp
                                        @foreach ($salary_detail as $item)
                                            <tr class="text-center">
                                                <td>{{ $no++ }}</td>
                                                </td>
                                                <td>{{ date('d/m/Y', strtotime($item->created_at)) }}</td>
                                                {{-- <td>{{ date('d/m/Y H:i:s', strtotime($item->created_at)) }}</td> --}}
                                                <td>{{ number_format($item->money) }}
                                                    @if ($item->currency == 1)
                                                        <b class="text-success">ກີບ</b>
                                                    @elseif($item->currency == 2)
                                                        <b class="text-warning">ບາດ</b>
                                                    @elseif($item->currency == 3)
                                                        <b class="text-danger">ໂດລາ</b>
                                                    @else
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    @if ($item->type == 1)
                                                        <p class="text-success">ເງິນສົດ</p>
                                                    @elseif($item->type == 2)
                                                        <p class="text-info">ເງິນໂອນ</p>
                                                    @else
                                                <td></td>
                                               @endif
                                               </td>
                                               <td>
                                                <button wire:click="show_paymoney({{ $item->id }})"
                                                    type="button" class="btn btn-danger btn-sm"><i
                                                        class="fas fa-minus-circle"></i> ຖອນເງິນ</button>
                                               </td>
                                               </tr>
                                               @endforeach
                                        <tr class="text-center">
                                            <td class="bg-light text-bold text-right" colspan="2">
                                                <i>ລວມເງິນກີບ</i>
                                            </td>
                                            <td class="text-bold">
                                                @if (!empty($sum_lak))
                                                    {{ number_format($sum_lak) }}
                                                @else
                                                    0
                                                @endif
                                            </td>
                                            <td class="bg-light text-left" colspan="2">LAK</td>
        
                                        </tr>
                                        <tr class="text-center">
                                            <td class="bg-light text-bold text-right" colspan="2">
                                                <i>ລວມເງິນບາດ</i>
                                            </td>
                                            <td class="text-bold">
                                                @if (!empty($sum_thb))
                                                    {{ number_format($sum_thb) }}
                                                @else
                                                    0
                                                @endif
                                            </td>
                                            <td class="bg-light text-left" colspan="2">THB</td>
        
                                        </tr>
                                        <tr class="text-center">
                                            <td class="bg-light text-bold text-right" colspan="2">
                                                <i>ລວມເງິນໂດລາ</i>
                                            </td>
                                            <td class="text-bold">
                                                @if (!empty($sum_usd))
                                                    {{ number_format($sum_usd) }}
                                                @else
                                                    0
                                                @endif
                                            </td>
                                            <td class="bg-light text-left" colspan="2">USD</td>
                                        </tr>
        
                                    </tbody>
                                </table>
                                <div class="float-right">
                                    {{-- {{ $salary_detail->links() }} --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('livewire.backend.show-paymoney')

</div>
