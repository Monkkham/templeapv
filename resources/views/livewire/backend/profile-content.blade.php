<div>
    {{-- ======================================== name page ====================================================== --}}
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h5><i class="fas fa-id-card text-cyan"></i> ແກ້ໄຂໂປຣຟາຍ
                       </h5>
                </div>
                <div class="col-sm-6">
                    {{-- <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">{{ __('lang.home') }}</a></li>
                        <li class="breadcrumb-item active">{{ __('lang.province') }}</li>
                    </ol> --}}
                </div>
            </div>
        </div>
    </section>
    <!--Foram add new-->
    <div class="col-md-12">
        <div class="card card-default">
            <div class="card-header bg-primary text-center">
                <h5><label><i class="fas  fa-user-lock"></i> ບັນຊີເຂົ້າສູ່ລະບົບ</label></h5>
            </div>
            <form>
                <div class="card-body">
                   
                    <div class="row">
                        <div class="container">
                            <div class="avatar-upload">
                                <div class="avatar-edit">
                                    <input type='file' wire:model="image" id="imageUpload2"
                                        accept=".png, .jpg, .jpeg" />
                                    {{-- <label for="imageUpload2"></label> --}}
                                </div>
                                @if ($image)
                                    <div id="imagePreview2" class="avatar-preview">
                                        <img src="{{ $image->temporaryUrl() }}" alt="" width="120px;">
                                    </div>
                                @else
                                    @if ($newimage)
                                        <div id="imagePreview2" class="avatar-preview">
                                            <img src="{{ asset('public/bill_photo') }}/{{ $newimage }}"
                                                alt="" width="120px;">
                                        </div>
                                    @else
                                        <div class="avatar-preview">
                                            <div id="imagePreview2"
                                                style="background-image: url({{ asset('public/logo/profile.jpg') }});">
                                            </div>
                                        </div>
                                    @endif
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>ຊື່ຜູ້ໃຊ້</label>
                                <input wire:model="name" type="text"
                                    class="form-control @error('name') is-invalid @enderror"
                                    placeholder="ຊື່ຜູ້ໃຊ້">
                                @error('name')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>ນາມສະກຸນ</label>
                                <input wire:model="lastname" type="text"
                                    class="form-control @error('lastname') is-invalid @enderror"
                                    placeholder="ນາມສະກຸນ">
                                @error('lastname')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>ເບີໂທ</label>
                                <input wire:model="phone" type="text"
                                    class="form-control @error('phone') is-invalid @enderror"
                                    placeholder="phone">
                                @error('phone')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>ອີເມວ</label>
                                <input wire:model="email" type="text"
                                    class="form-control @error('email') is-invalid @enderror"
                                    placeholder="ປ້ອນອີເມວ">
                                @error('email')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="d-flex justify-content-between md-2">
                        <button type="button" wire:click="resetform"
                            class="btn btn-warning">{{ __('lang.reset') }}</button>
                        <button type="button" wire:click="updateProfile"
                            class="btn btn-success">{{ __('lang.save') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
