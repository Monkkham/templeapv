<div>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3">
                    <h5 class="text-success"><i class="fa fa-book"></i> ບັນທືກທົ່ວໄປ</h5>
                </div>
                <div class="col-sm-9">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">ຫນ້າຫລັກ</a></li>
                        <li class="breadcrumb-item active">ບັນທືກທົ່ວໄປ</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">


                <!--List users- table table-bordered table-striped -->

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="col-md-6">
                                            {{-- @foreach ($rolepermissions as $items)
                        @if ($items->permissionname->name == 'createUser') --}}
                                            <a wire:click="create" class="btn btn-primary btn-lg"
                                                href="javascript:void(0)"><i class="fa fa-plus"></i>
                                                {{ __('lang.add') }}</a>
                                            {{-- @endif
                        @endforeach --}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">

                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                    </div>
                                    <div class="input-group input-group-sm" style="width: 240px;">
                                        <input wire:model="search" type="text" class="form-control"
                                            placeholder="{{ __('lang.search') }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="example1" class="table table-hover">
                                    <thead class="bg-success">
                                        <tr>
                                            <th>ຈັດການ</th>
                                            <th>ລຳດັບ</th>
                                            <th>ຊື່</th>
                                            <th>ຈຳນວນເງິນ</th>
                                            <th>ວ.ດ.ປ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $num = 1;
                                        @endphp

                                        @foreach ($general as $item)
                                            <tr>
                                                <td>
                                                    <div class="btn-group">
                                                        <button wire:click="edit({{ $item->id }})" type="button" class="btn btn-warning btn-sm"><i
                                                            class="fas fa-pencil-alt"></i></button>
                                                        <button wire:click="showDestroy({{ $item->id }})" type="button" class="btn btn-danger btn-sm"><i
                                                            class="fas fa-trash"></i></button>
                                                      </div>
                                                </td>
                                                
                                                <td>{{ $num++ }}</td>
                                                <td>{{ $item->name }}</td>
                                                <td>{{ $item->number }}</td>                                               
                                        <td>{{ date('d-m-Y', strtotime($item->date)) }}</td>
                                        </form>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                <div class="float-right">
                                    {{ $general->links() }}
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- /.modal-add -->
    <div wire:ignore.self class="modal fade" id="modal-add">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><i class="fa fa-plus text-success"></i> ເພີ່ມໃຫມ່</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>ລານການ</label>
                                    <input wire:model="name" placeholder="ປ້ອນຂໍ້ມູນ" type="text"
                                        class="form-control @error('name') is-invalid @enderror">
                                    @error('name')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>ຈຳນວນເງິນ</label>
                                    <input wire:model="number" placeholder="ປ້ອນຂໍ້ມູນ" min="1" type="tel"
                                        class="form-control money @error('number') is-invalid @enderror" autofocus>
                                    @error('number')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>ວ.ດ.ປ</label>
                                    <input wire:model="date" placeholder="ປ້ອນຂໍ້ມູນ" min="1" type="date"
                                        class="form-control money @error('date') is-invalid @enderror" autofocus>
                                    @error('date')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                </div>
                </form>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">ຍົກເລີກ</button>
                    <button wire:click="store" type="button" class="btn btn-success">ບັນທຶກ</button>
                </div>

            </div>
        </div>
    </div>

    <!-- /.modal-edit -->
    <div wire:ignore.self class="modal fade" id="modal-edit">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><i class="fa fa-edit text-warning"></i> ແກ້ໄຂ</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>ລານການ</label>
                                    <input wire:model="name" placeholder="ປ້ອນຂໍ້ມູນ" type="text"
                                        class="form-control @error('name') is-invalid @enderror">
                                    @error('name')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>ຈຳນວນເງິນ</label>
                                    <input wire:model="number" placeholder="ປ້ອນຂໍ້ມູນ" min="1" type="tel"
                                        class="form-control money @error('number') is-invalid @enderror" autofocus>
                                    @error('number')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>ວ.ດ.ປ</label>
                                    <input wire:model="date" placeholder="ປ້ອນຂໍ້ມູນ" min="1" type="date"
                                        class="form-control money @error('date') is-invalid @enderror" autofocus>
                                    @error('date')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                  
                </div>
                </form>

                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">ຍົກເລີກ</button>
                    <button wire:click="update" type="button" class="btn btn-success">ບັນທຶກ</button>
                </div>

            </div>
        </div>
    </div>

    <!-- /.modal-delete -->
    <div class="modal fade" id="modal-delete">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title"><i class="fa fa-trash"></i></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <input type="hidden" wire:model="hiddenId">
                    <h3>ຕ້ອງການລຶບອອກບໍ່ຂະນ້ອຍ?</h3>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">ຍົກເລີກ</button>
                    <button wire:click="destroy({{ $hiddenId }})" type="button"
                        class="btn btn-success">ລຶບອອກ</button>
                </div>
            </div>
        </div>
    </div>

</div>

@push('scripts')
    <script>
        window.addEventListener('show-modal-add', event => {
            $('#modal-add').modal('show');
        })
        window.addEventListener('hide-modal-add', event => {
            $('#modal-add').modal('hide');
        })
        window.addEventListener('show-modal-edit', event => {
            $('#modal-edit').modal('show');
        })
        window.addEventListener('hide-modal-edit', event => {
            $('#modal-edit').modal('hide');
        })
        window.addEventListener('show-modal-delete', event => {
            $('#modal-delete').modal('show');
        })
        window.addEventListener('hide-modal-delete', event => {
            $('#modal-delete').modal('hide');
        })
    </script>
@endpush
