<div>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3">
                    <h5 class="text-danger"><i class="fa fa-book"></i> ບັນທຶກລາຍຈ່າຍ</h5>
                </div>
                {{-- <div class="col-sm-9">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">ຫນ້າຫລັກ</a></li>
                        <li class="breadcrumb-item active">ບັນທຶກລາຍຈ່າຍ</li>
                    </ol>
                </div> --}}
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">


                <!--List users- table table-bordered table-striped -->

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="col-md-6">
                                            @foreach ($rolepermissions as $items)
                                                @if ($items->permissionname->name == 'action_expend')
                                                    <a wire:click="create" class="btn btn-primary btn-lg"
                                                        href="javascript:void(0)"><i class="fa fa-plus"></i>
                                                        {{ __('lang.add') }}</a>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">

                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                    </div>
                                    <div class="input-group input-group-sm" style="width: 240px;">
                                        <input wire:model="search" type="text" class="form-control"
                                            placeholder="{{ __('lang.search') }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead class="bg-danger">
                                        <tr>
                                            @foreach ($rolepermissions as $items)
                                                @if ($items->permissionname->name == 'action_expend')
                                                    <th>ຈັດການ</th>
                                                @endif
                                            @endforeach
                                            <th>ລຳດັບ</th>
                                            {{-- <th>ຮູບໃບບິນ</th> --}}
                                            <th>ຈ່າຍຄ່າ</th>
                                            <th>ຈຳນວນເງິນ</th>
                                            <th>ຄັງເງິນ</th>
                                            <th>ປະເພດເງິນ</th>
                                            <th>ວ.ດ.ປ</th>
                                            <th>ຜູ້ບັນທຶກ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $num = 1;
                                        @endphp

                                        @foreach ($expend as $item)
                                            <tr>
                                                @foreach ($rolepermissions as $items)
                                                    @if ($items->permissionname->name == 'action_expend')
                                                        <td>
                                                            <div class="btn-group">
                                                                <button wire:click="edit({{ $item->id }})"
                                                                    type="button" class="btn btn-warning btn-sm"><i
                                                                        class="fas fa-pencil-alt"></i></button>
                                                                <button wire:click="showDestroy({{ $item->id }})"
                                                                    type="button" class="btn btn-danger btn-sm"><i
                                                                        class="fas fa-trash"></i></button>
                                                            </div>
                                                        </td>
                                                    @endif
                                                @endforeach

                                                <td>{{ $num++ }}</td>
                                                {{-- <td>
                                                    @if (!empty($item->image))
                                                    <a href="{{$item->image}}">
                                                        <img src="{{$item->image}}"
                                                            width="80px;" height="50px;">
                                                    </a>
                                                @else
                                                <p class="text-center">ບໍ່ມີໃບບິນ</p>
                                                @endif
                                                </td> --}}
                                                {{-- <td>
                                                    @if (!empty($item->image))
                                                        <a href="{{ asset('public/bill_photo') }}/{{ $item->image }}">
                                                            <img src="{{ asset('public/bill_photo') }}/{{ $item->image }}"
                                                                width="80px;" height="50px;">
                                                        </a>
                                                    @else
                                                        <p class="text-center">ບໍ່ມີໃບບິນ</p>
                                                    @endif
                                                </td> --}}
                                                <td>{{ $item->name }}</td>
                                                <td>{{ number_format($item->qty_price) }}
                                                    @if ($item->currency == 1)
                                                        <b class="text-success">ກີບ</b>
                                                    @elseif($item->currency == 2)
                                                        <b class="text-warning">ບາດ</b>
                                                    @elseif($item->currency == 3)
                                                        <b class="text-danger">ໂດລາ</b>
                                                    @else
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($item->type_donate == 1)
                                                        <b class="text-danger">ຄັງບໍລິຫານ</b>
                                                    @elseif($item->type_donate == 2)
                                                        <b class="text-primary">ຄັງກໍ່ສ້າງ</b>
                                                    @endif
                                                </td>
                                                {{-- <td>
                                                  
                                                </td> --}}

                                                <td class="text-center">
                                                    @if ($item->type == 1)
                                                        <p class="text-success">ເງິນສົດ</p>
                                                    @elseif($item->type == 2)
                                                        <p class="text-info">ເງິນໃນບັນຊີ</p>
                                                    @else
                                                <td></td>
                                        @endif
                                        </td>
                                        <td>{{ date('d-m-Y', strtotime($item->created_at)) }}</td>
                                        <td>
                                            @if (!empty($item->creator))
                                                {{ $item->creator->name }}
                                            @endif
                                        </td>
                                        </form>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                <div class="float-right">
                                    {{ $expend->links() }}
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.modal-add -->
    <div wire:ignore.self class="modal fade" id="modal-add">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><i class="fa fa-plus text-success"></i> ເພີ່ມໃຫມ່</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form>
                        {{-- <div class="container">
                            <div wire:ignore class="avatar-upload">
                                <div class="avatar-edit">
                                    <input type='file' wire:model="image" id="imageUpload"
                                        accept=".png, .jpg, .jpeg" />
                                    <label for="imageUpload"></label>
                                </div>
                                <label class="text-center">ໃສ່ຮູບໃບບິນ(ຖ້າມີ)</label>
                                <div class="avatar-preview">
                                    <div id="imagePreview"
                                        style="background-image: url({{ asset('logo/logo1.jpg') }});">
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>ຈ່າຍຄ່າ</label>
                                    <input wire:model="name" type="text"
                                        class="form-control @error('name') is-invalid @enderror">
                                    @error('name')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>ຈຳນວນເງິນ@if ($qty_price)
                                        = {{ number_format($qty_price) }}
                                     @endif</label>
                                    <input wire:model="qty_price" min="1" type="tel"
                                        class="form-control @error('qty_price') is-invalid @enderror" autofocus>
                                    @error('qty_price')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <label>ເລືອກຄັງເງິນ</label>
                                <div class="form-group clearfix">
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary6" value="1" wire:model="type_donate"
                                            checked>
                                        <label for="radioPrimary6">ບໍລິຫານ
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary7" value="2"
                                            wire:model="type_donate">
                                        <label for="radioPrimary7">ກໍ່ສ້າງ
                                        </label>
                                    </div>
                                </div>

                                @error('type')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-sm-3">
                                <label>ເລືອກສະກຸນເງິນ</label>
                                <div class="form-group clearfix">
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary1" value="1"
                                            wire:model="currency" checked>
                                        <label for="radioPrimary1">ກີບ
                                        </label>
                                    </div>

                                </div>
                                <div class="form-group clearfix">
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary2" value="2"
                                            wire:model="currency">
                                        <label for="radioPrimary2">ບາດ
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary3" value="3"
                                            wire:model="currency">
                                        <label for="radioPrimary3">ໂດລາ
                                        </label>
                                    </div>
                                </div>
                                @error('currency')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-sm-3">
                                <label>ເລືອກປະເພດເງິນ</label>
                                <div class="form-group clearfix">
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary4" value="1" wire:model="type"
                                            checked>
                                        <label for="radioPrimary4">ເງິນສົດ
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary5" value="2" wire:model="type">
                                        <label for="radioPrimary5">ເງິນໃນບັນຊີ
                                        </label>
                                    </div>
                                </div>
                                @error('type')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                            </div>

                        </div>
                </div>
                </form>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">ຍົກເລີກ</button>
                    <button wire:click="store" type="button" class="btn btn-success">ບັນທຶກ</button>
                </div>

            </div>
        </div>
    </div>


    <!-- /.modal-edit -->
    <div wire:ignore.self class="modal fade" id="modal-edit">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><i class="fa fa-edit text-warning"></i> ແກ້ໄຂ</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form>
                        <div class="row">
                            {{-- <div class="container">
                                <div class="avatar-upload">
                                    <div class="avatar-edit">
                                        <input type='file' wire:model="image" id="imageUpload2"
                                            accept=".png, .jpg, .jpeg" />
                                        <label for="imageUpload2"></label>
                                    </div>
                                    @if ($image)
                                        <div id="imagePreview2" class="avatar-preview">
                                            <img src="{{ $image->temporaryUrl() }}" alt="" width="120px;">
                                        </div>
                                    @else
                                        @if ($newimage)
                                            <div id="imagePreview2" class="avatar-preview">
                                                <img src="{{ asset('public/bill_photo') }}/{{ $newimage }}"
                                                    alt="" width="120px;">
                                            </div>
                                        @else
                                            <div class="avatar-preview">
                                                <div id="imagePreview2"
                                                    style="background-image: url({{ asset('logo/logo1.jpg') }});">
                                                </div>
                                            </div>
                                        @endif
                                    @endif
                                </div>
                            </div> --}}
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>ຈ່າຍຄ່າ</label>
                                    <input wire:model="name" type="text"
                                        class="form-control @error('name') is-invalid @enderror">
                                    @error('name')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>ຈຳນວນເງິນ@if ($qty_price)
                                        = {{ number_format($qty_price) }}
                                     @endif</label>
                                    <input wire:model="qty_price" min="1" type="tel"
                                        class="form-control @error('qty_price') is-invalid @enderror" autofocus>
                                    @error('qty_price')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>ວ.ດ.ປ</label>
                                    <input wire:model="created_at" type="date"
                                        class="form-control @error('created_at') is-invalid @enderror" autofocus>
                                    @error('created_at')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <label>ເລືອກຄັງເງິນ</label>
                                <div class="form-group clearfix">
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary6" value="1"
                                            wire:model="type_donate" checked>
                                        <label for="radioPrimary6">ບໍລິຫານ
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary7" value="2"
                                            wire:model="type_donate">
                                        <label for="radioPrimary7">ກໍ່ສ້າງ
                                        </label>
                                    </div>
                                </div>

                                @error('type')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-sm-3">
                                <label>ເລືອກສະກຸນເງິນ</label>
                                <div class="form-group clearfix">
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary1" value="1"
                                            wire:model="currency" checked>
                                        <label for="radioPrimary1">ກີບ
                                        </label>
                                    </div>

                                </div>
                                <div class="form-group clearfix">
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary2" value="2"
                                            wire:model="currency">
                                        <label for="radioPrimary2">ບາດ
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary3" value="3"
                                            wire:model="currency">
                                        <label for="radioPrimary3">ໂດລາ
                                        </label>
                                    </div>
                                </div>
                                @error('currency')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-sm-3">
                                <label>ເລືອກປະເພດເງິນ</label>
                                <div class="form-group clearfix">
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary4" value="1" wire:model="type"
                                            checked>
                                        <label for="radioPrimary4">ເງິນສົດ
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary5" value="2" wire:model="type">
                                        <label for="radioPrimary5">ເງິນໃນບັນຊີ
                                        </label>
                                    </div>
                                </div>
                                @error('type')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                </div>
                </form>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">ຍົກເລີກ</button>
                    <button wire:click="update" type="button" class="btn btn-success">ບັນທຶກ</button>
                </div>

            </div>
        </div>
    </div>

    <!-- /.modal-delete -->
    <div class="modal fade" id="modal-delete">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title"><i class="fa fa-trash"></i></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <input type="hidden" wire:model="hiddenId">
                    <h3>ຕ້ອງການລຶບອອກບໍ່ຂະນ້ອຍ?</h3>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">ຍົກເລີກ</button>
                    <button wire:click="destroy({{ $hiddenId }})" type="button"
                        class="btn btn-success">ລຶບອອກ</button>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        window.addEventListener('show-modal-add', event => {
            $('#modal-add').modal('show');
        })
        window.addEventListener('hide-modal-add', event => {
            $('#modal-add').modal('hide');
        })
        window.addEventListener('show-modal-edit', event => {
            $('#modal-edit').modal('show');
        })
        window.addEventListener('hide-modal-edit', event => {
            $('#modal-edit').modal('hide');
        })
        window.addEventListener('show-modal-delete', event => {
            $('#modal-delete').modal('show');
        })
        window.addEventListener('hide-modal-delete', event => {
            $('#modal-delete').modal('hide');
        })

        // ================= avatar image edit==========================
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
                    $('#imagePreview').hide();
                    $('#imagePreview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imageUpload").change(function() {
            readURL(this);
        });
    </script>
@endpush
