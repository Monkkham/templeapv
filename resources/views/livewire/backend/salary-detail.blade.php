 <!-- /.modal-delete -->
 <div wire:ignore.self class="modal fade" id="modal-detail">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <h5 class="modal-title"><i class="fas fa-money-bill"></i> {{ $name }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-13">
                    <div class="col-md-13">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="text-center bg-light text-bold">
                                    <th>ລຳດັບ</th>
                                    <th>ວ.ດ.ປ ຮັບເງິນເດືອນ</th>
                                    <th>ສະກຸນເງິນ</th>
                                    <th>ປະເພດເງິນ</th>
                                    <th>ຈັດການ</th>

                                </tr>
                            </thead>
                            <tbody>

                                @php $no = 1 @endphp
                                @foreach ($this->list_detail as $item)
                                    <tr class="text-center">
                                        <td>{{ $no++ }}</td>
                                        </td>
                                        <td>{{ date('d/m/Y', strtotime($item->created_at)) }}</td>
                                        {{-- <td>{{ date('d/m/Y H:i:s', strtotime($item->created_at)) }}</td> --}}
                                        <td>{{ number_format($item->money) }}
                                            @if ($item->currency == 1)
                                                <b class="text-success">ກີບ</b>
                                            @elseif($item->currency == 2)
                                                <b class="text-warning">ບາດ</b>
                                            @elseif($item->currency == 3)
                                                <b class="text-danger">ໂດລາ</b>
                                            @else
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            @if ($item->type == 1)
                                                <p class="text-success">ເງິນສົດ</p>
                                            @elseif($item->type == 2)
                                                <p class="text-info">ເງິນໂອນ</p>
                                            @else
                                        <td></td>
                                       @endif
                                       </td>
                                       <td>
                                        <button wire:click="show_paymoney({{ $item->id }})"
                                            type="button" class="btn btn-info btn-sm"><i
                                                class="fas fa-plus-circle"></i> ຖອນເງິນ</button>
                                       </td>
                                       </tr>
                                       @endforeach
                                <tr class="text-center">
                                    <td class="bg-light text-bold text-right" colspan="2">
                                        <i>ລວມເງິນກີບ</i>
                                    </td>
                                    <td class="text-bold">
                                        @if (!empty($sum_lak))
                                            {{ number_format($sum_lak) }}
                                        @else
                                            0
                                        @endif
                                    </td>
                                    <td class="bg-light text-left" colspan="2">LAK</td>

                                </tr>
                                <tr class="text-center">
                                    <td class="bg-light text-bold text-right" colspan="2">
                                        <i>ລວມເງິນບາດ</i>
                                    </td>
                                    <td class="text-bold">
                                        @if (!empty($sum_thb))
                                            {{ number_format($sum_thb) }}
                                        @else
                                            0
                                        @endif
                                    </td>
                                    <td class="bg-light text-left" colspan="2">THB</td>

                                </tr>
                                <tr class="text-center">
                                    <td class="bg-light text-bold text-right" colspan="2">
                                        <i>ລວມເງິນໂດລາ</i>
                                    </td>
                                    <td class="text-bold">
                                        @if (!empty($sum_usd))
                                            {{ number_format($sum_usd) }}
                                        @else
                                            0
                                        @endif
                                    </td>
                                    <td class="bg-light text-left" colspan="2">USD</td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                    {{-- <hr>
                    <div class="form-group">
                        <label><i>ລວມຍອດປັດໄຈທີ່ເຄີຍໂມທະນາ</i>

                    </div> --}}
                </div>

            </div>
            {{-- <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-danger" data-dismiss="modal">ຍົກເລີກ</button>
                <button wire:click="new_donate()" type="button" class="btn btn-success">ບັນທຶກ</button>
            </div> --}}
        </div>
    </div>
</div>


@push('scripts')
    <script>
        window.addEventListener('show-detail', event => {
            $('#modal-detail').modal('show');
        })
        window.addEventListener('hide-detail', event => {
            $('#modal-detail').modal('hide');
        })
    </script>
@endpush
