 <!-- /.modal-delete -->
 <div wire:ignore.self class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <h5 class="modal-title"><i class="fas fa-pray"></i> {{ $name_lastname }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-13">
                    <div class="col-md-13">
                        <div class="form-group">
                            <label>ຈຳນວນປັດໃຈ</label>
                            <input wire:model="total_money" type="tel" placeholder="ປ້ອນຂໍ້ມູນໃຫມ່"
                                class="form-control @error('total_money') is-invalid @enderror">
                            @error('total_money')
                                <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                   
                    <div class="row">
                            <div class="col-sm-6">
                                <label>ເລືອກສະກຸນເງິນ</label>
                                <div class="form-group clearfix">
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary1" value="1"
                                            wire:model="currency">
                                        <label for="radioPrimary1">ກີບ
                                        </label>
                                    </div>

                                </div>
                                <div class="form-group clearfix">
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary2" value="2"
                                            wire:model="currency">
                                        <label for="radioPrimary2">ບາດ
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary3" value="3"
                                            wire:model="currency">
                                        <label for="radioPrimary3">ໂດລາ
                                        </label>
                                    </div>
                                </div>
                                @error('currency')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-sm-6">
                                <label>ເລືອກປະເພດເງິນ</label>
                                <div class="form-group clearfix">
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary4" value="1" wire:model="type">
                                        <label for="radioPrimary4">ເງິນສົດ
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary5" value="2"
                                            wire:model="type">
                                        <label for="radioPrimary5">ເງິນໃນບັນຊີ
                                        </label>
                                    </div>
                                </div>
                                @error('type')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                            </div>

                        </div>
                        <hr>
                        <div class="form-group">
                            <label><i>ລວມຍອດປັດໄຈທີ່ເຄີຍໂມທະນາ</i> <i class="fas fa-praying-hands text-warning"></i></label>
                            <h6 class="text-success">ເງິນກີບ: {{ number_format($sum_lak) }} LAK</h6>
                            <h6 class="text-warning">ເງິນບາດ: {{ number_format($sum_thb) }} THB</h6>
                            <h6 class="text-danger">ເງິນໂດລາ: {{ number_format($sum_usd) }} USD</h6>
                           
                        </div>
                </div>
               
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-danger" data-dismiss="modal">ຍົກເລີກ</button>
                <button wire:click="new_donate()" type="button"
                    class="btn btn-success">ບັນທຶກ</button>
            </div>
        </div>
    </div>
</div>


@push('scripts')
    <script>
        window.addEventListener('show-modal-add', event => {
            $('#modal-add').modal('show');
        })
        window.addEventListener('hide-modal-add', event => {
            $('#modal-add').modal('hide');
        })
        window.addEventListener('show-modal-edit', event => {
            $('#modal-edit').modal('show');
        })
        window.addEventListener('hide-modal-edit', event => {
            $('#modal-edit').modal('hide');
        })
        window.addEventListener('show-modal-delete', event => {
            $('#modal-delete').modal('show');
        })
        window.addEventListener('hide-modal-delete', event => {
            $('#modal-delete').modal('hide');
        })
        // ============== select 2 ===============
        // $(document).ready(function() {
        //     $('#select_data1').select2();
        //     $('#select_data1').on('change', function(e) {
        //         var data = $('#select_data1').select2("val");
        //         @this.set('province_id', data);
        //     });
        // });
        // $(document).ready(function() {
        //     $('#select_data2').select2();
        //     $('#select_data2').on('change', function(e) {
        //         var data = $('#select_data2').select2("val");
        //         @this.set('district_id', data);
        //     });
        // });
        $(document).ready(function() {
            $('#select_data3').select2();
            $('#select_data3').on('change', function(e) {
                var data = $('#select_data3').select2("val");
                @this.set('search_people', data);
            });
        });
        // ==========
    </script>
@endpush
