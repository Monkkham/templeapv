<div>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <b><i class="fa fa-users"></i> {{ __('lang.users') }}</b>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">{{ __('lang.home') }}</a></li>
                        <li class="breadcrumb-item active">{{ __('lang.users') }}</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <style>
        .control-user {
            display: flex;
            flex-direction: column;
            align-items: center;
        }

        .user-profile {
            position: relative;
            text-align: center;
            width: 200px;
            height: 200px;
            border-radius: 50%;
            overflow: hidden;
            cursor: pointer;
        }

        .user-profile img {
            position: relative;
            width: 100%;
            height: 100%;
            object-fit: ;
        }
    </style>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!--customers -->
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="row">
                                        <div class="col-md-6">
                                            {{-- @foreach ($rolepermissions as $items)
                        @if ($items->permissionname->name == 'createUser')
                            <a wire:click="create" class="btn btn-primary" href="javascript:void(0)"><i class="fa fa-plus"></i> {{__('lang.add')}}</a>
                        @endif
                        @endforeach --}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">

                                </div>
                                <div class="input-group input-group-sm" style="width: 240px;">
                                    <input wire:model="search" type="text" class="form-control"
                                        placeholder="{{ __('lang.search') }}">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-default">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead class="bg-primary">
                                        <tr style="text-align: center">
                                            <th>{{ __('lang.no') }}</th>
                                            <th>{{ __('lang.image') }}</th>
                                            <th>{{ __('lang.name') }} {{ __('lang.lastname') }}</th>
                                            <th>{{ __('lang.phone') }}</th>
                                            <th>ອີເມວ</th>
                                            
                                            <th>ສິດນຳໃຊ້</th>
                                        
                                            @foreach ($rolepermissions as $items)
                                            @if ($items->permissionname->name == 'action_roles')
                                            <th>{{ __('lang.action') }}</th>
                                            @endif
                                            @endforeach
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $stt = 1;
                                        @endphp
                                        @foreach ($user as $item)
                                            <tr style="text-align: center">
                                                <td>{{ $stt++ }}</td>
                                                <td><i class="fas fa-user-shield text-lg"></i></td>
                                                <td>{{ $item->name }} {{ $item->lastname }}</td>
                                                <td>{{ $item->phone }}</td>
                                                <td>{{ $item->email }}</td>
                                                <td>
                                                    @if (!empty($item->role))
                                                        <p class="bg-success rounded">{{ $item->role->name }}</p>
                                                    @endif
                                                </td>
                                                <td>
                                                    {{-- <button wire:click="showEdit({{ $item->id }})" type="button"
                                                        class="btn btn-warning btn-sm"><i
                                                            class="fas fa-pen-alt"></i></button> --}}
                                                    {{-- @if (auth()->user()->roles == '1') --}}
                                                    @foreach ($rolepermissions as $items)
                                                    @if ($items->permissionname->name == 'action_roles')
                                                    <button wire:click="showEdit({{ $item->id }})"
                                                        type="button" class="btn bg-gradient-pink btn-sm"> <i
                                                            class="fas fa-handshake"></i> ມອບສິດ</button>
                                                    @endif
                                                    @endforeach
                                                </td>
                                                </form>
                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                                <div>
                                    {{-- {{ $user->links() }} --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- ================== incloud link file ======================= --}}
{{-- @include('livewire.backend.user-update') --}}
@include('livewire.backend.user-delete')
</div>
