
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-3">
            <h3 class="m-0"><i class="nav-icon fas fa-tachometer-alt"></i> ຫນ້າຫຼັກ</h3>
          </div><!-- /.col -->
          {{-- <div class="col-sm-9">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">ຫນ້າຫລັກ</a></li>
              <li class="breadcrumb-item active">ຫນ້າຫລັກ</li>
            </ol>
          </div><!-- /.col --> --}}
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-6 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h5>{{ $count_income }} ລາຍການ</h5>
                <h6><small>ເງິນກີບ {{ number_format($sum_income_lak) }} LAK</small></h6>
                <h6>ເງິນບາດ {{ number_format($sum_income_thb) }} THB</h6>
                <h6>ເງິນໂດລາ {{ number_format($sum_income_usd) }} USD</h6>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="{{ route('backend.income') }}" class="small-box-footer">ລາຍຮັບທັງຫມົດ <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
            <!-- ./col -->
            <div class="col-lg-6 col-6">
              <!-- small box -->
              <div class="small-box bg-danger">
                <div class="inner">
                  <h5>{{ $count_expend }} ລາຍການ</h5>
                  <h6><small>ເງິນກີບ {{ number_format($sum_expend_lak) }} LAK</small></h6>
                <h6>ເງິນບາດ {{ number_format($sum_expend_thb) }} THB</h6>
                <h6>ເງິນໂດລາ {{ number_format($sum_expend_usd) }} USD</h6>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="{{ route('backend.expend') }}" class="small-box-footer">ລາຍຈ່າຍທັງຫມົດ <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>

                    <!-- ./col -->
                    {{-- <div class="col-lg-3 col-6">
                      <!-- small box -->
                      <div class="small-box bg-info">
                        <div class="inner">
                          <h5>ຈຳນວນພຣະສົງໃນວັດ</h5>
                          <span class="info-box-number">ທັງຫມົດ: {{ $count_all_monk }} <small>ອົງ</small></span><br>
                          <span class="info-box-number">ພຣະອາຈານ: {{ $count_teach_monk }} <small>ອົງ</small></span><br>
                          <span class="info-box-number">ພຣະ: {{ $count_monk }} <small>ອົງ</small></span><br>
                          <span class="info-box-number">ສຳມະເນນ: {{ $count_novice }} <small>ອົງ</small></span>
                        </div>
                        <div class="icon">
                          <i class="fas fa-users"></i>
                        </div>
                        <a href="#" class="small-box-footer">{{ __('lang.description') }} <i class="fas fa-arrow-circle-right"></i></a>
                      </div>
                    </div> --}}

                    
            <!-- ./col -->
            {{-- <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-warning">
                <div class="inner">
                  <h4>{{ $count_expend }} ລາຍການ</h4>
                  <h6>ເປັນເງິນ {{ number_format($sum_expend) }} ກີບ</h6>
  
                  <p>ລາຍຈ່າຍທັງຫມົດ</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="#" class="small-box-footer">{{ __('lang.description') }} <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div> --}}

          <!-- ./col -->
        </div>
        <!-- /.row -->
        
        <div class="row">
            <div class="col-md-3 col-sm-6 col-12">
              <div class="info-box">
                <span class="info-box-icon bg-info"><i class="fas fa-cart-arrow-down"></i></span>
    
                <div class="info-box-content">
                  <span class="info-box-text">{{ __('lang.quantity_seller') }}</span>
                  <span class="info-box-number">1,410 <small>{{ __('lang.list') }}</small></span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-12">
              <div class="info-box">
                <span class="info-box-icon bg-warning"><i class="fas fa-hand-holding-usd"></i></span>
    
                <div class="info-box-content">
                  <span class="info-box-text">{{ __('lang.quantity_debt') }}</span>
                  <span class="info-box-number">410 <small>{{ __('lang.list') }}</small></span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-12">
              <div class="info-box">
                <span class="info-box-icon bg-success"><i class="fa fa-users"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">ຈຳນວນພຣະສົງໃນວັດ</span>
                  {{-- <span class="info-box-number">ທັງຫມົດ: {{ $count_all_monk }} <small>ອົງ</small></span>
                  <span class="info-box-number">ພຣະອາຈານ: {{ $count_teach_monk }} <small>ອົງ</small></span>
                  <span class="info-box-number">ພຣະ: {{ $count_monk }} <small>ອົງ</small></span>
                  <span class="info-box-number">ສຳມະເນນ: {{ $count_novice }} <small>ອົງ</small></span> --}}
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-12">
              <div class="info-box">
                <span class="info-box-icon bg-danger"><i class="fa fa-users"></i></span>
    
                <div class="info-box-content">
                  <span class="info-box-text">ຜູ້ໃຊ້ງານລະບົບ</span>
                  <span class="info-box-number">{{ $count_user }} <small>ອົງ</small></span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
          </div>
          {{-- <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered">

                      <thead>
                      <tr class="text-center bg-info text-md">
                            <th colspan="8">ລາຍຊື່ພະສົງຂາດກິດຈະວັດ</th>
                      </tr>
                      <tr class="text-center bg-warning">
                        <th>{{__('lang.no')}}</th>
                        <th>{{__('lang.created_at')}}</th>
                        <th>{{__('lang.name')}}{{__('lang.customers')}}</th>
                        <th>{{__('lang.detail')}}{{__('lang.land')}}</th>
                        <th>{{__('lang.nguad')}}</th>
                        <th>{{__('lang.selling_price')}}</th>
                        <th>{{__('lang.balance')}}</th>
                        <th>{{__('lang.status')}}</th>
                      </tr>
                      </thead>
                      <tbody>
                    
                      </tbody>
                    </table>
                    <div class="d-flex justify-content-between">
                      <div>
                          {{$sale->links()}}
                      </div>
                    </div>
                  </div>
            </div>
          </div> --}}
          <!-- /.row -->
        
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->



