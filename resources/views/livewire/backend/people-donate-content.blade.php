<div>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3">
                    <h5 class="text-success"><i class="fas fa-pray"></i> ບັນທຶກລາຍຊື່ເຈົ້າສັດທາ</h5>
                </div>
                {{-- <div class="col-sm-9">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">ຫນ້າຫລັກ</a></li>
                        <li class="breadcrumb-item active">ບັນທຶກລາຍຊື່ເຈົ້າສັດທາ</li>
                    </ol>
                </div> --}}
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!--List users- table table-bordered table-striped -->
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="col-md-6">
                                            {{-- @foreach ($rolepermissions as $items)
                        @if ($items->permissionname->name == 'createUser') --}}
                                            <a wire:click="create" class="btn btn-primary btn-lg"
                                                href="javascript:void(0)"><i class="fa fa-plus"></i>
                                                {{ __('lang.add') }}</a>
                                            {{-- @endif
                        @endforeach --}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <input wire:model="search" type="text" class="form-control"
                                            placeholder="{{ __('lang.search') }}">
                                    </div>
                                    <div class="col-md-6">
                                        <select wire:model="search_people" id="select_data3" class="form-control">
                                          <option value="">ເລືອກເຈົ້າສັດທາ</option>
                                          @foreach($people_donate as $item)
                                          <option value="{{ $item->id }}">{{ $item->name_lastname }}</option>
                                          @endforeach
                                        </select>
                                      </div> 
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="example1" class="table table-hover">
                                    <thead class="bg-success">
                                        <tr>
                                            <th>ຈັດການ</th>
                                            <th>ລຳດັບ</th>
                                            <th>ຊຶ່ ນາມສະກຸນ</th>
                                            <th>ທີ່ຢູ່</th>
                                            <th>ວ.ດ.ປ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $num = 1;
                                        @endphp
                                        @foreach ($select_people as $item)
                                            <tr>
                                                <td>
                                                    <div class="btn-group">
                                                        <button wire:click="show_new_donate({{ $item->id }})"
                                                            type="button" class="btn btn-info btn-sm"><i
                                                                class="fas fa-plus"></i> ໂມທະນາ</button>
                                                        <button wire:click="edit({{ $item->id }})" type="button"
                                                            class="btn btn-warning btn-sm"><i
                                                                class="fas fa-pencil-alt"></i> ແກ້ໄຂ</button>

                                                    </div>
                                                </td>
                                                <td>{{ $num++ }}</td>
                                                <td>{{ $item->name_lastname }}</td>
                                                @if (!empty($item->village->name))
                                                    <td>{{ $item->village->name }}, {{ $item->district->name }},
                                                        {{ $item->province->name }}</td>
                                                @else
                                                    <td></td>
                                                @endif

                                                {{-- <td>{{ $item->donate }}</td>
                                                @if (!empty($item->total_money))
                                                    <td>{{ number_format($item->total_money) }}
                                                        @if ($item->currency == 1)
                                                            <p class="text-success">ກີບ</p>
                                                        @elseif($item->currency == 2)
                                                            <p class="text-warning">ບາດ</p>
                                                        @elseif($item->currency == 3)
                                                            <p class="text-danger">ໂດລາ</p>
                                                        @endif
                                                    </td>
                                                @else
                                                    <td></td>
                                                @endif --}}
                                                {{-- 

                                                <td>{{ $item->qty }}</td>
                                                <td class="text-center">
                                                    @if ($item->type == 1)
                                                        <p class="text-success">ເງິນສົດ</p>
                                                    @elseif($item->type == 2)
                                                        <p class="text-info">ເງິນໃນບັນຊີ</p>
                                                    @else
                                                <td></td>
                                        @endif
                                        </td> --}}
                                                <td>{{ date('d-m-Y', strtotime($item->created_at)) }}</td>
                                                </form>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="float-right">
                                    {{ $select_people->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('livewire.backend.people-donate-create')
    @include('livewire.backend.people-donate-update')
    @include('livewire.backend.people-donate-new')
</div>
