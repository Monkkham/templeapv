<!-- /.modal-edit -->
<div wire:ignore.self class="modal fade" id="modal-edit">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header bg-warning">
                <h4 class="modal-title"><i class="fa fa-edit"></i> {{ __('lang.edit') }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @if (Session::has('no_match_password'))
                    <div class="alert alert-danger text-center">
                        {{ __('lang.not_match_password') }}
                    </div>
                @endif
                <form>
                    <div class="container">
                        <div wire:ignore class="avatar-upload">
                            <div class="avatar-edit">
                                <input type='file' wire:model="image" id="imageUpload" accept=".png, .jpg, .jpeg" />
                                <label for="imageUpload"></label>
                            </div>
                            <div class="avatar-preview">
                                @if ($image)
                                    <img src="{{ $image->temporaryUrl() }}" alt="" width="120px;">
                                    {{-- <div id="imagePreview" style="background-image: url({{ asset('logo/logo.jpg') }});"> --}}
                                @else
                                    @if ($new_image)
                                        <div class="user-profile" style="border: 5px solid rgb(167, 167, 13)">
                                            <img src="{{ asset($new_image) }}" alt="">
                                        </div>
                                    @else
                                    <div id="imagePreview" style="background-image: url({{ asset('logo/logo.jpg') }});">
                                    @endif
                                @endif
                            </div>
                        </div>
                    </div>
            </div>
            {{-- <div class="control-user">
                @if ($image)
                    <div class="user-profile" style="border: 5px solid green;">
                        <img src="{{ $image->temporaryUrl() }}" alt="" width="120px;">
                    </div>
                    <i class="fas fa-check-circle" style="color:green;font-size:20px;padding:10px;"></i>
                @else
                    @if ($new_image)
                        <div class="user-profile" style="border: 5px solid rgb(167, 167, 13)">
                            <img src="{{ asset($new_image) }}" alt="">
                        </div>
                    @else
                        <div class="user-profile" style="border: 5px solid #000">
                            <img src="{{ asset('img/logo.jpg') }}" alt="">
                        </div>
                    @endif
                @endif
                <div class="col-md-3">
                    <div class="form-group">
                        <label>{{ __('lang.image') }}</label>
                        <input wire:model="img" type="file" placeholder="{{ __('lang.img') }}"
                            class="form-control @error('img') is-invalid @enderror">
                        @error('img')
                            <span style="color: red" class="error">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
            </div> --}}
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>{{ __('lang.name') }}</label>
                        <input wire:model="name" type="text" placeholder="{{ __('lang.name') }}"
                            class="form-control @error('name') is-invalid @enderror">
                        @error('name')
                            <span style="color: red" class="error">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>{{ __('lang.lastname') }}</label>
                        <input wire:model="lastname" type="text" placeholder="{{ __('lang.lastname') }}"
                            class="form-control @error('lastname') is-invalid @enderror">
                        @error('lastname')
                            <span style="color: red" class="error">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>{{ __('lang.village') }}</label>
                        <select wire:model="village_id" class="form-control">
                            <option value="" selected>{{ __('lang.select') }}</option>
                            <option value="famale">{{ __('lang.famale') }}</option>
                            <option value="male">{{ __('lang.male') }}</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>{{ __('lang.birthday') }}</label>
                        <input wire:model="birthday" type="date" placeholder="{{ __('lang.birthday') }}"
                            class="form-control @error('birthday') is-invalid @enderror">
                        @error('birthday')
                            <span style="color: red" class="error">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>{{ __('lang.phone') }}</label>
                        <input wire:model="phone" type="tel" placeholder="{{ __('lang.phone') }}"
                            class="form-control @error('phone') is-invalid @enderror">
                        @error('phone')
                            <span style="color: red" class="error">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>{{ __('lang.email') }}</label>
                        <input wire:model="email" type="email" placeholder="{{ __('lang.email') }}"
                            class="form-control @error('email') is-invalid @enderror">
                        @error('email')
                            <span style="color: red" class="error">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>{{ __('lang.address') }}</label>
                        <input wire:model="address" type="text" placeholder="{{ __('lang.address') }}"
                            class="form-control @error('address') is-invalid @enderror">
                        @error('address')
                            <span style="color: red" class="error">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>{{ __('lang.password') }}</label>
                        <input wire:model="password" type="password" id="password" placeholder="********"
                            class="form-control">
                        <input type="checkbox" id="eye"
                            onclick="show_password()">&nbsp;<label>{{ __('lang.show') }}</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>{{ __('lang.confirmpassword') }}</label>
                        <input wire:model="confirm_password" type="password" id="confirm_password"
                            placeholder="********" class="form-control">
                    </div>
                    @if (session::has('no_match_password'))
                        <span style="color: red" class="error">{{ session::get('no_match_password') }}</span>
                    @endif
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>{{ __('lang.province') }}</label>
                        <select wire:model="province_id" class="form-control">
                            <option value="" selected>{{ __('lang.select') }}</option>
                            @foreach ($province as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>{{ __('lang.district') }}</label>
                        <select wire:model="district_id" class="form-control">
                            <option value="" selected>{{ __('lang.select') }}</option>
                            @foreach ($district as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                        @error('storetype_id')
                            <span style="color: red" class="error">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>{{ __('lang.village') }}</label>
                        <select wire:model="village_id" class="form-control">
                            <option value="" selected>{{ __('lang.select') }}</option>
                            @foreach ($village as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>{{ __('lang.role') }}</label>
                        <select wire:model="role_id" class="form-control">
                            <option value="" selected>{{ __('lang.select') }}</option>
                            @foreach ($roles as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            </form>
        </div>
        <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-primary" data-dismiss="modal">{{ __('lang.close') }}</button>
            <button wire:click="update" type="button" class="btn btn-success">{{ __('lang.save') }}</button>
        </div>
    </div>
</div>
</div>
