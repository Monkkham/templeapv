<div>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3">
                    <h5><i class="fas fa-user-lock"></i> ແກ້ໄຂສິດທິການເຂົ້າເຖິງ</h5>
                </div>
                <div class="col-sm-9">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">ຫນ້າຫລັກ</a></li>
                        <li class="breadcrumb-item active">ແກ້ໄຂສິດທິການເຂົ້າເຖິງ</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>ຊື່ສິດທິ</label>
                                        <input wire:model="name" type="text" class="form-control @error('name') is-invalid @enderror" placeholder="ປ້ອນຂໍ້ມູນ">
                                        @error('name') <span style="color: red" class="error">{{ $message }}</span> @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row"> <!-- ========================= Start =========================== -->
                                <div class="col-md-2 text-right">
                                    <h6><b><u>ຂໍ້ມູນລາຍຮັບ</u></b></h6>
                                </div>
                                <div class="col-md-8">
                                    <hr>
                                </div>
                                <div class="col-md-2"><hr></div>
                            </div>
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="1">
                                                <label for="name" style="color:black">ຈັດການຂໍ້ມູນລາຍ-ຮັບ</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            <div class="row"> <!-- ========================= Start =========================== -->
                                <div class="col-md-2 text-right">
                                    <h6><b><u>ຂໍ້ມູນລາຍຈ່າຍ</u></b></h6>
                                </div>
                                <div class="col-md-8">
                                    <hr>
                                </div>
                                <div class="col-md-2"><hr></div>
                            </div>
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="row">
                                       
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="2">
                                                <label for="name" style="color:black">ຈັດການຂໍ້ມູນລາຍ-ຈ່າຍ</label>
                                            </div>
                                        </div>
            
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            <div class="row"> <!-- ========================= Start =========================== -->
                                <div class="col-md-2 text-right">
                                    <h6><b><u>ລາຍງານ</u></b></h6>
                                </div>
                                <div class="col-md-8">
                                    <hr>
                                </div>
                                <div class="col-md-2"><hr></div>
                            </div>
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="row">
                                       
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="4">
                                                <label for="name" style="color:black">ລາຍງານ-ລາຍຮັບ</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="5">
                                                <label for="name" style="color:black">ລາຍງານ-ລາຍຈ່າຍ</label>
                                            </div>
                                        </div>
            
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            {{-- <div class="row"> <!-- ========================= Start =========================== -->
                                <div class="col-md-2 text-right">
                                    <h6><b><u>{{__('lang.list_sale')}}</u></b></h6>
                                </div>
                                <div class="col-md-8">
                                    <hr>
                                </div>
                                <div class="col-md-2"><hr></div>
                            </div>
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="43">
                                                <label for="name" style="color:black">{{__('lang.view')}}{{__('lang.list_sale')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                -
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="44">
                                                <label for="name" style="color:black">{{__('lang.pay')}}{{__('lang.list_sale')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                -
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            <div class="row"> <!-- ========================= Start =========================== -->
                                <div class="col-md-2 text-right">
                                    <h6><b><u>{{__('lang.data')}}{{__('lang.product')}}</u></b></h6>
                                </div>
                                <div class="col-md-8">
                                    <hr>
                                </div>
                                <div class="col-md-2"><hr></div>
                            </div>
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="45">
                                                <label for="name" style="color:black">{{__('lang.view')}}{{__('lang.product')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="46">
                                                <label for="name" style="color:black">{{__('lang.add')}}{{__('lang.product')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="47">
                                                <label for="name" style="color:black">{{__('lang.edit')}}{{__('lang.product')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="48">
                                                <label for="name" style="color:black">{{__('lang.delete')}}{{__('lang.product')}}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div> --}}
                            {{-- <div class="row"> <!-- ========================= Start =========================== -->
                                <div class="col-md-2 text-right">
                                    <h6><b><u>{{__('lang.data')}}{{__('lang.customers')}}</u></b></h6>
                                </div>
                                <div class="col-md-8">
                                    <hr>
                                </div>
                                <div class="col-md-2"><hr></div>
                            </div>
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="49">
                                                <label for="name" style="color:black">{{__('lang.view')}}{{__('lang.customers')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="50">
                                                <label for="name" style="color:black">{{__('lang.add')}}{{__('lang.customers')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="51">
                                                <label for="name" style="color:black">{{__('lang.edit')}}{{__('lang.customers')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="52">
                                                <label for="name" style="color:black">{{__('lang.delete')}}{{__('lang.customers')}}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div> --}}
                            {{-- <div class="row"> <!-- ========================= Start =========================== -->
                                <div class="col-md-2 text-right">
                                    <h6><b><u>ລາຍງານ</u></b></h6>
                                </div>
                                <div class="col-md-8">
                                    <hr>
                                </div>
                                <div class="col-md-2"><hr></div>
                            </div> 
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="53">
                                                <label for="name" style="color:black">{{__('lang.view')}}{{__('lang.expend_income')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="54">
                                                <label for="name" style="color:black">{{__('lang.add')}}{{__('lang.expend_income')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="55">
                                                <label for="name" style="color:black">{{__('lang.edit')}}{{__('lang.expend_income')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="56">
                                                <label for="name" style="color:black">{{__('lang.delete')}}{{__('lang.expend_income')}}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div> --}}
                            {{-- <div class="row"> <!-- ========================= Start =========================== -->
                                <div class="col-md-2 text-right">
                                    <h6><b>
                                        <input wire:model="settings" type="checkbox" value="true">
                                        <u>{{__('lang.settings')}}</u>
                                    </b></h6>
                                </div>
                                <div class="col-md-8">
                                    <hr>
                                </div>
                                <div class="col-md-2"><hr></div>
                            </div>
                            @if($settings == 'true')
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="21">
                                                <label for="name" style="color:black">{{__('lang.view')}}{{__('lang.branchs')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="22">
                                                <label for="name" style="color:black">{{__('lang.add')}}{{__('lang.branchs')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="23">
                                                <label for="name" style="color:black">{{__('lang.edit')}}{{__('lang.branchs')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="24">
                                                <label for="name" style="color:black">{{__('lang.delete')}}{{__('lang.branchs')}}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            <div class="row"> <!-- ========================= Start =========================== -->
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="25">
                                                <label for="name" style="color:black">{{__('lang.view')}}{{__('lang.interest')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                -
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="26">
                                                <label for="name" style="color:black">{{__('lang.edit')}}{{__('lang.interest')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                -
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            <div class="row"> <!-- ========================= Start =========================== -->
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="27">
                                                <label for="name" style="color:black">{{__('lang.view')}}{{__('lang.payment_type')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="28">
                                                <label for="name" style="color:black">{{__('lang.add')}}{{__('lang.payment_type')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="29">
                                                <label for="name" style="color:black">{{__('lang.edit')}}{{__('lang.payment_type')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="30">
                                                <label for="name" style="color:black">{{__('lang.delete')}}{{__('lang.payment_type')}}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            <div class="row"> <!-- ========================= Start =========================== -->
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="31">
                                                <label for="name" style="color:black">{{__('lang.view')}}{{__('lang.category')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="32">
                                                <label for="name" style="color:black">{{__('lang.add')}}{{__('lang.category')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="33">
                                                <label for="name" style="color:black">{{__('lang.edit')}}{{__('lang.category')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="34">
                                                <label for="name" style="color:black">{{__('lang.delete')}}{{__('lang.category')}}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            <div class="row"> <!-- ========================= Start =========================== -->
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="35">
                                                <label for="name" style="color:black">{{__('lang.view')}}{{__('lang.customer_type')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="36">
                                                <label for="name" style="color:black">{{__('lang.add')}}{{__('lang.customer_type')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="37">
                                                <label for="name" style="color:black">{{__('lang.edit')}}{{__('lang.customer_type')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="38">
                                                <label for="name" style="color:black">{{__('lang.delete')}}{{__('lang.customer_type')}}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            <div class="row"> <!-- ========================= Start =========================== -->
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="39">
                                                <label for="name" style="color:black">{{__('lang.view')}}{{__('lang.change_name')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                -
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="40">
                                                <label for="name" style="color:black">{{__('lang.edit')}}{{__('lang.change_name')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                -
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            <div class="row"> <!-- ========================= Start =========================== -->
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="9">
                                                <label for="name" style="color:black">{{__('lang.view')}}{{__('lang.village')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="10">
                                                <label for="name" style="color:black">{{__('lang.add')}}{{__('lang.village')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="11">
                                                <label for="name" style="color:black">{{__('lang.edit')}}{{__('lang.village')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="12">
                                                <label for="name" style="color:black">{{__('lang.delete')}}{{__('lang.village')}}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            <div class="row"> <!-- ========================= Start =========================== -->
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="13">
                                                <label for="name" style="color:black">{{__('lang.view')}}{{__('lang.district')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="14">
                                                <label for="name" style="color:black">{{__('lang.add')}}{{__('lang.district')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="15">
                                                <label for="name" style="color:black">{{__('lang.edit')}}{{__('lang.district')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="16">
                                                <label for="name" style="color:black">{{__('lang.delete')}}{{__('lang.district')}}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            <div class="row"> <!-- ========================= Start =========================== -->
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="17">
                                                <label for="name" style="color:black">{{__('lang.view')}}{{__('lang.province')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="18">
                                                <label for="name" style="color:black">{{__('lang.add')}}{{__('lang.province')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="19">
                                                <label for="name" style="color:black">{{__('lang.edit')}}{{__('lang.province')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="20">
                                                <label for="name" style="color:black">{{__('lang.delete')}}{{__('lang.province')}}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            @endif --}}

                            {{-- <div class="row"> <!-- ========================= Start =========================== -->
                                <div class="col-md-2 text-right">
                                    <h6><b>
                                        <input wire:model="reports" type="checkbox" value="true">
                                        <u>{{__('lang.reports')}}</u>
                                    </b></h6>
                                </div>
                                <div class="col-md-8">
                                    <hr>
                                </div>
                                <div class="col-md-2"><hr></div>
                            </div>
                            @if($reports == 'true')
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="57">
                                                <label for="name" style="color:black">{{__('lang.view')}}{{__('lang.report_all')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                -
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                -
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                -
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            <div class="row"> <!-- ========================= Start =========================== -->
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="58">
                                                <label for="name" style="color:black">{{__('lang.view')}}{{__('lang.report_bangle_loss')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                               -
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                -
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                -
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            <div class="row"> <!-- ========================= Start =========================== -->
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="59">
                                                <label for="name" style="color:black">{{__('lang.view')}}{{__('lang.report_income')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                               -
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                -
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                -
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            <div class="row"> <!-- ========================= Start =========================== -->
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="60">
                                                <label for="name" style="color:black">{{__('lang.view')}}{{__('lang.report_expend')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                               -
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                -
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                -
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            <div class="row"> <!-- ========================= Start =========================== -->
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="61">
                                                <label for="name" style="color:black">{{__('lang.view')}}{{__('lang.report_orverdue')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                               -
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                -
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                -
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            <div class="row"> <!-- ========================= Start =========================== -->
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="62">
                                                <label for="name" style="color:black">{{__('lang.view')}}{{__('lang.report_pay_finish')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                               -
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                -
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                -
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            @endif --}}

                            <div class="row"> <!-- ========================= Start =========================== -->
                                <div class="col-md-2 text-right">

                                    <h6><b>
                                        <input wire:model="users" type="checkbox" value="true">
                                        <u>ຂໍ້ມູນສິດທິ</u>
                                    </b></h6>
                                </div>
                                <div class="col-md-8">
                                    <hr>
                                </div>
                                <div class="col-md-2"><hr></div>
                            </div>
                            @if($users == 'true')
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="3">
                                                <label for="name">ຈັດການຂໍ້ມູນສິດທິ</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                            -
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                            -
                                            </div>
                                        </div>  <div class="col-md-3">
                                            <div class="form-group">
                                            -
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            {{-- <div class="row"> <!-- ========================= Start =========================== -->
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="1">
                                                <label for="name" style="color:black">{{__('lang.view')}}{{__('lang.user')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="2">
                                                <label for="name" style="color:black">{{__('lang.add_data')}}{{__('lang.user')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="3">
                                                <label for="name" style="color:black">{{__('lang.update')}}{{__('lang.user')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="4">
                                                <label for="name" style="color:black">{{__('lang.delete')}}{{__('lang.user')}}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div> --}}
                            @endif
                        </div>
                        <div class="card-footer">
                            <div class="d-flex justify-content-between md-2">
                                <button type="button" wire:click="back" class="btn btn-warning"><i class="fas fa-reply-all"></i> ກັບຄືນ</button>
                                <button type="button" wire:click="update" class="btn btn-success"><i class="fas fa-save"></i> {{__('lang.save')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
