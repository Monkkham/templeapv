
<!-- /.modal-add -->
<div wire:ignore.self class="modal fade" id="modal-add">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fa fa-plus text-success"></i> ເພີ່ມໃຫມ່</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="container">
                        <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>ຊື່ ນາມສະກຸນ</label>
                                <input wire:model="name" type="text" placeholder="ປ້ອນຂໍ້ມູນ"
                                    class="form-control @error('name') is-invalid @enderror">
                                @error('name')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                       
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>ຈຳນວນເງິນ</label>
                                    <input wire:model="money" type="tel" placeholder="ປ້ອນຂໍ້ມູນ"
                                        class="form-control @error('money') is-invalid @enderror">
                                    @error('money')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>ຊື່ບ່ອນເຮັດວຽກ</label>
                                    <input wire:model="company" type="text" placeholder="ປ້ອນຂໍ້ມູນ"
                                        class="form-control @error('company') is-invalid @enderror">
                                    @error('company')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <label>ເລືອກສະກຸນເງິນ</label>
                                <div class="form-group clearfix">
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary1" value="1"
                                            wire:model="currency">
                                        <label for="radioPrimary1">ກີບ
                                        </label>
                                    </div>

                                </div>
                                <div class="form-group clearfix">
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary2" value="2"
                                            wire:model="currency">
                                        <label for="radioPrimary2">ບາດ
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary3" value="3"
                                            wire:model="currency">
                                        <label for="radioPrimary3">ໂດລາ
                                        </label>
                                    </div>
                                </div>
                                @error('currency')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-sm-6">
                                <label>ເລືອກປະເພດເງິນ</label>
                                <div class="form-group clearfix">
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary4" value="1" wire:model="type">
                                        <label for="radioPrimary4">ເງິນສົດ
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary5" value="2"
                                            wire:model="type">
                                        <label for="radioPrimary5">ເງິນໂອນ
                                        </label>
                                    </div>
                                </div>
                                @error('type')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                            </div>

                        </div>
                    </div>
                    </div>
                </form>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-danger" data-dismiss="modal">ຍົກເລີກ</button>
                <button wire:click="store" type="button" class="btn btn-success">ບັນທຶກ</button>
            </div>

        </div>
    </div>
</div>
