<div>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-5">
                    <h5 class="text-success"><i class="fas fa-book-open"></i> ອ່ານລາຍຊື່ເຈົ້າສັດທາໂມທະນາທານ</h5>
                </div>
                <div class="col-sm-9">
                    <ol class="breadcrumb float-sm-right">
                        {{-- <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">ຫນ້າຫລັກ</a></li>
                        <li class="breadcrumb-item active">ບັນທຶກລາຍຮັບ</li> --}}
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">


                <!--List users- table table-bordered table-striped -->

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="col-md-7">
                                            <div class="btn-group">
                                                <button wire:click="showDestroy()" type="button"
                                                    @if (empty($mySelected)) disabled @endif
                                                    class="btn btn-primary btn-sm"><i class="fa fa-check-circle"></i>
                                                    ຢືນຍັນອ່ານເເລ້ວ</button>
                                                <button type="button" class="btn btn-default btn-sm"> <i type="button"
                                                        wire:click="refress"
                                                        class="fas fa-2x fa-sync fa-spin text-success"></i></button>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div>
                                <div>
                                    <table>
                                        <thead class="bg-success">
                                            <tr style="font-size: 10px">
                                                <th>ລຳດັບ</th>
                                                <th>ເລືອກ</th>
                                                {{-- <th><input type="checkbox" wire:model="selectAll"></th>
                                            <th><input type="hidden" wire:model="firstId" value="{{ $read_people_donate[0]->id }}"></th> --}}
                                                <th>ຊຶ່ ນາມສະກຸນ</th>
                                                <th>ທີ່ຢູ່</th>
                                                <th>ປັດໄຈ</th>
                                                {{-- <th>ໂມທະນາ</th> --}}
                                                {{-- <th>ຈຳນວນ</th>
                                            <th>ປະເພດ</th>
                                            <th>ວ.ດ.ປ</th> --}}
                                            </tr>
                                        </thead>
                                        <style>
                                            input.larger {
                                                width: 25px;
                                                height: 25px;
                                            }

                                            /* Ribbon-1 */
                                        </style>
                                        <tbody>
                                            @php
                                                $num = 1;
                                            @endphp

                                            @foreach ($read_people_donate as $item)
                                                <tr style="font-size: 10px">
                                                    <td><b style="font-size: 10px">ໃຫມ່</b> {{ $num++ }}
                                                    </td>
                                                    <td>
                                                        <input class="larger" type="checkbox" wire:model="mySelected"
                                                            value="{{ $item->id }}">
                                                    </td>

                                                    @if (!empty($item->people_donate))
                                                        <td> {{ $item->people_donate->name_lastname }}</td>
                                                    @else
                                                        <td></td>
                                                    @endif
                                                    @if (!empty($item->people_donate->village))
                                                        <td>{{ $item->people_donate->village->name }},
                                                            {{ $item->people_donate->district->name }},
                                                            <br>
                                                            {{ $item->people_donate->province->name }}</td>
                                                    @else
                                                        <td></td>
                                                    @endif
                                                    @if (!empty($item->total_money))
                                                        <td>{{ number_format($item->total_money) }}
                                                            @if ($item->currency == 1)
                                                                <b class="text-success">ກີບ</b>
                                                            @elseif($item->currency == 2)
                                                                <b class="text-warning">ບາດ</b>
                                                            @elseif($item->currency == 3)
                                                                <b class="text-danger">ໂດລາ</b>
                                                            @endif
                                                        </td>
                                                    @else
                                                        <td></td>
                                                    @endif
                                                    <td>{{ $item->donate }}</td>

                                                    {{-- <td>{{ $item->qty }}</td>
                                                <td class="text-center">
                                                    @if ($item->type == 1)
                                                        <p class="text-success">ເງິນສົດ</p>
                                                    @elseif($item->type == 2)
                                                        <p class="text-info">ເງິນໃນບັນຊີ</p>
                                                    @else
                                                <td></td>
                                        @endif
                                        </td>
                                        <td>{{ date('d-m-Y', strtotime($item->created_at)) }}</td> --}}
                                                    </form>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>

                                    {{-- <div class="float-right">
                                    {{ $read_people_donate->links() }}
                                </div> --}}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <!-- /.modal-delete -->
    <div class="modal fade" id="modal-delete">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h4 class="modal-title"><i class="fas fa-book-reader"></i></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    {{-- <input type="hidden" wire:model="checkedCountry"> --}}
                    <h3>ອ່ານເເລ້ວລະບໍ່ຂະນ້ອຍ?</h3>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">ຍັງເທື່ອ</button>
                    <button wire:click="confirm_readed()" type="button" class="btn btn-success">ແມ່ນເເລ້ວ</button>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <script>
        $(".page-item").on('click', function(event) {
            Livewire.emit('resetMySelected');
        })
    </script>









    <script>
        window.addEventListener('show-modal-add', event => {
            $('#modal-add').modal('show');
        })
        window.addEventListener('hide-modal-add', event => {
            $('#modal-add').modal('hide');
        })
        window.addEventListener('show-modal-edit', event => {
            $('#modal-edit').modal('show');
        })
        window.addEventListener('hide-modal-edit', event => {
            $('#modal-edit').modal('hide');
        })
        window.addEventListener('show-modal-delete', event => {
            $('#modal-delete').modal('show');
        })
        window.addEventListener('hide-modal-delete', event => {
            $('#modal-delete').modal('hide');
        })
    </script>
@endpush
