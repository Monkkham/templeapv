<div>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3">
                    <h5 class="text-success"><i class="fas fa-pray"></i> ຮັບເງິນເດືອນ</h5>
                </div>
                {{-- <div class="col-sm-9">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">ຫນ້າຫລັກ</a></li>
                        <li class="breadcrumb-item active">ບັນທຶກລາຍຊື່ເຈົ້າສັດທາ</li>
                    </ol>
                </div> --}}
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!--List users- table table-bordered table-striped -->
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="col-md-6">
                                            {{-- @foreach ($rolepermissions as $items)
                        @if ($items->permissionname->name == 'createUser') --}}
                                            <a wire:click="create" class="btn btn-primary btn-sm"
                                                href="javascript:void(0)"><i class="fa fa-plus"></i>
                                                {{ __('lang.add') }}</a>
                                            {{-- @endif
                        @endforeach --}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="row">
                                    {{-- <div class="col-md-6">
                                        <input wire:model="search" type="text" class="form-control"
                                            placeholder="{{ __('lang.search') }}">
                                    </div> --}}
                                    {{-- <div class="col-md-6">
                                        <select wire:model="search_people" id="select_data3" class="form-control">
                                          <option value="">ເລືອກເຈົ້າສັດທາ</option>
                                          @foreach ($people_donate as $item)
                                          <option value="{{ $item->id }}">{{ $item->name }}</option>
                                          @endforeach
                                        </select>
                                      </div>  --}}
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead class="bg-success">
                                        <tr>
                                            <th>ຈັດການ</th>
                                            <th>ລຳດັບ</th>
                                            <th>ຊຶ່ ນາມສະກຸນ</th>
                                            <th>ບ່ອນເຮັດວຽກ</th>
                                            <th>ວ.ດ.ປ ເລີ່ມວຽກ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $num = 1;
                                        @endphp
                                        @foreach ($salary_father as $item)
                                            <tr>
                                               @if (auth()->user()->phone == '52310548')
                                                <td>
                                                    <div class="btn-group">
                                                        <button wire:click="show_new_donate({{ $item->id }})"
                                                            type="button" class="btn btn-success btn-sm"><i
                                                                class="fas fa-plus-circle"></i> ຮັບເງິນ</button>
                                                        {{-- <button wire:click="show_salary_detail({{ $item->id }})" type="button"
                                                            class="btn btn-info btn-sm"><i
                                                                class="fas fa-eye"></i> ລາຍລະອຽດ</button> --}}
                                                                <button wire:click="Salary_detail({{ $item->id }})" type="button"
                                                                    class="btn btn-info btn-sm"><i
                                                                        class="fas fa-eye"></i> ລາຍລະອຽດ</button>
                                                        <button wire:click="edit({{ $item->id }})" type="button"
                                                            class="btn btn-warning btn-sm"><i
                                                                class="fas fa-pen-alt"></i> ແກ້ໄຂ</button>

                                                    </div>
                                                </td>
                                                @endif
                                                <td>{{ $num++ }}</td>
                                                <td>{{ $item->name }}</td>
                                                <td>{{ $item->company }}</td>
                                                {{-- @if (!empty($item->village->name))
                                                    <td>{{ $item->village->name }}, {{ $item->district->name }},
                                                        {{ $item->province->name }}</td>
                                                @else
                                                    <td></td>
                                                @endif --}}

                                                {{-- <td>{{ $item->donate }}</td>
                                                @if (!empty($item->total_money))
                                                    <td>{{ number_format($item->total_money) }}
                                                        @if ($item->currency == 1)
                                                            <p class="text-success">ກີບ</p>
                                                        @elseif($item->currency == 2)
                                                            <p class="text-warning">ບາດ</p>
                                                        @elseif($item->currency == 3)
                                                            <p class="text-danger">ໂດລາ</p>
                                                        @endif
                                                    </td>
                                                @else
                                                    <td></td>
                                                @endif --}}
                                                {{-- 

                                                <td>{{ $item->qty }}</td>
                                                <td class="text-center">
                                                    @if ($item->type == 1)
                                                        <p class="text-success">ເງິນສົດ</p>
                                                    @elseif($item->type == 2)
                                                        <p class="text-info">ເງິນໃນບັນຊີ</p>
                                                    @else
                                                <td></td>
                                        @endif
                                        </td> --}}
                                                <td>{{ date('d-m-Y', strtotime($item->created_at)) }}</td>
                                                </form>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="float-right">
                                    {{ $salary_father->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('livewire.backend.salary-create')
    @include('livewire.backend.salary-update')
    @include('livewire.backend.salary-get-new')
    @include('livewire.backend.show-paymoney')
    @include('livewire.backend.salary-detail')


</div>
