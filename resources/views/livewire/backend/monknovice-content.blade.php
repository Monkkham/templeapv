<div>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3">
                    <h5 class="text-success"><i class="fas fa-users"></i> ພຣະສົງສຳມະເນນ</h5>
                </div>
                <div class="col-sm-9">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">ຫນ້າຫລັກ</a></li>
                        <li class="breadcrumb-item active">ພຣະສົງສຳມະເນນ</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">


                <!--List users- table table-bordered table-striped -->

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="col-md-6">
                                            @if (auth()->user()->roles == 'admin')
                                            <a wire:click="create" class="btn btn-primary btn-sm"
                                                href="javascript:void(0)"><i class="fas fa-plus-circle"></i>
                                                ສະມາສິກໃຫມ່</a>
                                             @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">

                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                    </div>
                                    <div class="input-group input-group-sm" style="width: 240px;">
                                        <input wire:model="search" type="text" class="form-control"
                                            placeholder="{{ __('lang.search') }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="example1" class="table table-hover">
                                    <thead class="bg-success">
                                        <tr>
                                            <th>ລຳດັບ</th>
                                            <th>ຮູບ</th>
                                            <th>ຊື່</th>
                                            <th>ນາມສະກຸນ</th>
                                            <th>ຕຳແຫນ່ງ</th>
                                            <th>ສະຖານະ</th>
                                            @if (auth()->user()->roles == 'admin')
                                            <th>ຈັດການ</th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $num = 1;
                                        @endphp

                                        @foreach ($monk_novice as $item)
                                            <tr>
                                                {{-- <td>
                                                    <div class="btn-group">
                                                        <button wire:click="edit({{ $item->id }})" type="button"
                                                            class="btn btn-warning btn-sm"><i
                                                                class="fas fa-pencil-alt"></i></button>
                                                        <button wire:click="showDestroy({{ $item->id }})"
                                                            type="button" class="btn btn-danger btn-sm"><i
                                                                class="fas fa-trash"></i></button>
                                                    </div>
                                                </td> --}}

                                                <td>{{ $num++ }}</td>
                                                <td>
                                                    @if (!empty($item->image))
                                                        <a href="{{ asset('public/monk_novice') }}/{{ $item->image }}">
                                                            <img src="{{ asset('public/monk_novice') }}/{{ $item->image }}"
                                                                width="50px;" height="60px;">
                                                        </a>
                                                    @else
                                                        <p class="text-center">ບໍ່ມີຮູບພາບ</p>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($item->gender == 1)
                                                        <b class="text-bold">ພຣະອາຈານ</b>
                                                    @elseif($item->gender == 2)
                                                        <b class="text-bold">ພຣະ</b>
                                                    @elseif($item->gender == 3)
                                                    <b class="text-bold">ສຳມະເນນ</b>
                                                    @else
                                                    @endif
                                                    {{ $item->name }}
                                                </td>
                                                <td>{{ $item->lastname }}</td>
                                                <td class="text-center">
                                                    @if ($item->position == 1)
                                                        <p class="text-danger text-bold">ເຈົ້າອາວາດ</p>
                                                    @elseif($item->position == 2)
                                                        <p class="text-success text-bold">ຮອງເຈົ້າອາວາດ1</p>
                                                    @elseif($item->position == 3)
                                                        <p class="text-info text-bold">ຮອງເຈົ້າອາວາດ 2</p>
                                                    @elseif($item->position == 4)
                                                        <p class="text-dark">ລູກວັດ</p>
                                                    @else
                                                @endif
                                                </td>
                                        <td class="text-center">
                                            @if ($item->status_now == 1)
                                                <p class="text-success text-bold"><span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span> ອາໄສຢູ່</p>
                                            @else
                                    
                                        @endif
                                        </td>
                                        <td>
                                            @if (auth()->user()->roles == 'admin')
                                            <div class="btn-group">
                                                <button wire:click="edit({{ $item->id }})" type="button"
                                                    class="btn btn-warning btn-sm"><i
                                                        class="fas fa-pencil-alt"></i></button>
                                                {{-- <button wire:click="showDestroy({{ $item->id }})" type="button"
                                                    class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button> --}}
                                            </div>
                                            @endif
                                        </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                <div class="float-right">
                                    {{ $monk_novice->links() }}
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('livewire.backend.create_monk_novice')
    @include('livewire.backend.update_monk_novice')
</div>
