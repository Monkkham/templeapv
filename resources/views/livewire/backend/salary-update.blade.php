
<!-- /.modal-edit -->
<div wire:ignore.self class="modal fade" id="modal-edit">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fas fa-edit text-success"></i> ແກ້ໄຂ</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="container">
                        <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>ຊື່ ນາມສະກຸນ</label>
                                <input wire:model="name" type="text" placeholder="ປ້ອນຂໍ້ມູນ"
                                    class="form-control @error('name') is-invalid @enderror">
                                @error('name')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>ຊື່ບ່ອນເຮັດວຽກ</label>
                                    <input wire:model="company" type="text" placeholder="ປ້ອນຂໍ້ມູນ"
                                        class="form-control @error('company') is-invalid @enderror">
                                    @error('company')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>ວ.ດ.ປ ເລີ່ມວຽກ</label>
                                    <input wire:model="created_at" type="date" placeholder="ປ້ອນຂໍ້ມູນ"
                                        class="form-control @error('created_at') is-invalid @enderror">
                                    @error('created_at')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
    
                    </div>
                    </div>
                </form>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-danger" data-dismiss="modal">ຍົກເລີກ</button>
                <button wire:click="update" type="button" class="btn btn-success">ບັນທຶກ</button>
            </div>

        </div>
    </div>
</div>
