 <!-- /.modal-delete -->
 <div wire:ignore.self class="modal fade" id="modal-delete">
     <div class="modal-dialog modal-lg">
         <div class="modal-content">
             <div class="modal-header bg-success">
                 <h5 class="modal-title"><i class="fas fa-pray"></i> {{ $name_lastname }}</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
             <div class="modal-body">
                 <div class="col-md-13">
                     <div class="col-md-13">
                         <table class="table table-bordered table-hover">
                             <thead>
                                 <tr class="text-center bg-success text-bold">
                                     <th>ລຳດັບ</th>
                                     {{-- <th>ເຈົ້າສັດທາ</th> --}}
                                     {{-- <th>ທີ່ຢູ່</th> --}}
                                     <th>ວ.ດ.ປ ໂມທະນາ</th>
                                     <th>ສະກຸນເງິນ</th>
                                     <th>ປະເພດເງິນ</th>

                                 </tr>
                             </thead>
                             <tbody>

                                 @php $no = 1 @endphp
                                 @foreach ($this->list_detail as $item)
                                     <tr class="text-center">
                                         <td>{{ $no++ }}</td>
                                         </td>
                                         <td>{{ date('d/m/Y H:i:s', strtotime($item->created_at)) }}</td>
                                         <td>{{ number_format($item->total_money) }}
                                             @if ($item->currency == 1)
                                                 <b class="text-success">ກີບ</b>
                                             @elseif($item->currency == 2)
                                                 <b class="text-warning">ບາດ</b>
                                             @elseif($item->currency == 3)
                                                 <b class="text-danger">ໂດລາ</b>
                                             @else
                                             @endif
                                         </td>
                                         <td class="text-center">
                                             @if ($item->type == 1)
                                                 <p class="text-success">ເງິນສົດ</p>
                                             @elseif($item->type == 2)
                                                 <p class="text-info">ເງິນໃນບັນຊີ</p>
                                             @else
                                         <td></td>
                                        @endif
                                        </td>
                                        </tr>
                                        @endforeach
                                 <tr class="text-center">
                                     <td class="bg-light text-bold text-right" colspan="2">
                                         <i>ລວມຍອດໂມທະນາເງິນກີບ</i>
                                     </td>
                                     <td class="text-bold">
                                         @if (!empty($sum_lak))
                                             {{ number_format($sum_lak) }}
                                         @else
                                             0
                                         @endif
                                     </td>
                                     <td class="bg-light text-left" colspan="1">LAK</td>

                                 </tr>
                                 <tr class="text-center">
                                     <td class="bg-light text-bold text-right" colspan="2">
                                         <i>ລວມຍອດໂມທະນາເງິນບາດ</i>
                                     </td>
                                     <td class="text-bold">
                                         @if (!empty($sum_thb))
                                             {{ number_format($sum_thb) }}
                                         @else
                                             0
                                         @endif
                                     </td>
                                     <td class="bg-light text-left" colspan="1">THB</td>

                                 </tr>
                                 <tr class="text-center">
                                     <td class="bg-light text-bold text-right" colspan="2">
                                         <i>ລວມຍອດໂມທະນາເງິນໂດລາ</i>
                                     </td>
                                     <td class="text-bold">
                                         @if (!empty($sum_usd))
                                             {{ number_format($sum_usd) }}
                                         @else
                                             0
                                         @endif
                                     </td>
                                     <td class="bg-light text-left" colspan="1">USD</td>
                                 </tr>

                             </tbody>
                         </table>
                     </div>
                     {{-- <hr>
                     <div class="form-group">
                         <label><i>ລວມຍອດປັດໄຈທີ່ເຄີຍໂມທະນາ</i>

                     </div> --}}
                 </div>

             </div>
             {{-- <div class="modal-footer justify-content-between">
                 <button type="button" class="btn btn-danger" data-dismiss="modal">ຍົກເລີກ</button>
                 <button wire:click="new_donate()" type="button" class="btn btn-success">ບັນທຶກ</button>
             </div> --}}
         </div>
     </div>
 </div>


 @push('scripts')
     <script>
         window.addEventListener('show-modal-delete', event => {
             $('#modal-delete').modal('show');
         })
         window.addEventListener('hide-modal-delete', event => {
             $('#modal-delete').modal('hide');
         })
     </script>
 @endpush
