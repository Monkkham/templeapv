<div>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <i class="nav-icon fas fa-chart-line text-success"></i>
                    ລາຍງານ-ເຈົ້າສັດທາໂມທະນາທານ
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">{{ __('lang.home') }}</a></li>
                        <li class="breadcrumb-item active">ລາຍງານ-ເຈົ້າສັດທາໂມທະນາທານ</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="date" wire:model="start" class="form-control">
                                    </div>
                                </div><!-- end div-col -->
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="date" wire:model="end" class="form-control">
                                    </div>
                                </div><!-- end div-col -->
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <button wire:click="sub()" class="btn btn-primary"><i
                                                class="fas fa-file-pdf"></i> ສະເເດງ</button>
                                        <button class="btn btn-info" id="print"><i class="fas fa-print"></i>
                                            ປິ່ຣນ</button>
                                    </div>
                                </div><!-- end div-col -->

                            </div><!-- end div-row -->

                            {{-- @if (!empty($starts || $ends)) --}}
                            <hr>
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="right_content">
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <h5>{{ __('lang.headding1') }}</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <h5>{{ __('lang.headding2') }}</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                ========================***========================
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 text-center">
                                                <img src="{{ asset('logo/logo1.jpg') }}" alt="" height="100"
                                                    width="100">
                                            </div>
                                            <div class="col-md-3"></div>
                                            <div class="col-md-6 text-right">
                                                <h5>ວັນທີ່ພິມ: {{ date('d/m/Y H:i:s') }}</h5>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 text-center">
                                                <h5>ວັດອຳພາວັນ</h5>
                                            </div>
                                            <div class="col-md-3"></div>
                                            <div class="col-md-6">

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <h4><u><b>ສະຫລຸບລາຍງານ-ເຈົ້າສັດທາໂມທະນາທານ</b></u></h4>
                                                <input type="hidden" wire:model="hidenId">

                                                <h4><b>ວັນທີ່:
                                                        @if (!empty($starts))
                                                            {{ date('d-m-Y', strtotime($starts)) }}
                                                        @endif
                                                        ຫາ ວັນທີ່:
                                                        @if (!empty($ends))
                                                            {{ date('d-m-Y', strtotime($ends)) }}
                                                    </b></h4>
                                                @endif
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-hover">
                                                        <thead>
                                                            <tr class="text-center bg-success text-bold">
                                                                <th>ລຳດັບ</th>
                                                                <th>ເຈົ້າສັດທາ</th>
                                                                <th>ທີ່ຢູ່</th>
                                                                <th>ກີບ</th>
                                                                <th>ບາດ</th>
                                                                <th>ໂດລາ</th>
                                                                <th>ລາຍລະອຽດ</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                            @php $no = 1 @endphp
                                                            @foreach ($report_people_donate as $item)
                                                                <tr class="text-center">
                                                                    <td>{{ $no++ }}</td>
                                                                    <td>{{ $item->name_lastname }}</td>
                                                                    @if (!empty($item->village))
                                                                    <td>{{ $item->village->name }},
                                                                        {{ $item->district->name }},
                                                                        {{ $item->province->name }}</td>
                                                                @else
                                                                    <td></td>
                                                                @endif
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>{{ $sum3 }}</td>
                                                                    <td></td>
                                                                    <td><button type="button" wire:click="show_donate_detail({{ $item->id }})" class="btn btn-primary sm"><i class="fas fa-eye"></i></button></td>
                                                                    {{-- <td>{{ number_format($item->people_donate_detail->total_money) }}
                                                                        @if ($item->currency == 1)
                                                                            <b class="text-success">ກີບ</b>
                                                                        @endif
                                                                    </td>
                                                                    <td>{{ number_format($item->people_donate_detail->total_money) }}
                                                                        @if ($item->currency == 2)
                                                                            <b class="text-success">ບາດ</b>
                                                                        @endif
                                                                    </td>
                                                                    <td>{{ number_format($item->people_donate_detail->total_money) }}
                                                                        @if ($item->currency == 3)
                                                                            <b class="text-success">ໂດລາ</b>
                                                                        @endif
                                                                    </td> --}}
                                                            </tr>
                                                            @endforeach
                                                            {{-- <tr class="text-center">
                                                                <td class="bg-light text-bold text-right"
                                                                    colspan="3">
                                                                    <i>ລວມເງິນກີບ</i>
                                                                </td>
                                                                <td class="text-bold">
                                                                    @if (!empty($sum1))
                                                                        {{ number_format($sum1) }}
                                                                    @else
                                                                        0
                                                                    @endif
                                                                </td>
                                                                <td class="bg-light text-left" colspan="2">LAK</td>

                                                            </tr>
                                                            <tr class="text-center">
                                                                <td class="bg-light text-bold text-right"
                                                                    colspan="3">
                                                                    <i>ລວມເງິນບາດ</i>
                                                                </td>
                                                                <td class="text-bold">
                                                                    @if (!empty($sum2))
                                                                        {{ number_format($sum2) }}
                                                                    @else
                                                                        0
                                                                    @endif
                                                                </td>
                                                                <td class="bg-light text-left" colspan="2">THB</td>

                                                            </tr>
                                                            <tr class="text-center">
                                                                <td class="bg-light text-bold text-right"
                                                                    colspan="3">
                                                                    <i>ລວມເງິນໂດລາ</i>
                                                                </td>
                                                                <td class="text-bold">
                                                                    @if (!empty($sum3))
                                                                        {{ number_format($sum3) }}
                                                                    @else
                                                                        0
                                                                    @endif
                                                                </td>
                                                                <td class="bg-light text-left" colspan="2">USD</td>
                                                            </tr> --}}
                                                           <tr>
                                                            <td colspan="5">
                                                                @if(!empty(Auth::user()->name))
                                                                ຜູ້ພິມລາຍງານ: <i>{{Auth::user()->name}} {{Auth::user()->lastname}}</i>
                                                            @endif
                                                            </td>
                                                           </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            {{-- @endif --}}

                        </div><!-- end card body-->
                    </div><!-- end card -->
                </div>
            </div>
        </div>
    </section>
                {{-- ========================== report detail =========================== --}}
<div class="modal fade" id="modal-detail">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h4 class="modal-title"><i class="fa fa-trash"></i> {{ __('lang.delete') }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h3 class="text-center">{{ __('lang.do you want to delete') }}</h3>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-primary" data-dismiss="modal">{{ __('lang.close') }}</button>
                {{-- <button wire:click="destroy({{ $ID }})" type="button"
                    class="btn btn-danger">{{ __('lang.ok') }}</button> --}}
            </div>
        </div>
    </div>
</div>
{{-- ========================== script =========================== --}}
@include('livewire.backend.show-report-people-detail')

</div>

@push('scripts')
<script>
         //Add
         window.addEventListener('show-detail', event => {
            $('#modal-detail').modal('show');
        })
        window.addEventListener('hide-detail', event => {
            $('#modal-detail').modal('hide');
        })
</script>
    <script>
        $(document).ready(function() {

            $('#print').click(function() {
                printDiv();

                function printDiv() {
                    var printContents = $(".right_content").html();
                    var originalContents = document.body.innerHTML;
                    document.body.innerHTML = printContents;
                    window.print();
                    document.body.innerHTML = originalContents;
                }
                location.reload();
            });
        });
    </script>
@endpush
