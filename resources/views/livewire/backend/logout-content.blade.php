<div>
    {{-- =========================================== modal delete ============================================ --}}
    <div class="modal fade" id="modal-logout">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title"><i class="fa fa-trash"></i> {{ __('lang.delete') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" wire:model="ID">

                    {{-- <h3 class="text-center">{{ '( ' . $name . ' )' . __('lang.do you want to delete') }}</h3> --}}
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">{{ __('lang.cancle') }}</button>
                    {{-- <button wire:click="destroy({{ $ID }})" type="button" --}}
                        {{-- class="btn btn-success">{{ __('lang.ok') }}</button> --}}
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
    <script>
        //Add
        window.addEventListener('show-logout', event => {
        $('#modal-logout').modal('show');
      })
      window.addEventListener('hide-logout', event => {
        $('#modal-logout').modal('hide');
      })
      </script>
</div>
