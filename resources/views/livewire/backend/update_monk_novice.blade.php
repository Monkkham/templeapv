    <!-- /.modal-edit -->
    <div wire:ignore.self class="modal fade" id="modal-edit">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><i class="fa fa-plus text-success"></i> ແກ້ໄຂຂໍ້ມູນພຣະສົງ</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form>
                        <div class="container">
                            <div class="avatar-upload">
                                <div class="avatar-edit">
                                    <input type='file' wire:model="image" id="imageUpload2"
                                        accept=".png, .jpg, .jpeg" />
                                    <label for="imageUpload2"></label>
                                </div>
                                @if ($image)
                                    <div id="imagePreview2" class="avatar-preview">
                                        <img src="{{ $image->temporaryUrl() }}" alt="" width="120px;">
                                    </div>
                                @else
                                    @if ($newimage)
                                        <div id="imagePreview2" class="avatar-preview">
                                            <img src="{{ asset('public/monk_novice') }}/{{ $newimage }}"
                                                alt="" width="120px;">
                                        </div>
                                    @else
                                        <div class="avatar-preview">
                                            <div id="imagePreview2"
                                                style="background-image: url({{ asset('logo/logo1.jpg') }});">
                                            </div>
                                        </div>
                                    @endif
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <label>ເລືອກ ພຣະ/ສຳມະເນນ</label>
                                <div class="form-group clearfix">
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary1" value="1"
                                            wire:model="gender" checked>
                                        <label for="radioPrimary1">ພຣະອາຈານ(ພຣະເຖຣະ)
                                        </label>
                                    </div>
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary2" value="2"
                                            wire:model="gender" checked>
                                        <label for="radioPrimary2">ພຣະ(ຄູບາ)
                                        </label>
                                    </div>
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary3" value="3"
                                            wire:model="gender">
                                        <label for="radioPrimary3">ສຳມະເນນ(ຈົວ)
                                        </label>
                                    </div>
                                </div>
                                @error('gender')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>ຊື່</label>
                                    <input wire:model="name" type="text" placeholder="ປ້ອນຂໍ້ມູນ"
                                        class="form-control @error('name') is-invalid @enderror">
                                    @error('name')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>ນາມສະກຸນ</label>
                                    <input wire:model="lastname" type="text" placeholder="ປ້ອນຂໍ້ມູນ"
                                        class="form-control @error('lastname') is-invalid @enderror" autofocus>
                                    @error('lastname')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>ຕຳແຫນ່ງ</label>
                                    <select wire:model="position" class="form-control @error('position') is-invalid @enderror">
                                        <option value="" selected>ເລືອກຕຳແຫນ່ງ</option>
                                        <option value="1">ເຈົ້າອາວາດ</option>
                                        <option value="2">ຮອງເຈົ້າອາວາດ 1</option>
                                        <option value="3">ຮອງເຈົ້າອາວາດ 2</option>
                                        <option value="4">ລູກວັດ</option>
                                    </select>
                                    @error('position')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>ສະຖານະ</label>
                                    <select wire:model="status_now" class="form-control">
                                        <option value="" selected>ເລືອກສະຖານະ</option>
                                        <option value="2">ລາສິກຂາ</option>
                                        <option value="3">ຍົກຍ້າຍ</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                </div>
                </form>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">ຍົກເລີກ</button>
                    <button wire:click="update" type="button" class="btn btn-success">ບັນທຶກ</button>
                </div>

            </div>
        </div>
    </div>