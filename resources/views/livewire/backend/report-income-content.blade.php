<div>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <i class="nav-icon fas fa-chart-line text-success"></i>
                    ລາຍງານ-ລາຍຮັບ
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">{{ __('lang.home') }}</a></li>
                        <li class="breadcrumb-item active">ລາຍງານ-ລາຍຮັບ</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            @foreach ($rolepermissions as $items)
                                @if ($items->permissionname->name == 'report_income')
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input type="date" wire:model="starts" class="form-control">
                                            </div>
                                        </div><!-- end div-col -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input type="date" wire:model="ends" class="form-control">
                                            </div>
                                        </div><!-- end div-col -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <select wire:model="money_type" id="money_types"
                                                    class="form-control
                                         @error('money_type') is-invalid @enderror">
                                                    <option value="" selected>ເລືອກ-ປະເພດເງິນ</option>
                                                    <option value="1">ເງິນສົດ</option>
                                                    <option value="2">ເງິນໃນບັນຊີ</option>
                                                </select>
                                                @error('money_type')
                                                    <span style="color: red" class="error">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                {{-- <button wire:click="sub()" class="btn btn-primary"><i
                                                class="fas fa-file-pdf"></i> ສະເເດງ</button> --}}
                                                <button class="btn btn-info" id="print"><i class="fas fa-print"></i>
                                                    ປິ່ຣນ</button>
                                                <button onclick="ExportExcel()" class="btn btn-success"><i
                                                        class="fas fa-file-excel"></i>
                                                    Excel</button>
                                            </div>
                                        </div><!-- end div-col -->

                                    </div><!-- end div-row -->
                                @endif
                            @endforeach
                            {{-- @if (!empty($starts || $ends)) --}}
                            <hr>
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="right_content">
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <h5>{{ __('lang.headding1') }}</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <h5>{{ __('lang.headding2') }}</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                ========================***========================
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 text-center">
                                                <img src="{{ asset('logo/logo1.jpg') }}" alt="" height="100"
                                                    width="100">
                                            </div>
                                            <div class="col-md-3"></div>
                                            <div class="col-md-6 text-right">
                                                <h5>ວັນທີ່ພິມ: {{ date('d/m/Y H:i:s') }}</h5>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 text-center">
                                                <h5>ວັດອຳພາວັນ</h5>
                                            </div>
                                            <div class="col-md-3"></div>
                                            <div class="col-md-6">

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <h4><u><b>ສະຫລຸບລາຍງານ-ລາຍຮັບ</b></u></h4>

                                                <h4><b>ວັນທີ່:
                                                        @if (!empty($starts))
                                                            {{ date('d-m-Y', strtotime($starts)) }}
                                                        @endif
                                                        ຫາ ວັນທີ່:
                                                        @if (!empty($ends))
                                                            {{ date('d-m-Y', strtotime($ends)) }}
                                                        @endif
                                                    </b></h4>

                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table id="export_excel" class="table table-bordered table-hover">
                                                        <thead>
                                                            <tr class="text-center bg-success text-bold">
                                                                <th>ລຳດັບ</th>
                                                                <th>ວ.ດ.ປ</th>
                                                                <th>ຮັບຈາກ</th>
                                                                <th>ຈຳນວນເງິນ</th>
                                                                <th>ຄັງເງິນ</th>
                                                                <th>ປະເພດເງິນ</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                            @php $no = 1 @endphp
                                                            @foreach ($report_income as $item)
                                                                <tr class="text-center">
                                                                    <td>{{ $no++ }}</td>
                                                                    <td>{{ date('d-m-Y', strtotime($item->created_at)) }}
                                                                    </td>
                                                                    <td class="text-left">{{ $item->name }}</td>
                                                                    <td>{{ number_format($item->qty_price) }}
                                                                        @if ($item->currency == 1)
                                                                            <b class="text-success">ກີບ</b>
                                                                        @elseif($item->currency == 2)
                                                                            <b class="text-warning">ບາດ</b>
                                                                        @elseif($item->currency == 3)
                                                                            <b class="text-danger">ໂດລາ</b>
                                                                        @else
                                                                        @endif
                                                                    </td>
                                                                    <td>
                                                                        @if ($item->type_donate == 1)
                                                                            <p class="text-danger">ຄັງບໍລິຫານ</p>
                                                                        @elseif($item->type_donate == 2)
                                                                            <p class="text-primary">ຄັງກໍ່ສ້າງ</p>
                                                                        @endif
                                                                    </td>

                                                                    <td class="text-center">
                                                                        @if ($item->type == 1)
                                                                            <p class="text-success">ເງິນສົດ</p>
                                                                        @elseif($item->type == 2)
                                                                            <p class="text-info">ເງິນໃນບັນຊີ</p>
                                                                        @else
                                                                    <td></td>
                                                            @endif
                                                            </td>
                                                            </tr>
                                                            @endforeach
                                                            <tr class="text-center">
                                                                <td class="bg-light text-bold" rowspan="3"
                                                                    colspan="2">
                                                                    <h3 class="text-danger">ຄັງບໍລິຫານ</h3>
                                                                </td>
                                                                <td class="bg-light text-bold text-right">
                                                                    <i>ລວມເງິນກີບ</i>
                                                                </td>
                                                                <td class="text-bold">
                                                                    @if (!empty($sum1))
                                                                        {{ number_format($sum1) }}
                                                                    @else
                                                                        0
                                                                    @endif
                                                                </td>
                                                                <td class="bg-light text-left" colspan="2">LAK</td>

                                                            </tr>
                                                            <tr class="text-center">
                                                                <td class="bg-light text-bold text-right">
                                                                    <i>ລວມເງິນບາດ</i>
                                                                </td>
                                                                <td class="text-bold">
                                                                    @if (!empty($sum2))
                                                                        {{ number_format($sum2) }}
                                                                    @else
                                                                        0
                                                                    @endif
                                                                </td>
                                                                <td class="bg-light text-left" colspan="2">THB</td>

                                                            </tr>
                                                            <tr class="text-center">
                                                                <td class="bg-light text-bold text-right">
                                                                    <i>ລວມເງິນໂດລາ</i>
                                                                </td>
                                                                <td class="text-bold">
                                                                    @if (!empty($sum3))
                                                                        {{ number_format($sum3) }}
                                                                    @else
                                                                        0
                                                                    @endif
                                                                </td>
                                                                <td class="bg-light text-left" colspan="2">USD</td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="6" class="bg-light">
                                                                    {{-- @if (!empty(Auth::user()->name))
                                                                ຜູ້ຮັບຜິດຊອບລາຍຮັບ: <i>{{Auth::user()->name}} {{Auth::user()->lastname}}</i>
                                                            @endif --}}
                                                                </td>
                                                            </tr>
                                                            <tr class="text-center">
                                                                <td class="bg-light text-bold" rowspan="3"
                                                                    colspan="2">
                                                                    <h3 class="text-primary">ຄັງກໍ່ສ້າງ</h3>
                                                                </td>
                                                                <td class="bg-light text-bold text-right">
                                                                    <i>ລວມເງິນກີບ</i>
                                                                </td>
                                                                <td class="text-bold">
                                                                    @if (!empty($sum4))
                                                                        {{ number_format($sum4) }}
                                                                    @else
                                                                        0
                                                                    @endif
                                                                </td>
                                                                <td class="bg-light text-left" colspan="2">LAK</td>

                                                            </tr>
                                                            <tr class="text-center">
                                                                <td class="bg-light text-bold text-right">
                                                                    <i>ລວມເງິນບາດ</i>
                                                                </td>
                                                                <td class="text-bold">
                                                                    @if (!empty($sum5))
                                                                        {{ number_format($sum5) }}
                                                                    @else
                                                                        0
                                                                    @endif
                                                                </td>
                                                                <td class="bg-light text-left" colspan="2">THB</td>

                                                            </tr>
                                                            <tr class="text-center">
                                                                <td class="bg-light text-bold text-right">
                                                                    <i>ລວມເງິນໂດລາ</i>
                                                                </td>
                                                                <td class="text-bold">
                                                                    @if (!empty($sum6))
                                                                        {{ number_format($sum6) }}
                                                                    @else
                                                                        0
                                                                    @endif
                                                                </td>
                                                                <td class="bg-light text-left" colspan="2">USD</td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="6">
                                                                    @if (!empty(Auth::user()->name))
                                                                        ຜູ້ຮັບຜິດຊອບລາຍຮັບ: <i>{{ Auth::user()->name }}
                                                                            {{ Auth::user()->lastname }}</i>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            {{-- @endif --}}

                        </div><!-- end card body-->
                    </div><!-- end card -->
                </div>
            </div>
        </div>

    </section>
</div>
@push('scripts')
    <script>
        function ExportExcel(type, fn, dl) {
            var elt = document.getElementById('export_excel');
            var wb = XLSX.utils.table_to_book(elt, {
                sheet: "Sheet JS"
            });
            return dl ?
                XLSX.write(wb, {
                    bookType: type,
                    bookSST: true,
                    type: 'base64'
                }) :
                XLSX.writeFile(wb, fn || (window.location.pathname + '.' + (type || 'xlsx')));
        }
    </script>
    <script>
        $(document).ready(function() {

            $('#print').click(function() {
                printDiv();

                function printDiv() {
                    var printContents = $(".right_content").html();
                    var originalContents = document.body.innerHTML;
                    document.body.innerHTML = printContents;
                    window.print();
                    document.body.innerHTML = originalContents;
                }
                location.reload();
            });
        });
    </script>
@endpush
