{{-- ========================== edit =========================== --}}
<div wire:ignore.self class="modal fade" id="modal-edit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-gradient-pink">
                <h4 class="modal-title"><i class="fas fa-handshake"></i> ມອບສິດຫນ້າທີ່ໃຫ້ {{ $name }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" wire:model="ID">
                <div class="col-sm-12">
                    <label class="text-center">ເລືອກສິດທິໃຫ້ກັບຜູ້ໃຊ້ລະບົບ</label>
                    <div class="col-md-12">
                        <div class="form-group">
                            <select wire:model="role_id" class="form-control" >
                              <option value="" selected>ເລືອກສິດທິ</option>
                                @foreach($roles as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                            @error('role_id')
                            <span style="color: red" class="error">{{ $message }}</span>
                        @enderror
                        </div>
                  </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-danger" data-dismiss="modal">ຍົກເລີກ</button>
                <button wire:click="update({{ $ID }})" type="button"
                    class="btn btn-success">ຕົກລົງ</button>
            </div>
        </div>
    </div>
</div>
{{-- ========================== script =========================== --}}
@push('scripts')
    <script>
        //Add
        window.addEventListener('show-add', event => {
            $('#modal-add').modal('show');
        })
        window.addEventListener('hide-add', event => {
            $('#modal-add').modal('hide');
        })

        //Edit
        window.addEventListener('show-edit', event => {
            $('#modal-edit').modal('show');
        })
        window.addEventListener('hide-edit', event => {
            $('#modal-edit').modal('hide');
        })

        //Delete
        window.addEventListener('show-delete', event => {
            $('#modal-delete').modal('show');
        })
        window.addEventListener('hide-delete', event => {
            $('#modal-delete').modal('hide');
        })
    </script>
    {{-- ================== show-hide paddword ==================== --}}
    <script>
        //show password
        var state = false;

        function show_password() {
            if (state) {
                document.getElementById("password").setAttribute("type",
                    "password");
                document.getElementById("eye").style.color = '#5887ef';
                state = false;
            } else {
                document.getElementById("password").setAttribute("type",
                    "text");
                document.getElementById("eye").style.color = '#5887ef';
                state = true;
            }
        }
        var edit_state = false;

        function edit_show_password() {
            if (state) {
                document.getElementById("edit_password").setAttribute("type",
                    "password");
                document.getElementById("edit_eye").style.color = '#5887ef';
                state = false;
            } else {
                document.getElementById("edit_password").setAttribute("type",
                    "text");
                document.getElementById("edit_eye").style.color = '#5887ef';
                state = true;
            }
        }
        // ================= avatar image uupload==========================
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
                    $('#imagePreview').hide();
                    $('#imagePreview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imageUpload").change(function() {
            readURL(this);
        });
    </script>
@endpush
