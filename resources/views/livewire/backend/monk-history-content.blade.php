<div>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3">
                    <h5 class="text-danger"><i class="fas fa-clock-o"></i> ປະຫວັດພຣະສົງ</h5>
                </div>
                <div class="col-sm-9">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">ຫນ້າຫລັກ</a></li>
                        <li class="breadcrumb-item active">ປະຫວັດພຣະສົງ</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">


                <!--List users- table table-bordered table-striped -->

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="col-md-6">
                                            {{-- @foreach ($rolepermissions as $items)
                        @if ($items->permissionname->name == 'createUser') --}}
                                            {{-- <a wire:click="create" class="btn btn-primary btn-sm"
                                                href="javascript:void(0)"><i class="fas fa-plus-circle"></i>
                                                ສະມາສິກໃຫມ່</a> --}}
                                            {{-- @endif
                        @endforeach --}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">

                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                    </div>
                                    <div class="input-group input-group-sm" style="width: 240px;">
                                        <input wire:model="search" type="text" class="form-control"
                                            placeholder="{{ __('lang.search') }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="example1" class="table table-hover">
                                    <thead class="bg-danger">
                                        <tr>
                                            <th>ລຳດັບ</th>
                                            <th>ຮູບ</th>
                                            <th>ຊື່</th>
                                            <th>ນາມສະກຸນ</th>
                                            <th>ອາດີດ</th>
                                            <th>ສະຖານະ</th>
                                            <th>ຈັດການ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $num = 1;
                                        @endphp

                                        @foreach ($monk_novice as $item)
                                            <tr>
                                                {{-- <td>
                                                    <div class="btn-group">
                                                        <button wire:click="edit({{ $item->id }})" type="button"
                                                            class="btn btn-warning btn-sm"><i
                                                                class="fas fa-pencil-alt"></i></button>
                                                        <button wire:click="showDestroy({{ $item->id }})"
                                                            type="button" class="btn btn-danger btn-sm"><i
                                                                class="fas fa-trash"></i></button>
                                                    </div>
                                                </td> --}}

                                                <td>{{ $num++ }}</td>
                                                <td>
                                                    @if (!empty($item->image))
                                                        <a href="{{ asset('public/monk_novice') }}/{{ $item->image }}">
                                                            <img src="{{ asset('public/monk_novice') }}/{{ $item->image }}"
                                                                width="50px;" height="60px;">
                                                        </a>
                                                    @else
                                                        <p class="text-center">ບໍ່ມີຮູບພາບ</p>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($item->gender == 1)
                                                        <b class="text-bold">ພຣະອາຈານ</b>
                                                    @elseif($item->gender == 2)
                                                        <b class="text-bold">ພຣະ</b>
                                                    @elseif($item->gender == 3)
                                                    <b class="text-bold">ສຳມະເນນ</b>
                                                    @else
                                                    @endif
                                                    {{ $item->name }}
                                                </td>
                                                <td>{{ $item->lastname }}</td>
                                                <td class="text-center">
                                                    @if ($item->position == 1)
                                                        <p class="text-danger text-bold">ເຈົ້າອາວາດ</p>
                                                    @elseif($item->position == 2)
                                                        <p class="text-success text-bold">ຮອງເຈົ້າອາວາດ1</p>
                                                    @elseif($item->position == 3)
                                                        <p class="text-info text-bold">ຮອງເຈົ້າອາວາດ 2</p>
                                                    @elseif($item->position == 4)
                                                        <p class="text-dark">ລູກວັດ</p>
                                                    @else
                                                @endif
                                                </td>
                                        <td class="text-center">
                                            @if ($item->status_now == 2)
                                                <p class="text-danger text-bold">ລາສິກຂາ</p>
                                            @elseif($item->status_now == 3)
                                            <p class="text-warning text-bold">ຍົກຍ້າຍ</p>
                                        @else
                                        @endif
                                        </td>
                                        <td>
                                            <div class="btn-group">
                                                <button wire:click="edit({{ $item->id }})" type="button"
                                                    class="btn btn-info btn-sm"><i
                                                        class="fas fa-eye"></i></button>
                                                {{-- <button wire:click="showDestroy({{ $item->id }})" type="button"
                                                    class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button> --}}
                                            </div>
                                        </td>
                                        </form>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                <div class="float-right">
                                    {{ $monk_novice->links() }}
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- @include('livewire.backend.create_monk_novice')
    @include('livewire.backend.update_monk_novice') --}}
</div>
