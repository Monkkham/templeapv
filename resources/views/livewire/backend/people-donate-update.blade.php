<!-- /.modal-create -->
<div wire:ignore.self class="modal fade" id="modal-edit">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fa fa-edit"></i> {{ __('lang.edit') }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="container">

                        <div class="col-md-13">
                            <div class="form-group">
                                <label>ຊື່ ນາມສະກຸນ</label>
                                <input wire:model="name" type="text" placeholder="ປ້ອນຂໍ້ມູນ"
                                    class="form-control @error('name') is-invalid @enderror">
                                @error('name')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            
                            {{-- <div class="col-md-4">
                    <div class="form-group">
                        <label>{{ __('lang.village') }}</label>
                        <select wire:model="village_id" class="form-control">
                            <option value="" selected>{{ __('lang.select') }}</option>
                            <option value="famale">{{ __('lang.famale') }}</option>
                            <option value="male">{{ __('lang.male') }}</option>
                        </select>
                    </div>
                </div> --}}
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>ເງິນເດືອນ</label>
                                    <input wire:model="donate" type="text" placeholder="ປ້ອນຂໍ້ມູນ"
                                        class="form-control @error('donate') is-invalid @enderror">
                                    @error('donate')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>ຈຳນວນປັດໃຈ</label>
                                    <input wire:model="total_money" type="tel" placeholder="ປ້ອນຂໍ້ມູນ"
                                        class="form-control @error('total_money') is-invalid @enderror">
                                    @error('total_money')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>ຈຳນວນວັດຖຸ</label>
                                    <input wire:model="qty" type="tel" placeholder="ປ້ອນຂໍ້ມູນ"
                                        class="form-control @error('qty') is-invalid @enderror">
                                    @error('qty')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>ແຂວງ</label>
                                    <select id="select_data1" wire:model="province_id" class="form-control">
                                        <option value="1" selected>ເລືອກ</option>
                                        @foreach ($province as $item)
                                            <option  value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>ເມືອງ</label>
                                    <select id="select_data2" wire:model="district_id" id="select_data2" class="form-control">
                                        <option value="" selected>ເລືອກ</option>
                                        @foreach ($districts as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>ບ້ານ</label>
                                    <select id="select_data3" wire:model="village_id" class="form-control">
                                        <option value="" selected>ເລືອກ</option>
                                        @foreach ($villages as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <label>ເລືອກສະກຸນເງິນ</label>
                                <div class="form-group clearfix">
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary1" value="1"
                                            wire:model="currency">
                                        <label for="radioPrimary1">ກີບ
                                        </label>
                                    </div>

                                </div>
                                <div class="form-group clearfix">
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary2" value="2"
                                            wire:model="currency">
                                        <label for="radioPrimary2">ບາດ
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary3" value="3"
                                            wire:model="currency">
                                        <label for="radioPrimary3">ໂດລາ
                                        </label>
                                    </div>
                                </div>
                                @error('currency')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-sm-6">
                                <label>ເລືອກປະເພດເງິນ</label>
                                <div class="form-group clearfix">
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary4" value="1" wire:model="type">
                                        <label for="radioPrimary4">ເງິນສົດ
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary5" value="2"
                                            wire:model="type">
                                        <label for="radioPrimary5">ເງິນໃນບັນຊີ
                                        </label>
                                    </div>
                                </div>
                                @error('type')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                            </div>

                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-danger" data-dismiss="modal">ຍົກເລີກ</button>
                <button wire:click="update" type="button" class="btn btn-success">ບັນທຶກ</button>
            </div>
        </div>
    </div>
</div>