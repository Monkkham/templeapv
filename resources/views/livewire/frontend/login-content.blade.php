<main class="main">
    <nav aria-label="breadcrumb" class="breadcrumb-nav border-0 mb-0">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Pages</a></li>
                <li class="breadcrumb-item active" aria-current="page">Login</li>
            </ol>
        </div><!-- End .container -->
    </nav><!-- End .breadcrumb-nav -->

    <div class="login-page bg-image pt-8 pb-8 pt-md-12 pb-md-12 pt-lg-17 pb-lg-17"
        style="background-image: url('assets/images/backgrounds/login-bg.jpg')">
        <div class="container">
            <div class="form-box">
                <div class="form-tab">
                    <ul class="nav nav-pills nav-fill" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link" id="signin-tab-2" data-toggle="tab" href="#signin-2" role="tab"
                                aria-controls="signin-2" aria-selected="false"><i class="fa fa-user-lock"></i> {{ __('lang.login') }}</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="register-2" role="tabpanel"
                            aria-labelledby="register-tab-2">
                            <form action="#">

                                <label for="register-password-2">{{ __('lang.phone') }}</label>
                                <input wire:model="phone" placeholder="{{ __('lang.input') }}{{ __('lang.phone') }}"
                                    type="tel" class="form-control money @error('phone') is-invalid @enderror">
                                @error('phone')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                                <label for="register-password-2">{{ __('lang.password') }}</label>
                                <input wire:model="password" placeholder="**********"
                                    type="tel" class="form-control money @error('password') is-invalid @enderror">
                                @error('password')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror

                                <div class="form-footer">
                                    <button wire:click="register" type="button" class="btn btn-outline-primary-2">
                                        <span>{{ __('lang.register') }}</span>
                                        <i class="icon-long-arrow-right"></i>
                                    </button>

                                    <div class="">
                                        <label for="register-policy-2">{{ __('lang.no_account') }}<a class="text-primary"
                                                href="{{ route('frontend.register') }}"> {{ __('lang.register') }}{{ __('lang.new') }}</a>
                                            *</label>
                                    </div><!-- End .custom-checkbox -->
                                </div><!-- End .form-footer -->
                            </form>
                            <div class="form-choice">
                                <p class="text-center">or sign in with</p>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <a href="#" class="btn btn-login btn-g">
                                            <i class="icon-google"></i>
                                            Login With Google
                                        </a>
                                    </div><!-- End .col-6 -->
                                    <div class="col-sm-6">
                                        <a href="#" class="btn btn-login  btn-f">
                                            <i class="icon-facebook-f"></i>
                                            Login With Facebook
                                        </a>
                                    </div><!-- End .col-6 -->
                                </div><!-- End .row -->
                            </div><!-- End .form-choice -->
                        </div><!-- .End .tab-pane -->
                    </div><!-- End .tab-content -->
                </div><!-- End .form-tab -->
            </div><!-- End .form-box -->
        </div><!-- End .container -->
    </div><!-- End .login-page section-bg -->
</main><!-- End .main -->
