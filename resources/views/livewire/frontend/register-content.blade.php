<main class="main">
    <nav aria-label="breadcrumb" class="breadcrumb-nav border-0 mb-0">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Pages</a></li>
                <li class="breadcrumb-item active" aria-current="page">Login</li>
            </ol>
        </div><!-- End .container -->
    </nav><!-- End .breadcrumb-nav -->

    <div class="login-page bg-image pt-8 pb-8 pt-md-12 pb-md-12 pt-lg-17 pb-lg-17"
        style="background-image: url('assets/images/backgrounds/login-bg.jpg')">
        <div class="container">
            <div class="form-box">
                <div class="form-tab">
                    <ul class="nav nav-pills nav-fill" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link" id="signin-tab-2" data-toggle="tab" href="#signin-2" role="tab"
                                aria-controls="signin-2" aria-selected="false"><i class="fa fa-user-plus"></i> {{ __('lang.register') }}{{ __('lang.new') }}</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="register-2" role="tabpanel"
                            aria-labelledby="register-tab-2">
                            <form action="#">
                               <div class="row">
                                <div class="col-sm-6">
                                    <label for="register-password-2">{{ __('lang.name') }} {{ __('lang.users') }}</label>
                                    <input wire:model="name" placeholder="{{ __('lang.input') }}{{ __('lang.name') }}" type="text"
                                        class="form-control money @error('name') is-invalid @enderror">
                                    @error('name')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="col-sm-6">
                                    <label for="register-password-2">{{ __('lang.lastname') }}</label>
                                    <input wire:model="lastname" placeholder="{{ __('lang.input') }}{{ __('lang.lastname') }}" type="text"
                                        class="form-control money @error('lastname') is-invalid @enderror">
                                    @error('lastname')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                               </div>
                                <label for="register-password-2">{{ __('lang.phone') }}</label>
                                <input wire:model="phone" placeholder="{{ __('lang.input') }}{{ __('lang.phone') }}" type="tel"
                                    class="form-control money @error('phone') is-invalid @enderror">
                                @error('phone')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="register-password-2">{{ __('lang.password') }}</label>
                                        <input wire:model="password" placeholder="**********" type="password"
                                            class="form-control money @error('password') is-invalid @enderror">
                                        @error('password')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="register-password-2">{{ __('lang.confirm_password') }}</label>
                                        <input wire:model="confirm_password" placeholder="**********" type="password"
                                            class="form-control money @error('confirm_password') is-invalid @enderror">
                                        @error('confirm_password')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                   </div>
                                <div class="form-footer">
                                    <button wire:click="register" type="button" class="btn btn-outline-primary-2">
                                        <span>{{ __('lang.register') }}</span>
                                        <i class="icon-long-arrow-right"></i>
                                    </button>

                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="register-policy-2"
                                            required>
                                        <label class="custom-control-label" for="register-policy-2">I agree to the <a
                                                href="#">privacy policy</a> *</label>
                                    </div><!-- End .custom-checkbox -->
                                </div><!-- End .form-footer -->
                            </form>
                            <div class="form-choice">
                                <p class="text-center">or sign in with</p>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <a href="#" class="btn btn-login btn-g">
                                            <i class="icon-google"></i>
                                            Login With Google
                                        </a>
                                    </div><!-- End .col-6 -->
                                    <div class="col-sm-6">
                                        <a href="#" class="btn btn-login  btn-f">
                                            <i class="icon-facebook-f"></i>
                                            Login With Facebook
                                        </a>
                                    </div><!-- End .col-6 -->
                                </div><!-- End .row -->
                            </div><!-- End .form-choice -->
                        </div><!-- .End .tab-pane -->
                    </div><!-- End .tab-content -->
                </div><!-- End .form-tab -->
            </div><!-- End .form-box -->
        </div><!-- End .container -->
    </div><!-- End .login-page section-bg -->
</main><!-- End .main -->
