<?php
use App\Http\Livewire\Logout;
use App\Http\Controllers\Login;
use Illuminate\Support\Facades\Route;
use App\Http\Livewire\Backend\LoginContent;
Route::get('localization/{local}', function ($local) {
    Session::put('local', $local);
    return back();
});
// ======================= for frontend =================== //
// Route::get('/register', App\Http\Livewire\Frontend\RegisterContent::class)->name('frontend.register');
// Route::get('/login', App\Http\Livewire\Frontend\LoginContent::class)->name('frontend.login');
// Route::get('/logout', [App\Http\Livewire\Frontend\LoginContent::class, 'logout'])->name('frontend.logout');
// Route::get('/', App\Http\Livewire\Frontend\HomeContent::class)->name('home');

// ======================= for backend =================== //
// Route::resource('/', Login::class);
// Route::resource('/logi', Login::class);

Route::get('/', LoginContent::class)->name('backend.login');
Route::get('/register', App\Http\Livewire\Backend\RegisterContent::class)->name('register');
Route::group(['middleware'=> 'auth'], function(){
Route::get('/logout', [App\Http\Livewire\Backend\LogoutContent::class,'logout'])->name('logout');
Route::get('/dashboards', App\Http\Livewire\Backend\DashboardContent::class)->name('dashboard');
Route::get('/income', App\Http\Livewire\Backend\IncomeContent::class)->name('backend.income');
Route::get('/expend', App\Http\Livewire\Backend\ExpendContent::class)->name('backend.expend');

//
Route::get('/report-income', App\Http\Livewire\Backend\ReportIncomeContent::class)->name('backend.report-income');
Route::get('/report-expend', App\Http\Livewire\Backend\ReportExpendContent::class)->name('backend.report-expend');
Route::get('/report-people-donate', App\Http\Livewire\Backend\ReportPeopleDonateContent::class)->name('backend.report_people_donate');
// Route::get('/products', App\Http\Livewire\Backend\ProductContent::class)->name('backend.products');
// Route::get('/provinces', App\Http\Livewire\Backend\ProvinceContent::class)->name('backend.provinces');
// Route::get('/lands', App\Http\Livewire\Backend\LandsContent::class)->name('backend.lands');
// Route::get('productss', App\Http\Livewire\Backend\ProductComponent::class)->name('allProducts');
// Route::get('products/add', App\Http\Livewire\Backend\AddProductComponent::class)->name('addProducts');
// Route::get('products/edit/{id}', App\Http\Livewire\Backend\EditProductComponent::class)->name('editProducts');
Route::get('/people_donate', App\Http\Livewire\Backend\PeopleDonateContent::class)->name('backend.people_donate');
Route::get('/read_people_donate', App\Http\Livewire\Backend\ReadPeopleDonateContent::class)->name('backend.read_people_donate');
Route::get('/general', App\Http\Livewire\Backend\GeneralContent::class)->name('backend.general');
Route::get('/monk_novices', App\Http\Livewire\Backend\MonknoviceContent::class)->name('backend.monk_novice');
Route::get('/monkhistory', App\Http\Livewire\Backend\MonkHistoryContent::class)->name('backend.monkhistory');
Route::get('/user', App\Http\Livewire\Backend\UsersContent::class)->name('backend.users');
Route::get('/users/profile/{id}', App\Http\Livewire\Backend\ProfileContent::class)->name('backend.profile');
//Roles 
Route::get('/roles', App\Http\Livewire\Backend\RoleContent::class)->name('backend.role');
Route::get('/create-roles', App\Http\Livewire\Backend\RoleCreateContent::class)->name('backend.create_role');
Route::get('/edit-role/{id}', App\Http\Livewire\Backend\RoleEditContent::class)->name('backend.edit_role');
// salary
Route::get('/salarys', App\Http\Livewire\Backend\SalaryFatherContent::class)->name('backend.salary');
Route::get('/salary-detail/{slug_id}', App\Http\Livewire\Backend\SalaryDetailContent::class)->name('backend.salary-detail');

});



