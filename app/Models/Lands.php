<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lands extends Model
{
    use HasFactory;
    protected $table = "lands";
    protected $fillable = 
    ['id','code','name','image','description_lo',
    'description_en','status_sale','status_of_on','created_at','updated_at'];
}
