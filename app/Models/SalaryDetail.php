<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SalaryDetail extends Model
{
    use HasFactory;
    protected $table = 'salary_detail';
    protected $fillable = [
        'id',
        'salary_father_id',
        'money',
        'currency',
        'type',
        'created_at',
        'updated_at'
    ];
    public function salary_detail()
    {
        return $this->belongsTo('App\Models\Salary_father','salary_father_id','id');
    }
}
