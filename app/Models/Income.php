<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Income extends Model
{
    use HasFactory;
    protected $table = 'income';
    protected $fillable = ['id','name','currency','type_donate','qty_price','type','creator_id','created_at','updated_at'];

    public function creator()
    {
        return $this->belongsTo('App\Models\User','creator_id','id');
    }
}
