<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class People_donate extends Model
{
    use HasFactory;
    protected $table = 'people_donate';
    protected $fillable = [
        'id',
        'village_id',
        'district_id',
        'province_id',
        'name_lastname',
        'created_at',
        'updated_at'
    ];
    public function village()
    {
        return $this->belongsTo('App\Models\Villages','village_id','id');
    }
    public function district()
    {
        return $this->belongsTo('App\Models\Districts','district_id','id');
    }
    public function province()
    {
        return $this->belongsTo('App\Models\Provinces','province_id','id');
    }
    public function people_donate_detail()
    {
        return $this->belongsTo('App\Models\People_donate_detail','id','people_donate_id');
    }
}
