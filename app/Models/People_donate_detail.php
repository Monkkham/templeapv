<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class People_donate_detail extends Model
{
    use HasFactory;
    protected $table = 'people_donate_detail';
    protected $fillable = [
        'id',
        'people_donate_id',
        'donate',
        'qty',
        'total_money',
        'currency',
        'type',
        'status',
        'created_at',
        'updated_at'
    ];
    public function people_donate()
    {
        return $this->belongsTo('App\Models\People_donate','people_donate_id','id');
    }
}
