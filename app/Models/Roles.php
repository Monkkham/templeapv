<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    use HasFactory;
    protected $table = "roles";
    protected $fillable = [
        'id',
        'name',
        'settings',
        'reports',
        'users',
        'del',
        'created_at',
        'updated_at'
    ];

    // public function district()
    // {
    //     return $this->belongsTo('App\Models\Districts','district_id','id');
    // }
}
