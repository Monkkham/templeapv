<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Monk_novice extends Model
{
    use HasFactory;
    protected $table = 'monk_novice';
    protected $fillable = 
    ['id',
    'image',
    'name',
    'lastname',
    'position',
    'gender',
    'status_now',
    'created_at',
    'updated_at'];
}
