<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lands_sub_image extends Model
{
    use HasFactory;
    protected $table = "lands_sub_image";
    protected $fillable = ['id','lands_id','image','created_at','updated_at'];

    public function lands()
    {
        return $this->belongsTo('App\Models\Lands','lands_id','id');
    }
}
