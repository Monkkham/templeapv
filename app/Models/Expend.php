<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Expend extends Model
{
    use HasFactory;
    protected $table = 'expend';
    protected $fillable = ['id','image','name','qty_price','currency','type_donate','type','del','creator_id','created_at','updated_at'];
    public function creator()
    {
        return $this->belongsTo('App\Models\User','creator_id','id');
    }
}
