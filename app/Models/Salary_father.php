<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Salary_father extends Model
{
    use HasFactory;
    protected $table = 'salary_father';
    protected $fillable = [
        'id',
        'name',
        'created_at',
        'updated_at'
    ];
}
