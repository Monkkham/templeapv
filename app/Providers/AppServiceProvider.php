<?php

namespace App\Providers;

use View;
use App\Models\Roles;
use App\Models\RolePermission;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function refress()
    {
      return;
    }

     public function boot()
    {
        View::composer('*', function($view)
        {
            if (Auth::check()){
                // $count_noti_sell = Sell::select('id','product_id','cus_id')->where('branch_id', auth()->user()->branch_id)->where('status',3)->count('id');
                // $noti_sell = Sell::select('id','product_id','cus_id')->where('branch_id', auth()->user()->branch_id)->where('status',3)->paginate(6);
                $roles = Roles::find(auth()->user()->role_id);
                $rolepermissions = RolePermission::where('role_id', auth()->user()->role_id)->orderBy('id','desc')->get();
                View::share(['rolepermissions'=> $rolepermissions,'roles'=>$roles]);
            }
        });
    }
}
