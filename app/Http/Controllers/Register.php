<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class Register extends Controller
{
    public $name, $lastname, $phone, $email, $confirm_password, $password;
    public function index()
    {
        return view('register');
    }
    public function resetForm(){
        $this->name = '';
        $this->lastname = '';
        $this->phone = '';
        $this->email = '';
        $this->password = '';
        $this->confirm_password = '';
    }
    public function register()
    {
        $this->validate([
            'name'=>'required',
            'phone'=>'required|regex:/^[0-9]+$/i',
            'lastname'=>'required',
            'email'=>'required',
        ],[
            'name.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'phone.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'phone.regex'=>'ປ້ອນເປັນຕົວເລກທັງຫມົດ!',
            'lastname.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'email.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
        ]);
        if($this->password == $this->confirm_password){
                $user = User::create([
                    'name'=> $this->name,
                    'lastname'=> $this->lastname,
                    'phone'=> $this->phone,
                    'email'=> $this->email,
                    'password'=> bcrypt($this->password),
                ]);
                $this->emit('alert', ['type' => 'success', 'message' => 'ລົງທະບຽນນຳໃຊ້ລະບົບສຳເລັດ!']);
                return redirect(route('dashboard'));
                $this->resetForm();
        }else{
           session()->flash('no_match', 'ລະຫັດຢືນຢັນບໍ່ຖືກຕ້ອງ');
        }
    }
}
