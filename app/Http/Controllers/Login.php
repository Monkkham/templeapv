<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Brian2694\Toastr\Facades\Toastr;
use Hash;

class Login extends Controller
{
    public $remember, $phone, $password;
    public function index()
    {
        return view('login');
    }

    public function store(Request $request)
    {
        dd();
        $request->validate([
            'phone' => 'required',
            'password' => 'required'
        ],[
            'phone.required' => 'ກະລຸນາປ້ອນເບີໂທກ່ອນ!',
            'password.required' => 'ກະລຸນາປ້ອນລະຫັດກ່ອນ!'
        ]);
        if(Auth::attempt([
            'phone'=>$request->phone,
            'password'=>$request->password
        ],$this->remember))
        {
            session()->flash('success', 'ເຂົ້າສູ່ລະບົບສຳເລັດເເລ້ວ');
            return redirect(route('dashboard'));
        }else{
            session()->flash('error', 'ເບີໂທ ຫລື ລະຫັດຜ່ານ ບໍ່ຖືກຕ້ອງ!ກະລຸນາລອງໃໝ່');
            return redirect(route('login.index'));
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login.index');
    }

}
