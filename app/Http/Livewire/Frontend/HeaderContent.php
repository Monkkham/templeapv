<?php

namespace App\Http\Livewire\Frontend;

use Livewire\Component;
use App\Models\Roles;
class HeaderContent extends Component
{
    public function render()
    {
        
        return view('layouts.front-end.header')->layout('layouts.front-end.base');
    }
    public function create()
    {
        $this->dispatchBrowserEvent('show-modal-add');
    }
}
