<?php

namespace App\Http\Livewire\Frontend;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Provinces;
use App\Models\Districts;
use App\Models\Villages;
use Livewire\Component;

class RegisterContent extends Component
{
public $name,
        $lastname,
        $gender,
        $phone,
        $email,
        $password,
        $address,
        $village_id,
        $district_id,
        $province_id,
        $roles_id,
        $image,
        $confirm_password;
        public $villages = [];
        public $districts = [];
        public function resetform()
        {
        $this->name = '';
        $this->lastname = '';
        $this->gender = '';
        $this->phone = '';
        $this->email = '';
        $this->password = '';
        $this->address = '';
        $this->village_id = '';
        $this->district_id = '';
        $this->province_id = '';
        $this->roles_id = '';
        $this->image = '';
        $this->confirm_password = '';
        }
        public function moun()
        {
            $this->role_id = 2;
        }
        public function render()
        {
            // $province = Provinces::orderBy('id','desc')->get();
            if(!empty($this->province_id)){
                $this->districts = Districts::where('province_id', $this->province_id)->orderBy('id','desc')->get();
            }
            if(!empty($this->district_id)){
                $this->villages = Villages::where('district_id',$this->district_id)->orderBy('id','desc')->get();
            }
            return view('livewire.frontend.register-content')
            ->layout('layouts.front-end.base');
        }
        protected $rules = [
            'name'=>'required',
            'lastname'=>'required',
            'phone'=>'required|numeric|digits_between:8,8|unique:users,phone',
            'password'=>'required|min:6',
            // 'province_id'=>'required',
            // 'district_id'=>'required',
            // 'village_id'=>'required',
        ];
        protected $messages = [
            'name.required'=>'ກະລຸນາປ້ອນຊື່ກ່ອນ!',
            'lastname.required'=>'ກະລຸນາປ້ອນນາມສະກຸນກ່ອນ!',
            'phone.required'=>'ກະລຸນາປ້ອນເບີໂທກ່ອນ!',
            'phone.numeric'=>'ກະລຸນາປ້ອນເບີໂທເປັນຕົວເລກ!',
            'phone.digits_between'=>'ກະລຸນາປ້ອນເບີໂທ 8 ຕົວເລກ!',
            'phone.unique'=>'ເບີໂທລະສັບນີ້ມີໃນລະບົບເເລ້ວ!',
            'password.required'=>'ກະລຸນາປ້ອນລະຫັດຜ່ານກ່ອນ!',
            'password.min'=>'ລະຫັດຜ່ານ 6 ຕົວຂື້ນໄປ!',
            // 'province_id.required'=>'ກະລຸນາເລືອກແຂວງກ່ອນ!',
            // 'district_id.required'=>'ກະລຸນາເລືອກເມືອງກ່ອນ!',
            // 'village_id.required'=>'ກະລຸນາເລຶືອກບ້ານກ່ອນ!',
        ];
        public function update($propertyName)
        {
            $this->validateOnly($propertyName);
        }
        public function register()
        {
            $this->validate();
            if($this->password == $this->confirm_password){
                // if($this->agree == 'agree'){
                    $user = User::create([
                        'name'=>$this->name,
                        'lastname'=>$this->lastname,
                        'phone'=>$this->phone,
                        'password'=>bcrypt($this->password),
                        'village_id'=>$this->village_id,
                        'district_id'=>$this->district_id,
                        'province_id'=>$this->province_id,
                    ]);
                    $this->resetForm();
                    $this->emit('alert', ['type' => 'success', 'message' => 'ລົງທະບຽນສຳເລັດເເລ້ວ!']);
                    return redirect(route('home'));
            //     }else{
            //         session()->flash('agree','ທ່ານຍອມຮັບນະໂຍບາຍຂອງພວກເຮົາບໍ່?');
            //     }
            }else{
                session()->flash('no_match','ລະຫັດຢືນຢັນບໍ່ຖືກຕ້ອງ ລອງໃຫມ່ອີກຄັ້ງ');
            }
        }


        
}
