<?php

namespace App\Http\Livewire\Frontend;

use Livewire\Component;

class LoginContent extends Component
{
    public function render()
    {
        return view('livewire.frontend.login-content')->layout('layouts.front-end.base');
    }
    public function logout()
    {
        Auth::logout();
        return redirect()->route('home');
    }
}
