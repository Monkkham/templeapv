<?php

namespace App\Http\Livewire\Backend;

use Livewire\Component;
use App\Models\Villages;
use App\Models\Districts;
use App\Models\Provinces;
use Livewire\WithPagination;
use App\Models\People_donate;
use App\Models\People_donate_detail;

class PeopleDonateContent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $villages = [];
    public $districts = [];
    public $province_id = 1;
    public $currency = 1;
    public $type = 1;
    public 
    $search,
    $ID, 
    $name_lastname, 
    $donate,
    $people_donate_id, 
    $total_money,
    $status,
    $village_id,
    $district_id,
    $sum_lak,
    $sum_thb,
    $sum_usd,
    $search_people,
    $created_at;
    public function render()
    {
        $province = Provinces::orderBy('id','desc')->get();
        if(!empty($this->province_id)){
            $this->districts = Districts::where('province_id', $this->province_id)->orderBy('id','desc')->get();
        }
        if(!empty($this->district_id)){
            $this->villages = Villages::where('district_id', $this->district_id)->orderBy('id','desc')->get();
        }
        $people_donate = People_donate::orderBy('id','desc')->paginate(10);
        if(!empty($this->search_people)){
            $select_people = People_donate::select('*')
            ->where('id', $this->search_people)
            ->orderBy('id','desc')
            ->paginate(10);
        }else{
            $select_people = People_donate::select('*')
            ->orderBy('id','desc')
            ->where('name_lastname','like','%' . $this->search. '%')
            ->paginate(10);
        }
        $people_donates = People_donate::orderBy('id','desc')->get();
        return view('livewire.backend.people-donate-content',compact('people_donate','province','select_people','people_donates'))->layout('layouts.backend.base');
    }
    public function resetField()
    {
        $this->name_lastname = '';
        $this->donate = '';
        $this->total_money = '';
        $this->district_id = '';
        $this->village_id = '';

    }
        public function create()
    {
        $this->resetField();
        $this->dispatchBrowserEvent('show-modal-add');
    }
    public function store()
    {
        // 'users.first_name' => 'required|min:2|regex:/[A-Za-z. -]/|max:255',
        // $this->validate([
            // 'name_lastname'=>'required',
            // 'donate'=>'required',
            // 'total_money'=>'required|regex:/^[0-9]+$/i',
            // 'currency'=>'required',
            // 'type'=>'required',
        // ],[
            // 'name_lastname.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            // 'total_money.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            // 'donate.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            // 'total_money.regex'=>'ປ້ອນເປັນຕົວເລກທັງຫມົດ!',
            // 'currency.required'=>'ເລືອກສະກຸນເງິນກ່ອນ!',
            // 'type.required'=>'ເລືອກປະເພດເງິນກ່ອນ!',
        // ]);

        $data = new People_donate();
        $data->name_lastname = $this->name_lastname;
        $data->province_id = $this->province_id;
        $data->district_id = $this->district_id;
        $data->village_id = $this->village_id;
        $data->save();
        $data2 = new People_donate_detail();
        $data2->people_donate_id = $data->id;
        $data2->donate = $this->donate;
        $data2->total_money = $this->total_money;
        $data2->currency = $this->currency;
        $data2->type = $this->type;
        $data2->save();
        $this->dispatchBrowserEvent('hide-modal-add');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ບັນທຶກຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ບັນທຶກຂໍ້ມູນສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
        // session()->flash('success', 'ບັນທຶກຂໍ້ມູນສຳເລັດ!');
        // return redirect()->route('backend.people_donate');
        // $this->resetField();
    }

    // public function store()
    // {
        // 'users.first_name' => 'required|min:2|regex:/[A-Za-z. -]/|max:255',
        // $this->validate([
            // 'name_lastname'=>'required',
            // 'donate'=>'required',
            // 'total_money'=>'required|regex:/^[0-9]+$/i',
            // 'currency'=>'required',
            // 'type'=>'required',
        // ],[
            // 'name_lastname.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            // 'total_money.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            // 'donate.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            // 'total_money.regex'=>'ປ້ອນເປັນຕົວເລກທັງຫມົດ!',
            // 'currency.required'=>'ເລືອກສະກຸນເງິນກ່ອນ!',
            // 'type.required'=>'ເລືອກປະເພດເງິນກ່ອນ!',
        // ]);
        // $check_number = str_replace(',', '', $this->total_money);
        // if (intval($check_number)) {
            
        // $data = new People_donate();
        // $data->name_lastname = $this->name_lastname;
        // $data->province_id = $this->province_id;
        // $data->district_id = $this->district_id;
        // $data->village_id = $this->village_id;
        // $data->save();
        // $data2 = new People_donate_detail();
        // $data2->people_donate_id = $data->id;
        // $data2->donate = $this->donate;
        // $data2->total_money = $this->total_money;
        // $data2->currency = $this->currency;
        // $data2->type = $this->type;
        // $data2->save();
        // $this->dispatchBrowserEvent('hide-modal-add');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ບັນທຶກຂໍ້ມູນສຳເລັດ!']);
        // $this->dispatchBrowserEvent('swal', [
        //     'title' => 'ບັນທຶກຂໍ້ມູນສຳເລັດ!',
        //     'icon'=>'success',
        //     'iconColor'=>'green',
        // ]);
        // session()->flash('success', 'ບັນທຶກຂໍ້ມູນສຳເລັດ!');
        // return redirect()->route('backend.people_donate');
        // $this->resetField();
    // } else {
    //     $this->emit('alert', ['type' => 'error', 'message' => 'ກະລຸນາປ້ອນຈຳນວນເງິນເປັນຕົວເລກ!']);
    // }
    // }
    public function show_new_donate($ids)
    {
        $this->dispatchBrowserEvent('show-modal-delete');
        $Data = People_donate::find($ids);
        $this->ID = $Data->id;
        $this->name_lastname = $Data->name_lastname;
        $this->sum_lak = People_donate_detail::select('total_money')
        ->where('people_donate_id',$this->ID)
        ->where('currency',1)
        ->orderBy('id', 'desc')->sum('total_money');
        $this->sum_thb = People_donate_detail::select('total_money')
        ->where('people_donate_id',$this->ID)
        ->where('currency',2)
        ->orderBy('id', 'desc')->sum('total_money');
        $this->sum_usd = People_donate_detail::select('total_money')
        ->where('people_donate_id',$this->ID)
        ->where('currency',3)
        ->orderBy('id', 'desc')->sum('total_money');
    }
    public function new_donate()
    {
         $this->validate([
            'total_money'=>'required|regex:/^[0-9]+$/i',
        ],[
            'total_money.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'total_money.regex'=>'ປ້ອນເປັນຕົວເລກທັງຫມົດ!',
        ]);
        $ids = $this->ID;
        $data = People_donate::find($ids);
        $data2 = new People_donate_detail();
        $data2->people_donate_id = $data->id;
        $data2->donate = $this->donate;
        $data2->total_money = $this->total_money;
        $data2->currency = $this->currency;
        $data2->type = $this->type;
        $data2->save();
        // $this->dispatchBrowserEvent('swal', [
        //     'title' => 'ບັນທຶກຂໍ້ມູນສຳເລັດ!',
        //     'icon'=>'success',
        //     'iconColor'=>'green',
        // ]);
        // $this->resetField();
        session()->flash('success', 'ບັນທຶກຂໍ້ມູນສຳເລັດ!');
        return redirect()->route('backend.people_donate');
        $this->dispatchBrowserEvent('hide-modal-delete');

    }
    public function edit($ids)
    {
        $this->dispatchBrowserEvent('show-modal-edit');

        $Data = People_donate::find($ids);
        $this->ID = $Data->id;
        $this->name_lastname = $Data->name_lastname;
        $this->donate = $Data->donate;
        $this->total_money = $Data->total_money;
        $this->currency = $Data->currency;
        $this->type = $Data->type;
        $this->province_id = $Data->province_id;
        $this->district_id = $Data->district_id;
        $this->village_id = $Data->village_id;
        $this->created_at = $Data->created_at;
        $this->resetField();
    }
    public function update()
    {
        $ids = $this->ID;
        $data = People_donate::find($ids);
        $data->name_lastname = $this->name_lastname;
        $data->donate = $this->donate;
        $data->total_money = $this->total_money;
        $data->currency = $this->currency;
        $data->type = $this->type;
        $data->province_id = $this->province_id;
        $data->district_id = $this->district_id;
        $data->village_id = $this->village_id;
        $data->created_at = $this->created_at;
        $data->save();
        $this->dispatchBrowserEvent('hide-modal-edit');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
        $this->resetField();
    }
    public function showDestroy($ids)
    {
        $this->dispatchBrowserEvent('show-modal-delete');
        $Data = People_donate::find($ids);
        $this->ID = $Data->id;
    }
    public function destroy()
    {
        $ids = $this->ID;
        $data = People_donate::find($ids);
        $data->delete();
        $this->dispatchBrowserEvent('hide-modal-delete');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ລຶບຂໍ້ມູນສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
    }
}
