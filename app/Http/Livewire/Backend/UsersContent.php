<?php

namespace App\Http\Livewire\Backend;

use App\Models\User;
use App\Models\Roles;
use Livewire\Component;
use App\Models\Districts;
use App\Models\Provinces;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use App\Models\Villages;

class UsersContent extends Component
{
    use WithFileUploads;
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public 
        $code, 
        $name,
        $lastname,
        $gender, 
        $birthday, 
        $phone, 
        $email,
        $address, 
        $province_id,
        $district_id, 
        $village_id,
        $image,
        $new_image,
        $password,
        $role_id, 
        $confirm_password, 
        $search, 
        $ID;
    public $village = [];
    public $district = [];
    public function mount()
    {
        $this->code = 'PAN'.date('ymdHis') . substr(fmod(microtime(true), 2), 2,0);
    }
    public function render()
    {        
        // $roles = Roles::orderBy('id','desc')->get();
        // $province = Provinces::where('del',1)->get();
        // if(!empty($this->province_id)){
        //     $this->district = Districts::where('province_id', $this->province_id)->where('del',1)->get();
        // }
        // if(!empty($this->district_id)){
        //     $this->village = Villages::where('district_id', $this->district_id)->where('del',1)->get();
        // }
        $user = User::orderBy('id','desc')->get();
        // ->where('name', 'like', '%' . $this->search. '%')
        // ->paginate(10);
        $roles = Roles::get();
        return view('livewire.backend.users-content',compact('user','roles'))->layout('layouts.backend.base');
    }
    public function resetform()
    {
           $this->code='';
           $this->name='';
           $this->lastname='';
           $this->gender=''; 
           $this->birthday=''; 
           $this->phone=''; 
           $this->email='';
           $this->address='';
           $this->province_id=''; 
           $this->district_id=''; 
           $this->village_id='';
           $this->image='';
           $this->new_image='';
           $this->password='';
           $this->confirm_password='';
           $this->search='';
           $this->ID='';
    }
    public function create()
    {
        $this->dispatchBrowserEvent('show-add');
        $this->resetform();
    }
    public function store()
    {
        $this->validate([
            'phone'=>'required|unique:users',
            'name'=>'required',
            'lastname'=>'required',
            'password'=>'required',
            'confirm_password'=>'required',
            'village_id'=>'required',
            'district_id'=>'required',
            'province_id'=>'required',
            'roles_id'=>'required',
        ],[
            'phone.required'=>'ກະລຸນາປ້ອນເບີໂທກ່ອນ!',
            'phone.unique'=>'ເບີໂທນີ້ມີໃນລະບົບແລ້ວ!',
            'name.required'=>'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
            'lastname.required'=>'ກະລຸນາໃສ່ນາມສະກຸນກ່ອນ!',
            'password.required'=>'ກະລຸນາໃສ່ລະຫັດຜ່ານກ່ອນ!',
            'confirm_password.required'=>'ກະລຸນາໃສ່ຍືນຍັນລະຫັດຜ່ານກ່ອນ!',
            'vill_id.required'=>'ກະລຸນາເລືອກຂໍ້ມູນກ່ອນ!',
            'dis_id.required'=>'ກະລຸນາເລືອກຂໍ້ມູນກ່ອນ!',
            'pro_id.required'=>'ກະລຸນາເລືອກຂໍ້ມູນກ່ອນ!',
            'roles_id.required'=>'ກະລຸນາເລືອກຂໍ້ມູນກ່ອນ!',
        ]);
        $data = new User();
        if($this->password == $this->confirm_password){
            $data->name = $this->name;
            $data->lastname = $this->lastname;
            $data->gender = $this->gender;
            $data->birthday = $this->birthday;
            $data->phone = $this->phone;
            $data->email = $this->email;
            $data->password = bcrypt($this->password);
            $data->address = $this->address;
            $data->village_id = $this->village_id;
            $data->district_id = $this->district_id;
            $data->province_id =$this->province_id;
            $data->roles_id =$this->roles_id;
            //upload image
            if (!empty($this->image)) {
                $this->validate([
                    'image' => 'required|mimes:jpg,png,jpeg',
                ]);
                $imageName = Carbon::now()->timestamp . '.' . $this->image->extension();
                $this->image->storeAs('upload/user', $imageName);
                $data->image = 'upload/user'.'/'.$imageName;
            }else{
                $data->image = '';
            }
            $data->save();
            $this->dispatchBrowserEvent('hide-add');
            $this->emit('alert', ['type' => 'success', 'message' => 'ເພີ່ມຂໍ້ມູນສຳເລັດ!']);
            $this->resetform();
            // session()->flash('success', 'ເພີ່ມຂໍ້ມູນສຳເລັດ');
            // return redirect(route('backend.user'));
        }else{
         session()->flash('no_match_password', 'ຂໍອາໄພລະຫັດຜ່ານຍືນຍັນບໍ່ຕົງກັນ!');
      }
    }
    public function showEdit($ids)
    {
        $data = User::find($ids);
        $this->ID=$data->id;
        $this->name = $data->name;
        $this->role_id = $data->role_id;
        $this->dispatchBrowserEvent('show-edit');
    }

    public function update()
    {
        $this->validate([
            'role_id'=>'required',
        ],[
            'role_id.required'=>'ເລືອກ1ສິດກ່ອນ!',
        ]);
        $ids = $this->ID;
        $data = User::find($ids);
        // $data->name = $this->name;
        // $data->lastname = $this->lastname;
        // $data->gender = $this->gender;
        // $data->birthday = $this->birthday;
        // $data->phone = $this->phone;
        // $data->email = $this->email;
        // if($this->password == $this->confirm_password){
        //     if(!empty($this->password)){
        //         $data->password = bcrypt($this->password);
        //     }
        // $data->address = $this->address;
        // $data->village_id = $this->village_id;
        // $data->district_id = $this->district_id;
        // $data->province_id =$this->province_id;
        $data->role_id =$this->role_id;
        // if ($this->image) {
        //     $this->validate([
        //         'image' => 'required|mimes:png,jpg,jpeg',
        //     ]);
        //     if ($this->image) {
        //         $this->validate([
        //             'image' => 'required|mimes:png,jpg,jpeg',
        //         ]);
        //         if ($this->image != $data->image) {
        //             if (!empty($data->image)) {
        //                 $images = explode(",", $data->images);
        //                 foreach ($images as $image) {
        //                     unlink('upload/user' . '/' . $data->image);
        //                 }
        //                 $data->delete();
        //             }
        //         }
        //         $imageName = Carbon::now()->timestamp . '.' . $this->image->extension();
        //         $this->image->storeAs('upload/user', $imageName);
        //         $data->image = 'upload/user'.'/'.$imageName;
        //     }
        // }
        $data->save();
        $this->dispatchBrowserEvent('hide-edit');
        session()->flash('success', 'ມອບສິດສຳເລັດເເລ້ວ');
        return redirect(route('backend.users'));
        // $this->emit('alert', ['type' => 'success', 'message' => 'ມອບສິດສຳເລັດເເລ້ວ!']);
        $this->resetform();
    // }else{
    //     session()->flash('no_match_password', 'ຂໍອາໄພລະຫັດຜ່ານຍືນຍັນບໍ່ຕົງກັນ!');
    //  }
    }
    public function showDestroy($ids)
    {
        $this->dispatchBrowserEvent('show-delete');
        $data = User::find($ids);
        $this->ID = $data->id;
    }
    public function destroy($ids)
    {
        $ids = $this->ID;
        $data = User::find($ids);
        $data->del = 0;
        $data->save();
        $this->dispatchBrowserEvent('hidel-delete');
        $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
        $this->resetform();
        // session()->flash('success', 'ລຶບຂໍ້ມູນສຳເລັດ');
        // return redirect(route('backend.user'));
    }
}
