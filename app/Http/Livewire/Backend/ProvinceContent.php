<?php

namespace App\Http\Livewire\Backend;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Provinces;
class ProvinceContent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $ID, $name, $search;
    public function render()
    {
        $provinces = Provinces::orderBy('id','desc')
        ->where('name','like','%' . $this->search. '%')
        ->paginate(10);
        return view('livewire.backend.province-content',compact('provinces'))->layout('layouts.backend.base');
    }
    public function resetform()
    {
        $this->name = '';
        $this->ID = '';
    }
    protected $rules = [
        'name'=>'required|unique:provinces'
    ];
    protected $messages = [
        'name.required'=>'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
        'name.unique'=>'ຂໍ້ມູນນີ້ມີໃນລະບົບເເລ້ວ!',
    ];
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }
    public function store()
    {
        $updateId = $this->ID;
        if($updateId > 0)
        {
            $this->validate([
                'name'=>'required'
            ],[
                'name.required'=>'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ'
            ]);
            $data = Provinces::find($updateId);
            $data->update([
                'name' => $this->name
                ]);
                $this->dispatchBrowserEvent('swal', [
                 'title' => 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ !',
                 'icon'=>'success',
                 'iconColor'=>'green',
             ]);
             $this->resetform();
         }
         else //ເພີ່ມໃໝ່
         {
             $validateData = $this->validate();
             Provinces::create($validateData);
             $this->dispatchBrowserEvent('swal', [
                 'title' => 'ເພີ່ມຂໍ້ມູນສຳເລັດ !',
                 'icon'=>'success',
                 'iconColor'=>'green',
             ]);
             $this->resetform();
         }
         // $this->emit('alert', ['type' => 'success', 'message' => 'ເພີ່ມຂໍ້ມູນສຳເລັດ!']);
        }
        public function edit($ids)
        {
            $data = Provinces::find($ids);
            $this->name = $data->name;
            $this->ID = $data->id;
        }
        public function showDestroy($ids)
        {
            $this->dispatchBrowserEvent('show-modal-delete');
            $data = Provinces::find($ids);
            $this->ID = $data->id;
        }

        public function destroy($ids)
        {
            $ids = $this->ID;
            $data = Provinces::find($ids);
            $data->delete();
            $this->dispatchBrowserEvent('hide-modal-delete');
            // $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
            $this->dispatchBrowserEvent('swal', [
                    'title' => 'ລຶບຂໍ້ມູນສຳເລັດ !',
                    'icon'=>'success',
                    'iconColor'=>'green',
                    ]);
                    $this->resetform();
        }
        // public function destroy($ids)
        // {
        //     $ids = $this->ID;
        //     $provinces = Provinces::find($ids);
        //         $provinces->del = 0;
        //         $provinces->save();
        //         $this->dispatchBrowserEvent('hide-modal-delete');
        //         $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
        //         $this->dispatchBrowserEvent('swal', [
        //             'title' => 'ລຶບຂໍ້ມູນສຳເລັດ !',
        //             'icon'=>'success',
        //             'iconColor'=>'green',
        //         ]);
        //         $this->resetdata();
        // }
}
