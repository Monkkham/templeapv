<?php

namespace App\Http\Livewire\Backend;

use App\Models\General;
use Livewire\Component;
use Livewire\WithPagination;

class GeneralContent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $hiddenId, $name, $number,$search, $date;
    public function render()
    {
        $general = General::orderBy('id','desc')   
        ->where('name','like','%' . $this->search. '%')
        ->paginate(10);
        return view('livewire.backend.general-content',compact('general'))->layout('layouts.backend.base');
    }
    public function resetField()
    {
        $this->name = '';
        $this->number = '';
        $this->date = '';
    }
        public function create(){

        $this->resetField();
        $this->dispatchBrowserEvent('show-modal-add');
    }

    public function store()
    {
        // 'users.first_name' => 'required|min:2|regex:/[A-Za-z. -]/|max:255',
        $this->validate([
            'name'=>'required',
            // 'number'=>'required|regex:/^[0-9]+$/i',
            'date'=>'required',
        ],[
            'name.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            // 'number.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            // 'number.regex'=>'ປ້ອນເປັນຕົວເລກທັງຫມົດ!',
            'date.required'=>'ເລືອກວັນທີ່ກ່ອນ!',
        ]);
        // $check_number = str_replace(',', '', $this->qty_price);
        // if (intval($check_number)) {
        $data = new General();
        $data->name = $this->name;
        $data->number = $this->number;
        $data->date = $this->date;
        $data->save();
        $this->dispatchBrowserEvent('hide-modal-add');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ບັນທຶກຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ບັນທຶກຂໍ້ມູນສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
        // session()->flash('message', 'ບັນທຶກຂໍ້ມູນສຳເລັດ!');
        // return redirect()->route('backend.income');
        $this->resetField();
    // } else {
    //     $this->emit('alert', ['type' => 'error', 'message' => 'ກະລຸນາປ້ອນຈຳນວນເງິນເປັນຕົວເລກ!']);
    // }
    }
    public function edit($ids)
    {
        $this->dispatchBrowserEvent('show-modal-edit');

        $Data = General::find($ids);
        $this->hiddenId = $Data->id;
        $this->name = $Data->name;
        $this->number = $Data->number;
        $this->date = $Data->date;
    }
    public function update()
    {
        $ids = $this->hiddenId;
        $data = General::find($ids);
        $data->name = $this->name;
        $data->number = $this->number;
        $data->date = $this->date;
        $data->save();
        $this->dispatchBrowserEvent('hide-modal-edit');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
        $this->resetField();
    }
    public function showDestroy($ids)
    {
        $this->dispatchBrowserEvent('show-modal-delete');
        $Data = General::find($ids);
        $this->hiddenId = $Data->id;
    }
    public function destroy()
    {
        $ids = $this->hiddenId;
        $data = General::find($ids);
        $data->delete();
        $this->dispatchBrowserEvent('hide-modal-delete');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ລຶບຂໍ້ມູນສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
    }
}
