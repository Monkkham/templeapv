<?php

namespace App\Http\Livewire\Backend;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\People_donate_detail;
use DB;
class ReadPeopleDonateContent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public 
    $search,
    $ID, 
    $donate,
    $people_donate_id, 
    $total_money,
    $currency,
    $type,
    $status,
    $district_id,
    $created_at;

    protected $listeners = ['resetMySelected' => 'resetSelected'];
    public $mySelected = [];
    public $selectAll = false;
    public $firstId = NULL;

    public function render()
    {
        $read_people_donate = People_donate_detail::where('status',1)->get();
        // ->where('name_lastname','like','%' . $this->search. '%')->get();
        // $this->firstId = $read_people_donate[0]->id;
        return view('livewire.backend.read-people-donate-content',compact('read_people_donate'))->layout('layouts.backend.base');
    }
    public function refress()
    {
        return;
    }

    // public function updatedSelectAll($value)
    // {
    //     if($value){
    //         $this->mySelected = People_donate::where('id','>=',$this->firstId)->limit(5)->pluck('id');
    //     }else{
    //         $this->mySelected = [];
    //     }
    // }

    // public function updatedMySelected($value)
    // {
    //     if(count($value) == 5){
    //         $this->selectAll = true;
    //     }else{
    //         $this->selectAll = false;
    //     }
    // }

    public function resetSelected()
    {
        $this->mySelected = [];
        $this->selectAll = false;
    }
    public function showDestroy()
    {
        $this->dispatchBrowserEvent('show-modal-delete');
    }
    public function confirm_readed()
    {
        $delete = DB::table('people_donate_detail')->WhereIn('id', $this->mySelected)->update(array('status' => '2'));
        // People_donate::WhereIn('id',$this->mySelected)->delete();
        $this->mySelected = [];
        $this->selectAll = false;
        $this->dispatchBrowserEvent('hide-modal-delete');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ຍືນຍັນອ່ານເເລ້ວສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
    }

}
