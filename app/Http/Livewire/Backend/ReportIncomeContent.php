<?php

namespace App\Http\Livewire\Backend;

use App\Models\Income;
use Livewire\Component;

class ReportIncomeContent extends Component
{
    public $start, $end;
    public $starts, $ends;
    public $money_type;
    public function render()
    {
        if(!empty($this->money_type))
        {
            $start = $this->starts;
            $end = date('Y-m-d', strtotime($this->ends . ' + ' . 0 . 'days'));
            $report_income = Income::select('*')
                ->whereBetween('created_at', [$this->starts, $end])
                ->where('type',$this->money_type)
                ->get();
                $sum1 = Income::select('qty_price')
                ->whereBetween('created_at', [$this->starts, $end])
                ->where('type',$this->money_type)
                ->where('currency',1)->where('type_donate',1)
                ->orderBy('id', 'desc')->sum('qty_price');
                $sum2 = Income::select('qty_price')
                ->whereBetween('created_at', [$this->starts, $end])
                ->where('type',$this->money_type)
                ->where('currency',2)->where('type_donate',1)
                ->orderBy('id', 'desc')->sum('qty_price');
                $sum3 = Income::select('qty_price')
                ->whereBetween('created_at', [$this->starts, $end])
                ->where('type',$this->money_type)
                ->where('currency',3)->where('type_donate',1)
                ->orderBy('id', 'desc')->sum('qty_price');
                $sum4 = Income::select('qty_price')
                ->whereBetween('created_at', [$this->starts, $end])
                ->where('type',$this->money_type)
                ->where('currency',1)->where('type_donate',2)
                ->orderBy('id', 'desc')->sum('qty_price');
                $sum5 = Income::select('qty_price')
                ->whereBetween('created_at', [$this->starts, $end])
                ->where('type',$this->money_type)
                ->where('currency',2)->where('type_donate',2)
                ->orderBy('id', 'desc')->sum('qty_price');
                $sum6 = Income::select('qty_price')
                ->whereBetween('created_at', [$this->starts, $end])
                ->where('type',$this->money_type)
                ->where('currency',3)->where('type_donate',2)
                ->orderBy('id', 'desc')->sum('qty_price');
        }else{
            $start = $this->starts;
            $end = date('Y-m-d', strtotime($this->ends . ' + ' . 1 . 'days'));
            $report_income = Income::select('*')
                ->whereBetween('created_at', [$this->starts, $end])->get();
                $sum1 = Income::select('qty_price')
                ->whereBetween('created_at', [$this->starts, $end])
                ->where('currency',1)->where('type_donate',1)
                ->orderBy('id', 'desc')->sum('qty_price');
                $sum2 = Income::select('qty_price')
                ->whereBetween('created_at', [$this->starts, $end])
                ->where('currency',2)->where('type_donate',1)
                ->orderBy('id', 'desc')->sum('qty_price');
                $sum3 = Income::select('qty_price')
                ->whereBetween('created_at', [$this->starts, $end])
                ->where('currency',3)->where('type_donate',1)
                ->orderBy('id', 'desc')->sum('qty_price');
                $sum4 = Income::select('qty_price')
                ->whereBetween('created_at', [$this->starts, $end])
                ->where('currency',1)->where('type_donate',2)
                ->orderBy('id', 'desc')->sum('qty_price');
                $sum5 = Income::select('qty_price')
                ->whereBetween('created_at', [$this->starts, $end])
                ->where('currency',2)->where('type_donate',2)
                ->orderBy('id', 'desc')->sum('qty_price');
                $sum6 = Income::select('qty_price')
                ->whereBetween('created_at', [$this->starts, $end])
                ->where('currency',3)->where('type_donate',2)
                ->orderBy('id', 'desc')->sum('qty_price');
        }
        return view('livewire.backend.report-income-content',compact('report_income','sum1','sum2','sum3','sum4','sum5','sum6'))->layout('layouts.backend.base');
    }
    // public function sub()
    // {
    //    if(!empty($this->start || $this->end))
    //    {
    //     $this->starts = $this->start;
    //     $this->ends = $this->end;
    //    }else{
    //     $this->emit('alert', ['type' => 'error', 'message' => 'ເລືອກວັນທີ່ຫາວັນທີ່ກ່ອນຂະນ້ອຍໃຈເຍັນນ!']);
    //    }
    // }
}
