<?php

namespace App\Http\Livewire\Backend;

use App\Models\Expend;
use App\Models\Income;
use Auth;
use Livewire\Component;

class ReportExpendContent extends Component
{
    public $start, $end;
    public $starts, $ends;
    public $money_type;
    public function render()
    {
        if (!empty($this->money_type)) {
            $start = $this->starts;
            $end = date('Y-m-d', strtotime($this->ends . ' + ' . 0 . 'days'));
            $report_expend = Expend::select('*')
                ->whereBetween('created_at', [$this->starts, $end])
                ->where('type', $this->money_type)
                ->get();
            $sum1 = Expend::select('qty_price')
                ->whereBetween('created_at', [$this->starts, $end])
                ->where('type', $this->money_type)
                ->where('currency', 1)->where('type_donate', 1)
                ->orderBy('id', 'desc')->sum('qty_price');
            $sum2 = Expend::select('qty_price')
                ->whereBetween('created_at', [$this->starts, $end])
                ->where('type', $this->money_type)
                ->where('currency', 1)->where('type_donate', 2)
                ->orderBy('id', 'desc')->sum('qty_price');
            $sum_income1 = Income::select('qty_price')
                ->whereBetween('created_at', [$this->starts, $end])
                ->where('type', $this->money_type)
                ->where('currency', 1)->where('type_donate', 1)
                ->orderBy('id', 'desc')->sum('qty_price');
            $sum_income2 = Income::select('qty_price')
                ->whereBetween('created_at', [$this->starts, $end])
                ->where('type', $this->money_type)
                ->where('currency', 1)->where('type_donate', 2)
                ->orderBy('id', 'desc')->sum('qty_price');
        } else {
            $start = $this->starts;
            $end = date('Y-m-d', strtotime($this->ends . ' + ' . 0 . 'days'));
            $report_expend = Expend::select('*')
                ->whereBetween('created_at', [$this->starts, $end])
                ->get();
            $sum1 = Expend::select('qty_price')
                ->whereBetween('created_at', [$this->starts, $end])
                ->where('currency', 1)->where('type_donate', 1)
                ->orderBy('id', 'desc')->sum('qty_price');
            $sum2 = Expend::select('qty_price')
                ->whereBetween('created_at', [$this->starts, $end])
                ->where('currency', 1)->where('type_donate', 2)
                ->orderBy('id', 'desc')->sum('qty_price');
            $sum_income1 = Income::select('qty_price')
                ->whereBetween('created_at', [$this->starts, $end])
                ->where('currency', 1)->where('type_donate', 1)
                ->orderBy('id', 'desc')->sum('qty_price');
            $sum_income2 = Income::select('qty_price')
                ->whereBetween('created_at', [$this->starts, $end])
                ->where('currency', 1)->where('type_donate', 2)
                ->orderBy('id', 'desc')->sum('qty_price');
        }
        return view('livewire.backend.report-expend-content', compact('report_expend', 'sum1', 'sum2', 'sum_income1', 'sum_income2'))->layout('layouts.backend.base');
    }
    public function mount()
    {
        $this->name = Auth::user()->name;
        // $this->lastname  = Auth::user()->lastname;
    }
    // public function sub()
    // {
    //     if (!empty($this->start || $this->end)) {
    //         $this->starts = $this->start;
    //         $this->ends = $this->end;
    //     } else {
    //         $this->emit('alert', ['type' => 'error', 'message' => 'ເລືອກວັນທີ່ຫາວັນທີ່ກ່ອນຂະນ້ອຍໃຈເຍັນນ!']);
    //     }
    // }
}
