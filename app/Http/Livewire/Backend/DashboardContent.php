<?php

namespace App\Http\Livewire\Backend;

use DB;
use App\Models\Income;
use Livewire\Component;
use App\Models\Expend;
class DashboardContent extends Component
{
    public function render()
    {
        // ====== count monk novice ======= //
        $count_all_monk = DB::table('monk_novice')->where('status_now',1)->count();
        $count_teach_monk = DB::table('monk_novice')->where('gender',1)->where('status_now',1)->count();
        $count_monk = DB::table('monk_novice')->where('gender',2)->where('status_now',1)->count();
        $count_novice = DB::table('monk_novice')->where('gender',3)->where('status_now',1)->count();

        $count_income = DB::table('income')->count();
        $count_expend = DB::table('expend')->count();
        $count_user = DB::table('users')->count();
        $income = Income::orderBy('id','desc')->get();
        $expend = Expend::orderBy('id','desc')->get();
        $sum_income_lak = DB::table("income")->where('currency',1)->sum('qty_price');
        $sum_income_thb = DB::table("income")->where('currency',2)->sum('qty_price');
        $sum_income_usd = DB::table("income")->where('currency',3)->sum('qty_price');
        $sum_expend_lak = DB::table("expend")->where('currency',1)->sum('qty_price');
        $sum_expend_thb = DB::table("expend")->where('currency',2)->sum('qty_price');
        $sum_expend_usd = DB::table("expend")->where('currency',3)->sum('qty_price');
        return view('livewire.backend.dashboard-content',
        compact('count_income',
        'count_expend',
        'income',
        'expend',
        'sum_income_lak',
        'sum_income_thb',
        'sum_income_usd',
        'sum_expend_lak',
        'sum_expend_thb',
        'sum_expend_usd',
        'count_user',
        'count_all_monk',
        'count_teach_monk',
        'count_monk',
        'count_novice'
        ))
        ->layout('layouts.backend.base');
    }
}
