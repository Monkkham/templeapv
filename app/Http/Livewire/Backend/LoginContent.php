<?php

namespace App\Http\Livewire\Backend;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
class LoginContent extends Component
{
    public $phone, $password,$remember;
    public function render()
    {
        return view('livewire.backend.login-content')->layout('layouts.backend.login');
    }
    public function login()
    {
        $this->validate([
            'phone' => 'required',
            'password' => 'required',
        ],[
            'phone.required' => 'ປ້ອນເບີກ່ອນຂະນ້ອຍ!',
            'password.required' => 'ປ້ອນລະຫັດກ່ອນຂະນ້ອຍ!',
        ]);
        if (Auth::attempt([
            'phone' => $this->phone,
            'password' => $this->password],
            $this->remember)) 
        {
            session()->flash('success', 'ເຂົ້າລະບົບໄດ້ລະເດີ້ຂະນ້ອຍ!');
            return redirect(route('dashboard'));
        }else{
            session()->flash('error', 'ອະແນ່ລືມລະຫັດຜ່ານອີກລະ!');
            return redirect(route('backend.login'));
        }
    }
}
