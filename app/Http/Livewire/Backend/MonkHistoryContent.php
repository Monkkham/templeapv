<?php

namespace App\Http\Livewire\Backend;
use App\Models\Monk_novice;
use Livewire\Component;
use Livewire\WithPagination;
use Livewire\WithFileUploads;

class MonkHistoryContent extends Component
{
    use WithFileUploads;
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public function render()
    {
        $monk_novice = Monk_novice::whereIn('status_now', [2,3])
        ->paginate(10);
        return view('livewire.backend.monk-history-content',compact('monk_novice'))->layout('layouts.backend.base');
    }
}
