<?php

namespace App\Http\Livewire\Backend;

use Livewire\Component;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
class RegisterContent extends Component
{
    public $name, $lastname, $phone, $email, $confirm_password, $password;
    public function render()
    {
        return view('livewire.backend.register-content')->layout('layouts.backend.login');
    }
    public function resetForm(){
        $this->name = '';
        $this->lastname = '';
        $this->phone = '';
        // $this->email = '';
        $this->password = '';
        $this->confirm_password = '';
    }
    public function register()
    {
        $this->validate([
            'name'=>'required',
            'password'=>'required',
            'phone'=>'required|regex:/^[0-9]+$/i',
            'phone'=>'required|unique:users',
            'lastname'=>'required',
            // 'email'=>'required',
            'phone'=>'required|min:8',
        ],[
            'name.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'phone.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'phone.unique'=>'ເບີໂທນີ້ມີໃນລະບົບເເລ້ວ!',
            'phone.regex'=>'ປ້ອນເປັນຕົວເລກທັງຫມົດ!',
            'lastname.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            // 'email.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'password.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'phone.min'=>'ເບີໂທລະສັບຕ້ອງ 8 ຕົວເລກ',
        ]);
        if($this->password == $this->confirm_password){
                $user = User::create([
                    'name'=> $this->name,
                    'lastname'=> $this->lastname,
                    'phone'=> $this->phone,
                    // 'email'=> $this->email,
                    'password'=> bcrypt($this->password),
                ]);
                $this->emit('alert', ['type' => 'success', 'message' => 'ລົງທະບຽນນຳໃຊ້ລະບົບສຳເລັດ!']);
                Auth::login($user);
                return redirect(route('dashboard'));
                $this->resetForm();
        }else{
        //    session()->flash('no_match', 'ລະຫັດຢືນຢັນບໍ່ຖືກຕ້ອງ');
           $this->emit('alert', ['type' => 'error', 'message' => 'ລະຫັດຢືນຢັນບໍ່ຕົງກັນນິມົນກວດຄືນ!']);
        }
    }
}
