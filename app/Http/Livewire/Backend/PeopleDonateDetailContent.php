<?php

namespace App\Http\Livewire\Backend;

use Livewire\Component;

class PeopleDonateDetailContent extends Component
{
    public function render()
    {
        return view('livewire.backend.people-donate-detail-content');
    }
}
