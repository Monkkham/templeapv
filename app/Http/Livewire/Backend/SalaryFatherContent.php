<?php

namespace App\Http\Livewire\Backend;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Salary_father;
use App\Models\SalaryDetail;
class SalaryFatherContent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $currency = 1;
    public $type = 2;
    public $list_detail = [];
    public 
    $search,
    $ID, 
    $name, 
    $donate,
    $salary_father_id, 
    $money,
    $company,
    $sum_lak,
    $sum_thb,
    $sum_usd,
    $created_at;
    public function render()
    {
        $salary_father = Salary_father::orderBy('id','desc')->paginate(10);
        return view('livewire.backend.salary-father-content',compact('salary_father'))->layout('layouts.backend.base');
    }
    public function Salary_detail($ids)
    {
        return redirect(route('backend.salary-detail',$ids));
    }
    public function resetField()
    {
        $this->name = '';
        $this->money = '';
        $this->company = '';

    }
    public function resetForm()
    {
        $this->money = '';
    }
        public function create()
    {
        $this->resetField();
        $this->dispatchBrowserEvent('show-modal-add');
    }
    public function store()
    {
        // 'users.first_name' => 'required|min:2|regex:/[A-Za-z. -]/|max:255',
        $this->validate([
            'name'=>'required',
            'company'=>'required',
            'money'=>'required|regex:/^[0-9]+$/i',
            'currency'=>'required',
            'type'=>'required',
        ],[
            'name.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'money.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'company.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'money.regex'=>'ປ້ອນເປັນຕົວເລກທັງຫມົດ!',
            'currency.required'=>'ເລືອກສະກຸນເງິນກ່ອນ!',
            'type.required'=>'ເລືອກປະເພດເງິນກ່ອນ!',
        ]);

        $data = new Salary_father();
        $data->name = $this->name;
        $data->company = $this->company;
        $data->save();
        $data2 = new SalaryDetail();
        $data2->salary_father_id = $data->id;
        $data2->money = $this->money;
        $data2->currency = $this->currency;
        $data2->type = $this->type;
        $data2->save();
        $this->dispatchBrowserEvent('hide-modal-add');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ບັນທຶກຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ບັນທຶກຂໍ້ມູນສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
        // session()->flash('success', 'ບັນທຶກຂໍ້ມູນສຳເລັດ!');
        // return redirect()->route('backend.people_donate');
        // $this->resetField();
    }

    // public function store()
    // {
        // 'users.first_name' => 'required|min:2|regex:/[A-Za-z. -]/|max:255',
        // $this->validate([
            // 'name_lastname'=>'required',
            // 'donate'=>'required',
            // 'total_money'=>'required|regex:/^[0-9]+$/i',
            // 'currency'=>'required',
            // 'type'=>'required',
        // ],[
            // 'name_lastname.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            // 'total_money.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            // 'donate.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            // 'total_money.regex'=>'ປ້ອນເປັນຕົວເລກທັງຫມົດ!',
            // 'currency.required'=>'ເລືອກສະກຸນເງິນກ່ອນ!',
            // 'type.required'=>'ເລືອກປະເພດເງິນກ່ອນ!',
        // ]);
        // $check_number = str_replace(',', '', $this->total_money);
        // if (intval($check_number)) {
            
        // $data = new People_donate();
        // $data->name_lastname = $this->name_lastname;
        // $data->province_id = $this->province_id;
        // $data->district_id = $this->district_id;
        // $data->village_id = $this->village_id;
        // $data->save();
        // $data2 = new People_donate_detail();
        // $data2->people_donate_id = $data->id;
        // $data2->donate = $this->donate;
        // $data2->total_money = $this->total_money;
        // $data2->currency = $this->currency;
        // $data2->type = $this->type;
        // $data2->save();
        // $this->dispatchBrowserEvent('hide-modal-add');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ບັນທຶກຂໍ້ມູນສຳເລັດ!']);
        // $this->dispatchBrowserEvent('swal', [
        //     'title' => 'ບັນທຶກຂໍ້ມູນສຳເລັດ!',
        //     'icon'=>'success',
        //     'iconColor'=>'green',
        // ]);
        // session()->flash('success', 'ບັນທຶກຂໍ້ມູນສຳເລັດ!');
        // return redirect()->route('backend.people_donate');
        // $this->resetField();
    // } else {
    //     $this->emit('alert', ['type' => 'error', 'message' => 'ກະລຸນາປ້ອນຈຳນວນເງິນເປັນຕົວເລກ!']);
    // }
    // }
    public function show_new_donate($ids)
    {
        $this->dispatchBrowserEvent('show-modal-delete');
        $Data = Salary_father::find($ids);
        $this->ID = $Data->id;
        $this->name = $Data->name;
        $this->resetForm();
    }
    public function new_donate()
    {
         $this->validate([
            'money'=>'required|regex:/^[0-9]+$/i',
        ],[
            'money.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'money.regex'=>'ປ້ອນເປັນຕົວເລກທັງຫມົດ!',
        ]);
        $ids = $this->ID;
        $data = Salary_father::find($ids);
        $data2 = new SalaryDetail();
        $data2->salary_father_id = $data->id;
        $data2->money = $this->money;
        $data2->currency = $this->currency;
        $data2->type = $this->type;
        $data2->save();
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ບັນທຶກຂໍ້ມູນສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
        $this->resetField();
        // session()->flash('success', 'ບັນທຶກຂໍ້ມູນສຳເລັດ!');
        // return redirect()->route('backend.people_donate');
        $this->dispatchBrowserEvent('hide-modal-delete');

    }
    public function edit($ids)
    {
        $this->dispatchBrowserEvent('show-modal-edit');

        $Data = Salary_father::find($ids);
        $this->ID = $Data->id;
        $this->name = $Data->name;
        $this->company = $Data->company;
        $this->created_at = $Data->created_at;
    }
    public function update()
    {
        $ids = $this->ID;
        $data = Salary_father::find($ids);
        $data->name = $this->name;
        $data->company = $this->company;
        $data->created_at = $this->created_at;
        $data->save();
        $this->dispatchBrowserEvent('hide-modal-edit');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
        $this->resetField();
    }
    public function show_salary_detail($ids)
    {
        $this->dispatchBrowserEvent('show-detail');
        $Data = Salary_father::find($ids);
        $this->ID = $Data->id;
        $this->name = $Data->name;
        $this->sum_lak = SalaryDetail::select('money')
        ->where('salary_father_id',$this->ID)
        ->where('currency',1)
        ->orderBy('id', 'desc')->sum('money');
        $this->sum_thb = SalaryDetail::select('money')
        ->where('salary_father_id',$this->ID)
        ->where('currency',2)
        ->orderBy('id', 'desc')->sum('money');
        $this->sum_usd = SalaryDetail::select('money')
        ->where('salary_father_id',$this->ID)
        ->where('currency',3)
        ->orderBy('id', 'desc')->sum('money');
        $this->list_detail = SalaryDetail::select('*')
        ->where('salary_father_id',$this->ID)->get();
    }
    public function showDestroy($ids)
    {
        $this->dispatchBrowserEvent('show-modal-delete');
        $Data = People_donate::find($ids);
        $this->ID = $Data->id;
    }
    public function destroy()
    {
        $ids = $this->ID;
        $data = People_donate::find($ids);
        $data->delete();
        $this->dispatchBrowserEvent('hide-modal-delete');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ລຶບຂໍ້ມູນສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
    }
    public function show_paymoney($ids)
    {
        $this->dispatchBrowserEvent('show-modal-paymoney');
        $Data = SalaryDetail::find($ids);
        $this->salary_father_id = $ids;
        $this->name = $Data->name;
        $this->money = $Data->money;
        $this->resetForm();
    }
    public function paymoney()
    {
         $this->validate([
            'money'=>'required|regex:/^[0-9]+$/i',
        ],[
            'money.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'money.regex'=>'ປ້ອນເປັນຕົວເລກທັງຫມົດ!',
        ]);
        $check_int = str_replace(',','',$this->money);
        if(intval($check_int)){
            $data = SalaryDetail::find($this->salary_father_id);
            if(intval($data->money <= 0)){
                $this->emit('alert', ['type' => 'warning', 'message' => 'ຂໍອາໄພບໍ່ມີຈຳນວນເງິນໃຫ້ຖອນໄດ້!']);
            }elseif(intval($data->money < $check_int)){
                $this->emit('alert', ['type' => 'warning', 'message' => 'ຂໍອາໄພຍອດເງິນໃນບັນຊີຕ່ຳກວ່າຈຳນວນທີ່ຈະຖອນ!']);
            }else{
                $data->money = $data->money - $check_int;
                $data->save();
                $this->resetField();
                // session()->flash('success', 'ບັນທຶກຂໍ້ມູນສຳເລັດ!');
                // return redirect()->route('backend.people_donate');
                $this->dispatchBrowserEvent('hide-modal-paymoney');
                $this->dispatchBrowserEvent('swal', [
                    'title' => 'ຖອນເງິນອອກສຳເລັດ!',
                    'icon'=>'success',
                    'iconColor'=>'green',
                ]);
            }
        }else{
            $this->emit('alert', ['type' => 'warning', 'message' => 'ກະລຸນາປ້ອນຂໍ້ມູນເປັນຕົວເລກ!']);
            $this->money = "";
        }
    }
}
