<?php

namespace App\Http\Livewire\Backend;

use App\Models\Type;
use App\Models\Curren;
use App\Models\Expend;
use Livewire\Component;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use Carbon\Carbon;
class ExpendContent extends Component
{
    use WithFileUploads;
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $hiddenId,$image,$newimage,$search, $name, $qty_price, $created_at,$creator_id;
    public $currency = 1;
    public $type = 1;
    public $type_donate = 2;
    public function render()
    {
        $expend = Expend::orderBy('id','desc')
        ->where('name','like','%' . $this->search. '%')
        ->orwhere('created_at','like','%' . $this->search. '%')
        ->paginate(10);
        return view('livewire.backend.expend-content',compact('expend'))->layout('layouts.backend.base');
    }
    public function resetField()
    {
        $this->image = '';
        $this->name = '';
        $this->qty_price = '';
        $this->created_at = '';
    }
        public function create(){

        $this->resetField();
        $this->dispatchBrowserEvent('show-modal-add');
    }

    public function store()
    {
        $this->validate([
            'name'=>'required',
            'qty_price'=>'required|regex:/^[0-9]+$/i',
            'currency'=>'required',
            'type'=>'required',
            'type_donate'=>'required',
            // 'date'=>'required',
        ],[
            'name.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'qty_price.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'qty_price.regex'=>'ປ້ອນເປັນຕົວເລກທັງຫມົດ!',
            'currency.required'=>'ເລືອກສະກຸນເງິນກ່ອນ!',
            'type.required'=>'ເລືອກປະເພດເງິນກ່ອນ!',
            'type_donate.required'=>'ເລືອກປະເພດກ່ອນ!',
            // 'date.required'=>'ເລືອກວັນທີ່ກ່ອນ!',
        ]);
        $check_number = str_replace(',', '', $this->qty_price);
        if (intval($check_number)) {
        $data = new Expend();
        if (!empty($this->image)) {
            $this->validate([
                'image' => 'required|mimes:jpg,png,jpeg',
            ]);
            $imageName = Carbon::now()->timestamp . '.' . $this->image->extension();
            // $this->image->storeAs('bill_photo', $imageName);
            // $data->image = 'bill_photo'.'/'. $imageName;
            $this->image->storeAs('bill_photo', $imageName);
            $data->image = $imageName;
        }else{
            $data->image = '';
        }
        $data->name = $this->name;
        $data->qty_price = $this->qty_price;
        $data->currency = $this->currency;
        $data->type = $this->type;
        $data->type_donate = $this->type_donate;
        $data->creator_id = auth()->user()->id;
        // $data->date = $this->date;
        $data->save();
        $this->dispatchBrowserEvent('hide-modal-add');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ບັນທຶກຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ບັນທຶກຂໍ້ມູນສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
        $this->resetField();
    } else {
        $this->emit('alert', ['type' => 'error', 'message' => 'ກະລຸນາປ້ອນຈຳນວນເງິນເປັນຕົວເລກ!']);
    }
    }
    public function edit($ids)
    {
        $this->dispatchBrowserEvent('show-modal-edit');

        $Data = Expend::find($ids);
        $this->hiddenId = $Data->id;
        $this->newimage = $Data->image;
        $this->name = $Data->name;
        $this->qty_price = $Data->qty_price;
        $this->currency = $Data->currency;
        $this->type = $Data->type;
        $this->type_donate = $Data->type_donate;
        $this->created_at = $Data->created_at;
    }
    public function update()
    {
        $this->validate([
            'name'=>'required',
            'qty_price'=>'required|regex:/^[0-9]+$/i',
            'currency'=>'required',
            'type'=>'required',
            'type_donate'=>'required',
            // 'date'=>'required',
        ],[
            'name.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'qty_price.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'qty_price.regex'=>'ປ້ອນເປັນຕົວເລກທັງຫມົດ!',
            'currency.required'=>'ເລືອກສະກຸນເງິນກ່ອນ!',
            'type.required'=>'ເລືອກປະເພດເງິນກ່ອນ!',
            'type_donate.required'=>'ເລືອກປະເພດກ່ອນ!',
            // 'date.required'=>'ເລືອກວັນທີ່ກ່ອນ!',
        ]);
        $ids = $this->hiddenId;
        $data = Expend::find($ids);
        $data->name = $this->name;
        $data->qty_price = $this->qty_price;
        $data->currency = $this->currency;
        $data->type = $this->type;
        $data->type_donate = $this->type_donate;
        $data->created_at = $this->created_at;
        if ($this->image) {
            $this->validate([
                'image' => 'required|mimes:png,jpg,jpeg',
            ]);
            if ($this->image) {
                $this->validate([
                    'image' => 'required|mimes:png,jpg,jpeg',
                ]);
                if ($this->image != $data->image) {
                    if ($data->image) {
                        unlink('public/bill_photo' . '/' . $data->image);
                    }
                    if ($data->images) {
                        $images = explode(",", $data->images);
                        foreach ($images as $image) {
                            unlink('public/bill_photo' . '/' . $data->image);
                        }
                        $data->delete();
                    }
                }
                $imageName = Carbon::now()->timestamp . '.' . $this->image->extension();
                $this->image->storeAs('bill_photo', $imageName);
                $data->image = $imageName;
            }
        }
        $data->save();
        $this->dispatchBrowserEvent('hide-modal-edit');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
        $this->resetField();
    }
    public function showDestroy($ids)
    {
        $this->dispatchBrowserEvent('show-modal-delete');
        $Data = Expend::find($ids);
        $this->hiddenId = $Data->id;
    }
    public function destroy()
    {
        $ids = $this->hiddenId;
        $data = Expend::find($ids);
        $data->delete();
        $this->dispatchBrowserEvent('hide-modal-delete');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ລຶບຂໍ້ມູນສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
    }
}
