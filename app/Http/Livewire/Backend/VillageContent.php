<?php

namespace App\Http\Livewire\Backend;

use Livewire\Component;

class VillageContent extends Component
{
    public function render()
    {
        return view('livewire.backend.village-content');
    }
}
