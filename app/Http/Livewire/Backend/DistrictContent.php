<?php

namespace App\Http\Livewire\Backend;

use Livewire\Component;

class DistrictContent extends Component
{
    public function render()
    {
        return view('livewire.backend.district-content');
    }
}
