<?php

namespace App\Http\Livewire\Backend;
use App\Models\People_donate_detail;
use App\Models\People_donate;
use Livewire\Component;
use DB;
class ReportPeopleDonateContent extends Component
{
    
    public $start, $end;
    public $starts, $ends;
    public $people_donate_id;
    public $sum_lak = [];
    public $sum_thb = [];
    public $sum_usd = [],$name_lastname;
    public $list_detail = [];
    public $hidenId, $order_id;
    // public function mount()
    // {
    //     $orders = People_donate::where('id', $this->order_id)->get();
    // }
    public function render()
    {
        $start = $this->starts;
        $end = date('Y-m-d', strtotime($this->ends . ' + ' . 1 . 'days'));
        $report_people_donate = People_donate::select('*')->get();
        $sum3 = People_donate_detail::select('*')
        ->where('people_donate_id',62)
        ->where('currency',1)
        ->sum('total_money');


        // dd($sum3);
        return view('livewire.backend.report-people-donate-content',compact('report_people_donate','sum3'))->layout('layouts.backend.base');
    }
    public function show_donate_detail($ids)
    {
        $this->dispatchBrowserEvent('show-modal-delete');
        $Data = People_donate::find($ids);
        $this->ID = $Data->id;
        $this->name_lastname = $Data->name_lastname;
        $this->sum_lak = People_donate_detail::select('total_money')
        ->where('people_donate_id',$this->ID)
        ->where('currency',1)
        ->orderBy('id', 'desc')->sum('total_money');
        $this->sum_thb = People_donate_detail::select('total_money')
        ->where('people_donate_id',$this->ID)
        ->where('currency',2)
        ->orderBy('id', 'desc')->sum('total_money');
        $this->sum_usd = People_donate_detail::select('total_money')
        ->where('people_donate_id',$this->ID)
        ->where('currency',3)
        ->orderBy('id', 'desc')->sum('total_money');
        $this->list_detail = People_donate_detail::select('*')
        ->where('people_donate_id',$this->ID)->get();
    }
    public function sub()
    {
       if(!empty($this->start || $this->end))
       {
        $this->starts = $this->start;
        $this->ends = $this->end;
       }else{
        $this->emit('alert', ['type' => 'warning', 'message' => 'ເລືອກວັນທີ່ຫາວັນທີ່ກ່ອນຂະນ້ອຍໃຈເຍັນນ!']);
       }
    }
}
