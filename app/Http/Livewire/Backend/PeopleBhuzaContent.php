<?php

namespace App\Http\Livewire\Backend;

use Livewire\Component;

class PeopleBhuzaContent extends Component
{
    public function render()
    {
        return view('livewire.backend.people-bhuza-content');
    }
}
