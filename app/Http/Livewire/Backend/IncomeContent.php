<?php

namespace App\Http\Livewire\Backend;

use App\Models\Type;
use App\Models\Curren;
use App\Models\Income;
use Livewire\Component;
use Livewire\WithPagination;

class IncomeContent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $hiddenId, $name, $qty_price,$search, $created_at,$creator_id;
    public $currency = 1;
    public $type = 1;
    public $type_donate = 1;
    public function render()
    {
        $income = Income::orderBy('id','desc')   
        ->where('name','like','%' . $this->search. '%')
        ->orwhere('created_at','like','%' . $this->search. '%')
        ->paginate(10);
        return view('livewire.backend.income-content',compact('income'))->layout('layouts.backend.base');
    }
    public function resetField()
    {
        $this->name = '';
        $this->qty_price = '';
    }
        public function create(){

        $this->resetField();
        $this->dispatchBrowserEvent('show-modal-add');
    }

    public function store()
    {
        // 'users.first_name' => 'required|min:2|regex:/[A-Za-z. -]/|max:255',
        $this->validate([
            'name'=>'required',
            'qty_price'=>'required|regex:/^[0-9]+$/i',
            'currency'=>'required',
            'type'=>'required',
            'type_donate'=>'required',
            // 'date'=>'required',
        ],[
            'name.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'qty_price.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'qty_price.regex'=>'ປ້ອນເປັນຕົວເລກທັງຫມົດ!',
            'currency.required'=>'ເລືອກສະກຸນເງິນກ່ອນ!',
            'type.required'=>'ເລືອກປະເພດເງິນກ່ອນ!',
            'type_donate.required'=>'ເລືອກຄັງເງິນກ່ອນກ່ອນ!',
            // 'date.required'=>'ເລືອກວັນທີ່ກ່ອນ!',
        ]);
        $check_number = str_replace(',', '', $this->qty_price);
        if (intval($check_number)) {
        $data = new Income();
        $data->name = $this->name;
        $data->qty_price = $this->qty_price;
        $data->currency = $this->currency;
        $data->type = $this->type;
        $data->type_donate = $this->type_donate;
        $data->creator_id = auth()->user()->id;
        $data->save();
        $this->dispatchBrowserEvent('hide-modal-add');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ບັນທຶກຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ບັນທຶກຂໍ້ມູນສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
        // session()->flash('message', 'ບັນທຶກຂໍ້ມູນສຳເລັດ!');
        // return redirect()->route('backend.income');
        $this->resetField();
    } else {
        $this->emit('alert', ['type' => 'error', 'message' => 'ກະລຸນາປ້ອນຈຳນວນເງິນເປັນຕົວເລກ!']);
    }
    }
    public function edit($ids)
    {
        $this->dispatchBrowserEvent('show-modal-edit');

        $Data = Income::find($ids);
        $this->hiddenId = $Data->id;
        $this->name = $Data->name;
        $this->qty_price = $Data->qty_price;
        $this->currency = $Data->currency;
        $this->type = $Data->type;
        $this->type_donate = $Data->type_donate;
        $this->created_at = $Data->created_at;
    }
    public function update()
    {
        $this->validate([
            'name'=>'required',
            'qty_price'=>'required|regex:/^[0-9]+$/i',
            'currency'=>'required',
            'type'=>'required',
            'type_donate'=>'required',
            // 'date'=>'required',
        ],[
            'name.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'qty_price.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'qty_price.regex'=>'ປ້ອນເປັນຕົວເລກທັງຫມົດ!',
            'currency.required'=>'ເລືອກສະກຸນເງິນກ່ອນ!',
            'type.required'=>'ເລືອກປະເພດເງິນກ່ອນ!',
            'type_donate.required'=>'ເລືອກຄັງເງິນກ່ອນ!',
            // 'date.required'=>'ເລືອກວັນທີ່ກ່ອນ!',
        ]);
        $ids = $this->hiddenId;
        $data = Income::find($ids);
        $data->name = $this->name;
        $data->qty_price = $this->qty_price;
        $data->currency = $this->currency;
        $data->type = $this->type;
        $data->type_donate = $this->type_donate;
        $data->created_at = $this->created_at;
        $data->save();
        $this->dispatchBrowserEvent('hide-modal-edit');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
        $this->resetField();
    }
    public function showDestroy($ids)
    {
        $this->dispatchBrowserEvent('show-modal-delete');
        $Data = Income::find($ids);
        $this->hiddenId = $Data->id;
    }
    public function destroy()
    {
        $ids = $this->hiddenId;
        $data = Income::find($ids);
        $data->delete();
        $this->dispatchBrowserEvent('hide-modal-delete');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ລຶບຂໍ້ມູນສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
    }
}
