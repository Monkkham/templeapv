<?php

namespace App\Http\Livewire\Backend;
use App\Models\Monk_novice;
use Livewire\Component;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use Carbon\Carbon;
class MonknoviceContent extends Component
{
    use WithFileUploads;
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public 
    $ID,
    $image,
    $newimage,
    $search,
    $name,
    $lastname,
    $position,
    $gender,
    $status_now,
    $created_at,
    $updated_at;
    public function render()
    {
        $monk_novice = Monk_novice::where('name','like','%' . $this->search. '%')
        ->where('status_now',1)
        ->paginate(10);
        return view('livewire.backend.monknovice-content',compact('monk_novice'))->layout('layouts.backend.base');
    }
    public function resetform()
    {
        $this->image = '';
        $this->name = '';
        $this->status_now = '';
        $this->gender = '';
        $this->lastname = '';
        $this->created_at = '';
    }
        public function create(){

        $this->resetform();
        $this->dispatchBrowserEvent('show-modal-add');
    }

    public function store()
    {
        $this->validate([
            'name'=>'required',
            'lastname'=>'required',
            'position'=>'required',
            // 'qty_price'=>'required|regex:/^[0-9]+$/i',
        ],[
            'name.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'lastname.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'position.required'=>'ເລືອກຂໍ້ມູນກ່ອນ!',
            // 'qty_price.regex'=>'ປ້ອນເປັນຕົວເລກທັງຫມົດ!',
        ]);
        $data = new Monk_novice();
        if (!empty($this->image)) {
            $this->validate([
                'image' => 'required|mimes:jpg,png,jpeg',
            ]);
            $imageName = Carbon::now()->timestamp . '.' . $this->image->extension();
            // $this->image->storeAs('monk_novice', $imageName);
            // $data->image = 'monk_novice'.'/'. $imageName;
            $this->image->storeAs('monk_novice', $imageName);
            $data->image = $imageName;
        }else{
            $data->image = '';
        }
        $data->name = $this->name;
        $data->lastname = $this->lastname;
        $data->position = $this->position;
        $data->gender = $this->gender;
        $data->status_now = 1;
        $data->save();
        $this->dispatchBrowserEvent('hide-modal-add');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ບັນທຶກຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ບັນທຶກຂໍ້ມູນສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
        $this->resetform();
    }
    public function edit($ids)
    {
        $this->dispatchBrowserEvent('show-modal-edit');

        $Data = Monk_novice::find($ids);
        $this->ID = $Data->id;
        $this->newimage = $Data->image;
        $this->name = $Data->name;
        $this->lastname = $Data->lastname;
        $this->position = $Data->position;
        $this->gender = $Data->gender;
        $this->status_now = $Data->status_now;
    }
    public function update()
    {
        $this->validate([
            'name'=>'required',
            'position'=>'required',
            // 'gender'=>'required',
        ],[
            'name.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'position.required'=>'ເລືອກຂໍ້ມູນກ່ອນ!',
            // 'gender.required'=>'ເລືອກປະເພດກ່ອນ!',
        ]);
        $ids = $this->ID;
        $data = Monk_novice::find($ids);
        $data->name = $this->name;
        $data->lastname = $this->lastname;
        $data->position = $this->position;
        $data->gender = $this->gender;
        $data->status_now = $this->status_now;
        if ($this->image) {
            $this->validate([
                'image' => 'required|mimes:png,jpg,jpeg',
            ]);
            if ($this->image) {
                $this->validate([
                    'image' => 'required|mimes:png,jpg,jpeg',
                ]);
                if ($this->image != $data->image) {
                    if ($data->image) {
                        unlink('public/monk_novice' . '/' . $data->image);
                    }
                    if ($data->images) {
                        $images = explode(",", $data->images);
                        foreach ($images as $image) {
                            unlink('public/monk_novice' . '/' . $data->image);
                        }
                        $data->delete();
                    }
                }
                $imageName = Carbon::now()->timestamp . '.' . $this->image->extension();
                $this->image->storeAs('monk_novice', $imageName);
                $data->image = $imageName;
            }
        }
        $data->save();
        $this->dispatchBrowserEvent('hide-modal-edit');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
        $this->resetform();
    }
}
