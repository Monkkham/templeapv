<?php

namespace App\Http\Livewire\Backend;
use DB;
use App\Models\User;
use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
class ProfileContent extends Component
{
    use WithFileUploads;
    public $name, $lastname, $image, $email, $phone, $newimage;
    public function mount()
    {
        $user = User::where('id', auth()->user()->id)->first();
        $this->name = $user->name;
        $this->lastname = $user->lastname;
        $this->phone = $user->phone;
        $this->email = $user->email;
        $this->image = $user->image;
    }
    public function render()
    {
        return view('livewire.backend.profile-content')->layout('layouts.backend.base');
    }
    public function resetform()
    {
        $this->name = '';
        $this->lastname = '';
    }
    public function updateProfile()
    {
        $updateId = auth()->user()->id;
        if($updateId > 0)
        {
            $this->validate([
                'name'=>'required',
                'lastname'=>'required',
                'phone'=>'required|min:8|numeric|',
                // 'email'=>'email',
            ],[
                'name.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ',
                'lastname.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ',
                'phone.required'=>'ປ້ອນເບີໂທລະສັບກ່ອນ',
                'phone.min'=>'ເບີໂທລະສັບຕ້ອງ 8 ຕົວເລກ',
            ]);
        }
                $data = User::find(auth()->user()->id);
                $user_data = [
                    'name'=> $this->name,
                    'lastname'=> $this->lastname,
                    'phone'=> $this->phone,
                    'email'=> $this->email,
                ];
                DB::table('users')->where('id', $updateId)->update($user_data);
                // session()->flash('message', 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ!');
                $this->emit('alert', ['type' => 'success', 'message' => 'ແກ້ໄຂໂປຣຟາຍສຳເລັດ!']);
    }
}
