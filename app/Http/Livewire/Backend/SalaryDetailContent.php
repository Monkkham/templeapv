<?php

namespace App\Http\Livewire\Backend;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Salary_father;
use App\Models\SalaryDetail;
class SalaryDetailContent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $slug_id,$ID,$salary_father_id;
    public $name;
    public function mount($slug_id)
    {
        $salarys = Salary_father::where('id', $this->slug_id)->first();
        $this->salary_father_id = $salarys->id;
        $this->name = $salarys->name;
    }
    public function render()
    {
        // $salary_father = Salary_father::orderBy('id','desc')->paginate(10);
        $salary_detail = SalaryDetail::where('salary_father_id',$this->salary_father_id)->where('money', '>', '0')->orderBy('id','desc')->get();

        $sum_lak = SalaryDetail::select('money')
        ->where('salary_father_id',$this->salary_father_id)
        ->where('currency',1)
        ->orderBy('id', 'desc')->sum('money');
        $sum_thb = SalaryDetail::select('money')
        ->where('salary_father_id',$this->salary_father_id)
        ->where('currency',2)
        ->orderBy('id', 'desc')->sum('money');
        $sum_usd = SalaryDetail::select('money')
        ->where('salary_father_id',$this->salary_father_id)
        ->where('currency',3)
        ->orderBy('id', 'desc')->sum('money');
        return view('livewire.backend.salary-detail-content',compact('salary_detail','sum_lak','sum_thb','sum_usd'))->layout('layouts.backend.base');
    }
    public function resetField()
    {
        $this->name = '';
        $this->money = '';
        $this->company = '';

    }
    public function resetForm()
    {
        $this->money = '';
    }
    public function show_paymoney($ids)
    {
        $this->dispatchBrowserEvent('show-modal-paymoney');
        $Data = SalaryDetail::find($ids);
        $this->salary_father_id = $ids;
        $this->name = $Data->name;
        $this->money = $Data->money;
        $this->resetForm();
    }
    public function paymoney()
    {
         $this->validate([
            // 'money'=>'required|regex:/^[0-9]+$/i',
            'money'=>'required',
        ],[
            'money.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            // 'money.regex'=>'ປ້ອນເປັນຕົວເລກທັງຫມົດ!',
        ]);
        $check_int = str_replace(',','',$this->money);
        if(intval($check_int)){
            $data = SalaryDetail::find($this->salary_father_id);
            if(intval($data->money <= 0)){
                $this->emit('alert', ['type' => 'warning', 'message' => 'ຂໍອາໄພບໍ່ມີຈຳນວນເງິນໃຫ້ຖອນໄດ້!']);
            }elseif(intval($data->money < $check_int)){
                $this->emit('alert', ['type' => 'warning', 'message' => 'ຂໍອາໄພຍອດເງິນໃນບັນຊີຕ່ຳກວ່າຈຳນວນທີ່ຈະຖອນ!']);
            }else{
                $data->money = $data->money - $check_int;
                $data->save();
                $this->resetField();
                session()->flash('success', 'ບັນທຶກຂໍ້ມູນສຳເລັດ!');
                return redirect(route('backend.salary-detail', $this->slug_id));
                // return redirect()->route('backend.people_donate');
                // $this->dispatchBrowserEvent('hide-modal-paymoney');
                // $this->dispatchBrowserEvent('swal', [
                //     'title' => 'ຖອນເງິນອອກສຳເລັດ!',
                //     'icon'=>'success',
                //     'iconColor'=>'green',
                // ]);
            }
        }else{
            $this->emit('alert', ['type' => 'warning', 'message' => 'ກະລຸນາປ້ອນຂໍ້ມູນເປັນຕົວເລກ!']);
            $this->money = "";
        }
    }
}
